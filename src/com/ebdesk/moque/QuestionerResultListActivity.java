package com.ebdesk.moque;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.ebdesk.moque.adapters.QuestionerResultListAdapter;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.constants.GlobalVariable;
import com.ebdesk.moque.data.PackagedData;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.QuestionResult;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.helpers.BitmapHelper;
import com.ebdesk.moque.helpers.DatabaseHelper;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;

import de.timroes.swipetodismiss.SwipeDismissList;

public class QuestionerResultListActivity extends SherlockActivity implements OnItemClickListener{
	private final String TAG = "QuestionerResultListActivity";
	private ListView respondenLV;
	private QuestionerResultListAdapter adapter;
	private Questioner questioner;
	private TextView totalRespTV;
	private TextView sentTV;
	private TextView notSentTV;
	private TextView areaAchievedTV;
	private TextView targetAreaTV;
	

	private ArrayAdapter<String> mAdapter;
	private SwipeDismissList mSwipeList;
	private final static String EXTRA_MODE = "MODE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_responden_list);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				
		handleIntent();
		initWidgets();
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	}
	
	private void handleIntent(){
		Intent intent = getIntent();
		int questionerId = intent.getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, 0);
		questioner = DatabaseManager.getHelper(this).getQuestionerWithID(questionerId);
	}

	@Override
	protected void onPause() {
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		super.onPause();
	}
	
	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (GlobalVariable.questionerStatus.equals("Broadcast")){
        	getActionBar().setSubtitle("Tekan lama untuk memilih");
        }
        
    }

	private void initWidgets(){
		adapter = new QuestionerResultListAdapter(this);
		adapter.refreshContent(questioner);
		
		totalRespTV = (TextView) findViewById(R.id.qter_total_responden);
		sentTV = (TextView) findViewById(R.id.qter_sent);
		notSentTV = (TextView) findViewById(R.id.qter_not_sent);
		areaAchievedTV  = (TextView) findViewById(R.id.qter_hasil_area);
		targetAreaTV = (TextView) findViewById(R.id.qter_target_area);
		
		updateFooterInformation();
		
		respondenLV = (ListView) findViewById(R.id.responden_list);
		respondenLV.setAdapter(adapter);
		respondenLV.setOnItemClickListener(this);
		respondenLV.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		respondenLV.setMultiChoiceModeListener(new ModeCallback());
		
		int modeInt = getIntent().getIntExtra("MODE", 0);
		SwipeDismissList.UndoMode mode = SwipeDismissList.UndoMode.values()[modeInt];
		mSwipeList = new SwipeDismissList(
				respondenLV,
				new SwipeDismissList.OnDismissCallback() {
				public SwipeDismissList.Undoable onDismiss(AbsListView listView, final int position) {
					final QuestionerResult item = adapter.getItem(position);
					adapter.remove(item);
					return new SwipeDismissList.Undoable() {
						@Override
						public String getTitle() {
							return item.getRespName() + " terhapus";
						}
						@Override
						public void undo() {
							adapter.insert(item, position);
						}
						@Override
						public void discard() {
							Log.w("DISCARD", "item " + item + " now finally discarded");
							BitmapHelper.deleteRespPhoto(item.getIdForServer()+".jpg");
							DatabaseManager.getHelper(QuestionerResultListActivity.this).deleteQuestionerResult(item);
						}
					};

				}
			},mode);

		// Just reset the adapter.
		resetAdapter();
	}
	
	private void resetAdapter() {
		mSwipeList.discardUndo();
		respondenLV.setAdapter(adapter);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		mSwipeList.discardUndo();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (GlobalVariable.questionerStatus.equals("Broadcast")){
			getSupportMenuInflater().inflate(R.menu.questioner_result_page, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.add_responden:
			//startRespondenChoicerActivity();
			addNewRespondenField();
			break;
		case R.id.questioner_result_refresh:
			sendTransaction();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// class for hold select
	private class ModeCallback implements ListView.MultiChoiceModeListener {
		private ArrayList<Integer> selectedIndexList;
		private ArrayList<Integer> finishedIndexList;
		private boolean firstTime = true;

		public ModeCallback() {
			selectedIndexList = new ArrayList<Integer>();
		}

		public void onDestroyActionMode(ActionMode mode) {
			selectedIndexList.clear();
			adapter.setActionMode(false);
			firstTime = true;
		}

		private void setSelected(int position) {
			Integer posInt = Integer.valueOf(position);
			if (!selectedIndexList.contains(posInt)) {
				selectedIndexList.add(posInt);
			}
		}

		private void setNotSelected(int position) {
			Integer posInt = Integer.valueOf(position);
			if (selectedIndexList.contains(posInt)) {
				selectedIndexList.remove(posInt);
			}
		}

		public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
			final int checkedCount = respondenLV.getCheckedItemCount();
			Integer positionIntObject = Integer.valueOf(position);

			if (checked && finishedIndexList.contains(positionIntObject)) {
				setSelected(position);
			} else {
				setNotSelected(position);
			}

			switch (checkedCount) {
			case 0:
				mode.setSubtitle(null);
				break;
			case 1:
				mode.setSubtitle("Satu data terpilih");
				break;
			default:
				mode.setSubtitle("" + checkedCount + " data terpilih");
				break;
			}
			
			if(firstTime){
				firstTime = !firstTime;
				if(!finishedIndexList.contains(positionIntObject))
					mode.finish();
			}
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, android.view.MenuItem item) {
			switch (item.getItemId()) {
			case R.id.upload:
				if(selectedIndexList.size()>0)
					sendTransactionOfSelectedItems(selectedIndexList);
				else
					Toast.makeText(QuestionerResultListActivity.this, "Tidak ada data perpilih", Toast.LENGTH_LONG).show();
				mode.finish();
				break;
			default:
				Toast.makeText(QuestionerResultListActivity.this, "Clicked " + item.getTitle(), Toast.LENGTH_SHORT)
						.show();
				break;
			}
			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, android.view.Menu menu) {
			if(GlobalVariable.questionerStatus.equals("Broadcast")){
				MenuInflater inflater = getMenuInflater();
				inflater.inflate(R.menu.list_select_menu, menu);
			}
				mode.setTitle("Pilih data survey");
				finishedIndexList = adapter.getCompleteQuestionerResultPositionList();
				adapter.setActionMode(true);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, android.view.Menu menu) {
			return false;
		}

	}
	
	private void sendTransactionOfSelectedItems(ArrayList<Integer> selectedIndexList) {
		ArrayList<PackagedData> data = new ArrayList<PackagedData>();
		int indexes[] = new int[selectedIndexList.size()];
		for (int i = 0; i < selectedIndexList.size(); i++) {
			indexes[i] = selectedIndexList.get(i);
		}
		ArrayList<QuestionerResult> questionerResultList = adapter.getQuestionerResultOnIndexes(indexes);
		for (QuestionerResult questionerResult : questionerResultList) {
			data.add(new PackagedData(questionerResult, getApplicationContext()));
		}
		
		ArrayList<URLTask> tasks = prepareURLTasks(data);

		for (URLTask task : tasks) {
			task.execute();
		}
	}
	
	private void sendTransaction(){
		ArrayList<PackagedData> data = new ArrayList<PackagedData>();
		try {
			ArrayList<QuestionerResult> questionerResultList = DatabaseManager.getHelper(this).getQuestionerResultFinishedNotSent(questioner.getId());
			for(QuestionerResult questionerResult:questionerResultList){
				data.add(new PackagedData(questionerResult,getApplicationContext()));
			}
			
			ArrayList<URLTask> tasks = prepareURLTasks(data);
			
			for(URLTask task:tasks){
				task.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * prepare asynctask runs a task send data to server
	 * @param packagedDataList
	 * @return
	 */
	private ArrayList<URLTask> prepareURLTasks(List<PackagedData> packagedDataList){
		ArrayList<URLTask> result = new ArrayList<URLTask>();
		for (final PackagedData packagedData : packagedDataList) {
			ResultListener listener = new ResultListener() {

				@Override
				public void onResultSuccess(Context context, String response) {
					try {
						JSONObject responInJSON = new JSONObject(response);
						if(!responInJSON.isNull("success")){
							if(responInJSON.getBoolean("success")){
								updateData(packagedData.getQuestionerResult());
								updateFooterInformation();
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onResultFailed(Context context) {
					Toast.makeText(QuestionerResultListActivity.this, "No Connection", Toast.LENGTH_LONG).show();
				}
			};
			URLTask task = new URLTask(this, URLBuilder.buildTransactionSendURL(), "", listener);
			task.setPostParams(packagedData.getPostedData());
			result.add(task);
		}
		return result;
	}
	
	private String provideData(){
		String result = "";
		try {
			ArrayList<QuestionerResult> questionerResultList = DatabaseManager.getHelper(this).getQuestionerResultFinishedNotSent(questioner.getId());
			JSONArray questionerResultJSONArray = new JSONArray();
			for(QuestionerResult questionerResult:questionerResultList){
				JSONObject questionerResultJSONObject = new JSONObject();
				questionerResultJSONObject.put("questioner_result_id", questionerResult.getId());
				
				ArrayList<Question> questionList = DatabaseManager.getHelper(this).getQuestionOfQuestioner(questionerResult.getQuestioner());
				JSONArray questionsJSONArray = new JSONArray();
				for(Question question:questionList){
					JSONObject questionJSONObject = new JSONObject();
					questionJSONObject.put("question_id", question.getIdFromServer());
					
					ArrayList<QuestionResult> questionResultList = DatabaseManager.getHelper(this).getQuestionResultList(questionerResult, question);
					JSONArray questionResultJSONArray = new JSONArray();
					for(QuestionResult questionResult:questionResultList){
						JSONObject questionResultJSONObject = new JSONObject();
						questionResultJSONObject.put("question_result_id", questionResult.getId());
						questionResultJSONArray.put(questionResultJSONObject);
					}
					questionJSONObject.put("question_results", questionResultJSONArray);
					questionsJSONArray.put(questionJSONObject);
				}
				questionerResultJSONObject.put("questions", questionsJSONArray);
				
				questionerResultJSONArray.put(questionerResultJSONObject);
			}
			result = questionerResultJSONArray.toString();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private void updateData(QuestionerResult questionerResult) {
		questionerResult.setSent(true);
		DatabaseManager.getHelper(this).updateQuestionerResult(questionerResult);
		adapter.refreshContent(questioner);
	}
	
	public void addNewRespondenField() {
		QuestionerResult qResult = new QuestionerResult();
		qResult.setCommentState("Harus beres");
		qResult.setCountry("Indonesia");
		
		SessionManager sessionManager = new SessionManager(getApplicationContext());
		String userName=sessionManager.getUsername();
		double lat=sessionManager.getLatitude();
		double lon=sessionManager.getLongitude();
		qResult.setInterviewedBy(userName);
		qResult.setQuestioner(questioner);
		qResult.setLattitude(lat);
		qResult.setLongitude(lon);
		qResult.setRespondenType(QuestionerResult.RespondenType.NONPANEL);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentDateandTime = sdf.format(new Date());
		qResult.setInterviewDate(currentDateandTime);
		DatabaseManager.getHelper(this).insertQuestionerResult(qResult);
		
		Intent intent = new Intent(this, AddRespondenActivity.class);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_PANEL, false);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, qResult.getId());
		startActivityForResult(intent, 1);
	}
	
	//jika pakai kirs grid - Not Used
	private void startRespondenChoicerActivity(){
		QuestionerResult qResult = new QuestionerResult();
		qResult.setCommentState("Harus beres");
		qResult.setCountry("Indonesia");
		qResult.setInterviewedBy("Agen Smith");
		qResult.setLocDesc("Beres");
		qResult.setQuestioner(questioner);
		DatabaseManager.getHelper(this).insertQuestionerResult(qResult);
		
		Intent intent = new Intent(this, RespondenChoicerActivity.class);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, questioner.getId());
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, qResult.getId());
		startActivity(intent);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		QuestionerResultListAdapter adapter= (QuestionerResultListAdapter) arg0.getAdapter();
		QuestionerResult qr= adapter.getItem(position);
		if(qr.getRespondenType().equals(QuestionerResult.RespondenType.PANEL)){
			Intent intent = new Intent(this, AddRespondenActivity.class);
			intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_PANEL, true);
			intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, adapter.getItem(position).getId());
			startActivity(intent);
		}else
		{
			Intent intent = new Intent(this, DoSurveyActivity.class);
			intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, adapter.getItem(position).getId());
			startActivity(intent);
		}
	}

	private void updateFooterInformation(){
		DatabaseHelper helper = DatabaseManager.getHelper(this);
		try {
			ArrayList<QuestionerResult> qListNotSent = helper.getQuestionerResultFinishedNotSent(questioner.getId());
			ArrayList<QuestionerResult> qListSent = helper.getQuestionerResultFinishedSent(questioner.getId());
			
			int notSent = qListNotSent.size();
			int totalResp = questioner.getQuestionerResultList().size();
			int sent = qListSent.size();
			int areaAchieved = questioner.getAchievedAreaCount();
			int targetArea = questioner.getTargetAreaCount();
			
			totalRespTV.setText(String.valueOf(totalResp));
			sentTV.setText(String.valueOf(sent));
			notSentTV.setText(String.valueOf(notSent));
			areaAchievedTV.setText(String.valueOf(areaAchieved));
			targetAreaTV.setText(String.valueOf(targetArea));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
