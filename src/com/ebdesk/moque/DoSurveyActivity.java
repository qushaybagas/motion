package com.ebdesk.moque;

import java.sql.SQLException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.fragments.AnsweringFragment;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.DirectoryHelper;
import com.ebdesk.moque.helpers.UILConfigBuilder;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class DoSurveyActivity extends SherlockFragmentActivity implements OnItemClickListener{
	private SideMenuAdapter adapter;
	private ListView questionLV;
	private SlidingMenu slidingMenu;
	private TextView questionerTitleTV;
	private AlertDialog exitDialog, finishDialog, notCompletedDialog;
	private QuestionerResult questionerResult;
	private AnsweringFragment currentDisplayedfragment;
	private String uniqueID;
	private ImageLoader imgLoader;
	private DisplayImageOptions displayOptions;
	
	private ImageView respondenIV;
	private TextView respondenNameTV;
	private TextView respondenAddressTV;
	private TextView respondenAgeTV;
	private TextView respondenPhoneNumberTV;
	private TextView respondenHandphoneTV;
	private TextView respondenOfficePhoneTV;
	private TextView respondenKTPNumberTV;
	private TextView respondenOccupationTV;
	private TextView respondenLastEduTV;
	private TextView respondenSexTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_do_survey);
		initDatas();
		initSideMenu();
		initWidgets();
		initConfirmExitDialog();
		initFinishAndExitDialog();
		initNotCompletedAnsweringDialog();
		initImageLoader();
		firstTime();
		retrieveRespondenData();
	}
	
	// retrieving respondent data to show in Sliding Drawer
	private void retrieveRespondenData(){
		String photoPath = DirectoryHelper.getPhotoRespBasePath()+"/"+questionerResult.getRespPhotoPath();
		String respName = questionerResult.getRespName();
		String respAddress = questionerResult.getRespAddress();
		String respAge = questionerResult.getRespAge();
		String respPhoneNumber = questionerResult.getRespPhone();
		String respHandphone = questionerResult.getRespHandPhone();
		String respOfficePhone = questionerResult.getRespOfficePhone();
		String respKTPNumber = questionerResult.getRespNoKTP();
		String respOccu = questionerResult.getRespOccupation();
		String respLastEdu = questionerResult.getRespLastEducation();
		String respSex = questionerResult.getRespSex();
		
		imgLoader.displayImage("file://"+photoPath, respondenIV, displayOptions);
		respondenNameTV.setText(respName);
		respondenAddressTV.setText(respAddress);
		respondenAgeTV.setText(respAge);
		respondenPhoneNumberTV.setText(respPhoneNumber);
		respondenHandphoneTV.setText(respHandphone);
		respondenOfficePhoneTV.setText(respOfficePhone);
		respondenKTPNumberTV.setText(respKTPNumber);
		respondenOccupationTV.setText(respOccu);
		respondenLastEduTV.setText(respLastEdu);
		respondenSexTV.setText(respSex);
	}
	
	private void initImageLoader(){
		imgLoader = ImageLoader.getInstance();
		imgLoader.init(UILConfigBuilder.getImageLoaderConfig(this));
		displayOptions = UILConfigBuilder.getDisplayOptions();
	}
	
	// if buttton back pressed
	@Override
	public void onBackPressed() {
		if(slidingMenu.isMenuShowing()){
			slidingMenu.toggle(true);
		}else{
			if (questionerResult.getState() == QuestionerResult.State.FINISH) {
				finishDialog.show();
			} else {
				exitDialog.show();
			}
		}
	}
	
	// dialog in the end of survey
	private void initFinishAndExitDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Questioner finish");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intent = new Intent(DoSurveyActivity.this, QuestionerResultListActivity.class);
				intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, questionerResult.getQuestioner().getId());
				intent.putExtra(CommonConstants.PASS_INTENT_UUID_TRANSCATION, uniqueID);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		});
		builder.setCancelable(false);
		finishDialog = builder.create();
	}
	
	//dialog if user not answer all of questioner
	private void initNotCompletedAnsweringDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Ada beberapa pertanyaan yang belum dijawab");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		});
		builder.setCancelable(false);
		notCompletedDialog = builder.create();
	}

	// dialong if user close survey before questioner finish
	private void initConfirmExitDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure?\nThis will pending the survey until you continue it");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent intent = new Intent(DoSurveyActivity.this, QuestionerResultListActivity.class);
				intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, questionerResult.getQuestioner().getId());
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		});
		exitDialog = builder.create();
	}
	
	private void initDatas(){
		int questionerResultId = getIntent().getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, 0);
		uniqueID = getIntent().getStringExtra(CommonConstants.PASS_INTENT_UUID_TRANSCATION);
		questionerResult = DatabaseManager.getHelper(this).getQuestionerResultWithId(questionerResultId);
	}

	// Menu sliding drawer initiation
	private void initSideMenu() {
		//getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().hide();
		slidingMenu = new SlidingMenu(this);
		slidingMenu.setMode(SlidingMenu.LEFT);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		slidingMenu.setBehindOffset(100);
		slidingMenu.setFadeDegree(0.35f);
		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		slidingMenu.setMenu(R.layout.menu_question_list);

		adapter = new SideMenuAdapter();
		questionLV = (ListView) slidingMenu.findViewById(R.id.menu_list_view);
		questionLV.setAdapter(adapter);
		questionLV.setOnItemClickListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			slidingMenu.toggle(true);
			break;
		case R.id.init_database:
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	// UI widget initiation
	private void initWidgets() {
		questionerTitleTV = (TextView) slidingMenu.findViewById(R.id.questioner_title);
		questionerTitleTV.setText(questionerResult.getQuestioner().getName());
		
		respondenIV = (ImageView) slidingMenu.findViewById(R.id.responden_photo);
		respondenNameTV = (TextView) slidingMenu.findViewById(R.id.resp_name);
		respondenAddressTV = (TextView) slidingMenu.findViewById(R.id.resp_address);
		respondenAgeTV = (TextView) slidingMenu.findViewById(R.id.resp_age);
		respondenPhoneNumberTV = (TextView) slidingMenu.findViewById(R.id.resp_telp);
		respondenHandphoneTV = (TextView) slidingMenu.findViewById(R.id.resp_hp);
		respondenOfficePhoneTV = (TextView) slidingMenu.findViewById(R.id.resp_office_phone);
		respondenKTPNumberTV = (TextView) slidingMenu.findViewById(R.id.resp_ktp);
		respondenOccupationTV = (TextView) slidingMenu.findViewById(R.id.resp_occupation);
		respondenLastEduTV = (TextView) slidingMenu.findViewById(R.id.resp_last_edu);
		respondenSexTV = (TextView) slidingMenu.findViewById(R.id.resp_sex);
	}

	//displaying question for first time
	private void firstTime(){
		currentDisplayedfragment = new AnsweringFragment();
		Bundle data = new Bundle();
		data.putInt(CommonConstants.PASS_INTENT_QUESTION_ID, adapter.getItem(0).getId());
		data.putInt(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, questionerResult.getId());
		currentDisplayedfragment.setArguments(data);
		getSupportFragmentManager().beginTransaction().replace(R.id.container, currentDisplayedfragment).commit();
	}
	
	//displaying question for second time or more
	private void attachQuestion(Question question, boolean toggle){
		currentDisplayedfragment = new AnsweringFragment();
		Bundle data = new Bundle();
		data.putInt(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, questionerResult.getId());
		data.putInt(CommonConstants.PASS_INTENT_QUESTION_ID, question.getId());
		currentDisplayedfragment.setArguments(data);
		
		getSupportFragmentManager().beginTransaction().replace(R.id.container, currentDisplayedfragment).commit();
		if (toggle)
			slidingMenu.toggle(true);
	}
	
	private void displayFragment(SideMenuAdapter adapter, int position, boolean toggle){
		Question question = (Question) adapter.getItem(position);
		attachQuestion(question, toggle);
		adapter.setSelectedIndex(position);
		questionLV.invalidateViews();
	}

	// if click/chose questioner list on Menu Drawer
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
		SideMenuAdapter adapter = (SideMenuAdapter) adapterView.getAdapter();
		displayFragment(adapter, position, true);
	}
	
	//if button next pressed
	public void doSaveAndGoToNext(View view){
		currentDisplayedfragment.saveAnswer();
		adapter.refresh();
		
		if (adapter.getSelectedIndex() < adapter.getCount() - 1)  //if user are not on the last questioner
			displayFragment(adapter, adapter.getSelectedIndex() + 1, false);
		else{  //else, user on the last questioner
			if(questionerResult.getState() != QuestionerResult.State.FINISH){  //if there is question not answered yet
				notCompletedDialog.show();
			}
		}
		
		questionerResult = DatabaseManager.getHelper(this).getQuestionerResultWithId(questionerResult.getId());
		if(questionerResult.getState() == QuestionerResult.State.FINISH){
			finishDialog.show();
		}
	}
	
	
	//-----------------------------
	// class for Slide menu drawer
	//-----------------------------
	private class SideMenuAdapter extends BaseAdapter {
		private ArrayList<Question> questionList;
		private int selectedIndex = 0;

		public SideMenuAdapter() {
			refresh();
		}
		
		public void refresh(){
			try {
				questionList = DatabaseManager.getHelper(DoSurveyActivity.this).getQuestionOfQuestioner(questionerResult.getQuestioner());
				notifyDataSetChanged();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public int getCount() {
			return questionList.size();
		}

		@Override
		public Question getItem(int position) {
			return questionList.get(position);
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			ViewHolder holder;
			if(convertView == null){
				holder = new ViewHolder();
				convertView = LayoutInflater.from(DoSurveyActivity.this).inflate(R.layout.question_item_list, null, false);
				holder.questionTitleTV = (TextView) convertView.findViewById(R.id.question_title);
				holder.questionSignIV = (ImageView) convertView.findViewById(R.id.question_sign);
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			
			if(position==selectedIndex){
				convertView.setBackgroundColor(getResources().getColor(R.color.button_blue));
			}else{
				convertView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
			}
			
			mapDataToView(questionList.get(position), holder);
			return convertView;
		}
		
		private void mapDataToView(Question question, ViewHolder holder){
			holder.questionTitleTV.setText(question.getNumber()+". "+question.getName());
			holder.questionSignIV.setImageResource(R.drawable.check);
			try {
				if (DatabaseManager.getHelper(DoSurveyActivity.this).isQuestionAnswered(questionerResult, question))
					holder.questionSignIV.setImageResource(R.drawable.check_active);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		public void setSelectedIndex(int index){
			selectedIndex = index;
		}
		
		public int getSelectedIndex() {
			return selectedIndex;
		}

		private class ViewHolder{
			TextView questionTitleTV;
			ImageView questionSignIV;
		}

	}


}
