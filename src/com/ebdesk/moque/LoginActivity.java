package com.ebdesk.moque;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;
import com.ebdesk.moque.services.GPSTracker;

public class LoginActivity extends SherlockActivity implements LocationListener {

    SessionManager session;
    
	private EditText usernameET;
	private EditText passwordET;
	private LocationManager locationManager;
	private String provider;
	Location location;
	String username;
	String password;
	private Double lat,lon;

    GPSTracker gps;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
		setContentView(R.layout.activity_login);
		session = new SessionManager(getApplicationContext());  
			
		initWidgets();
		turnGPSOn();
		// Get the location manager
	    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, true);
	    location = locationManager.getLastKnownLocation(provider);

	    // Initialize the location fields
	    if (location != null) {
		    System.out.println("Provider " + provider + " has been selected.");
		    onLocationChanged(location);
	    } else {
	    	System.out.println("Location Not Available");
	    }
	    
		//checking session
		if(session.isLoggedIn()){
			gotoQuestionerListActivity();
		}
	}
	
	private void gotoQuestionerListActivity(){
		Intent intent = new Intent(LoginActivity.this, QuestionerListActivity.class);
		startActivity(intent);
		finish();
	}
	
	private void initWidgets(){
		usernameET = (EditText) findViewById(R.id.login_username);
		passwordET = (EditText) findViewById(R.id.login_password);
	}
	
	public void doLogin(View view){
//		lat = (double) (location.getLatitude());
//	    lon = (double) (location.getLongitude());
    	
		String url = URLBuilder.buildLoginURL();
		username = usernameET.getText().toString();
		password = passwordET.getText().toString();
//		username = "admin";
//		password = "Admin123";
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
				
		ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					if(responseJson.getString("status").equals("granted")){
						saveUserInformationSession(responseJson.getString("username"), responseJson.getString("hash"),lat,lon);
						gotoQuestionerListActivity();
					}else{
						Toast.makeText(LoginActivity.this, responseJson.getString("error"), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(LoginActivity.this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show();
			}
		};
		
		URLTask task = new URLTask(this, url, "Signing in", listener);
		task.setPostParams(params);
		task.execute();
		
    }
	
	private void saveUserInformationSession(String username, String hash,double lat,double lon){
		session.saveUsername(username);
		session.saveHash(hash);
		session.saveLatitude(lat);
		session.saveLongitude(lon);
	}
	
	private void turnGPSOn(){
	    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

	    if(!provider.contains("gps")){ //if gps is disabled
	        final Intent poke = new Intent();
	        poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
	        poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
	        poke.setData(Uri.parse("3")); 
	        sendBroadcast(poke);
	    }
	    gps = new GPSTracker(LoginActivity.this);
	    
        // check if GPS enabled     
        if(gps.canGetLocation()){
             
            lat = gps.getLatitude();
            lon = gps.getLongitude();
             
            // \n is for new line
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + lat + "\nLong: " + lon, Toast.LENGTH_LONG).show();    
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
	    
	}

	/* Request updates at startup */
	  @Override
	  protected void onResume() {
	    super.onResume();
	    locationManager.requestLocationUpdates(provider, 400, 1, this);
	  }

	  /* Remove the locationlistener updates when Activity is paused */
	  @Override
	  protected void onPause() {
	    super.onPause();
	    locationManager.removeUpdates(this);
	  }

	@Override
	public void onLocationChanged(Location arg0) {
		double lat = (double) (location.getLatitude());
	    double lng = (double) (location.getLongitude());

	    Log.i("lat", lat+" : "+lng);
	}

	@Override
	public void onProviderDisabled(String arg0) {
		Toast.makeText(this, "Disabled provider " + provider,
		        Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		Toast.makeText(this, "Enabled new provider " + provider,
		        Toast.LENGTH_SHORT).show();
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
	
}