package com.ebdesk.moque.customviews;

import java.util.ArrayList;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

public class RadioButtonCheckedListener implements OnCheckedChangeListener{
	private ArrayList<RadioButton> radioButtonList;
	
	public RadioButtonCheckedListener(){
		radioButtonList = new ArrayList<RadioButton>();
	}
	
	public void addMember(RadioButton radioButton){
		radioButton.setOnCheckedChangeListener(this);
		radioButtonList.add(radioButton);
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
		if (checked) {
			for (RadioButton item : radioButtonList) {
				int itemOptionId = (Integer) item.getTag();
				int clickedOptionId = (Integer) compoundButton.getTag();
				if (itemOptionId == clickedOptionId) {
					item.setChecked(true);
				} else {
					item.setChecked(false);
				}
			}
		}
	}

}
