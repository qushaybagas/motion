package com.ebdesk.moque.adapters;

import java.sql.SQLException;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebdesk.moque.R;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.SessionManager;

public class QuestionerListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<Questioner> questionerList;
	private SessionManager session;

	public QuestionerListAdapter(Context context) {
		this.context = context;
		this.session = new SessionManager(context);
		refreshContent();
	}
	
	public void refreshContent(){
		try {
			questionerList = DatabaseManager.getHelper(context).getAllQuestionerOfUser(session.getUsername());
		} catch (SQLException e) {
			questionerList = new ArrayList<Questioner>();
			e.printStackTrace();
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return questionerList.size();
	}
	
	public ArrayList<Questioner> getAllQuestionerList(){
		return questionerList;
	}

	@Override
	public Questioner getItem(int position) {
		return questionerList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View existingView, ViewGroup parent) {
		ViewHolder holder;
		if (existingView == null) {
			existingView = LayoutInflater.from(context).inflate(R.layout.main_itemlist_layout, null, false);
			holder = new ViewHolder();
			holder.questionerTitleTV = (TextView) existingView.findViewById(R.id.questioner_title);
			holder.questionerSubTitleTV = (TextView) existingView.findViewById(R.id.questioner_subtitle);
			//holder.questionerTarget = (TextView) existingView.findViewById(R.id.questioner_target);
			holder.progressContainerV = existingView.findViewById(R.id.progress_container);
			holder.questionerStateTV = (TextView) existingView.findViewById(R.id.questioner_state);
			holder.agenAchievedTV = (TextView) existingView.findViewById(R.id.agen_achieved);
			holder.questionerDate=(TextView)existingView.findViewById(R.id.questioner_tanggal);
			holder.needToDownloadIndicatorV = existingView.findViewById(R.id.need_to_download_indicator);
			existingView.setTag(holder);
		} else {
			holder = (ViewHolder) existingView.getTag();
		}

		mapDataToView(holder, position);
		return existingView;
	}
	
	private void mapDataToView(ViewHolder holder, int position){
		Questioner questioner = questionerList.get(position);
		holder.questionerTitleTV.setText(questioner.getName());
		holder.questionerSubTitleTV.setText(questioner.getDescription());
		holder.questionerStateTV.setText(questioner.getState().toString());
		holder.questionerStateTV.setBackgroundColor(context.getResources().getColor(questioner.getState().getColor()));
		holder.agenAchievedTV.setText(String.valueOf(questioner.getTargetAgenCount()));
		holder.questionerDate.setText(questioner.getCreatedDate());
		
		ArrayList<Question> questionList = questioner.getQuestions();
		boolean isAllMediaDownloaded = true;
		for(Question question:questionList){
			isAllMediaDownloaded = isAllMediaDownloaded && question.isAllMediaDownloaded();
		}
		holder.progressContainerV.setVisibility(View.GONE);
		
		if(isAllMediaDownloaded){
			holder.needToDownloadIndicatorV.setVisibility(View.GONE);
		}else{
			holder.needToDownloadIndicatorV.setVisibility(View.VISIBLE);
		}
	}

	private class ViewHolder {
		TextView questionerTitleTV;
		TextView questionerSubTitleTV;
		TextView questionerTargetTV;
		View progressContainerV;
		TextView questionerStateTV;
		TextView agenAchievedTV;
		TextView questionerDate;
		View needToDownloadIndicatorV;
	}
}
