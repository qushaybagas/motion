package com.ebdesk.moque.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebdesk.moque.R;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.database.QuestionerResult;

public class QuestionerResultListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<QuestionerResult> qResultList;
	private ArrayList<String> selectedIndexList;
	private boolean isActionMode = false;
	
	public QuestionerResultListAdapter(Context context){
		this.context = context;
		selectedIndexList = new ArrayList<String>();
	}
	
	public boolean isActionMode() {
		return isActionMode;
	}

	public void setActionMode(boolean isActionMode) {
		this.isActionMode = isActionMode;
	}

	public void refreshContent(Questioner questioner){
		qResultList = questioner.getQuestionerResultList();
		notifyDataSetChanged();
	}
	
	/**
	 * return selected object of item selected on hold
	 * @param indexes
	 * @return
	 */
	public ArrayList<QuestionerResult> getQuestionerResultOnIndexes(int indexes[]){
		ArrayList<QuestionerResult> result = new ArrayList<QuestionerResult>();
		for(int index:indexes){
			result.add(qResultList.get(index));
		}
		
		return result;
	}
	
	/**
	 * get index of already finished survey
	 * @return
	 */
	public ArrayList<Integer> getCompleteQuestionerResultPositionList(){
		ArrayList<Integer> result = new ArrayList<Integer>();
		for(int i=0;i<qResultList.size();i++){
			if(qResultList.get(i).getState().equals(QuestionerResult.State.FINISH)){
				result.add(Integer.valueOf(i));
			}
		}
		return result;
	}

	@Override
	public int getCount() {
		return qResultList.size();
	}

	@Override
	public QuestionerResult getItem(int position) {
		return qResultList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public boolean isEnabled(int position) {
		boolean result = true;
		if(isActionMode){
			if(!qResultList.get(position).getState().equals(QuestionerResult.State.FINISH)){
				result = false;
			}
		}
		return result;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(R.layout.questioner_result_itemlist_layout, null, false);
			holder = new ViewHolder();
			holder.respondenNameTV = (TextView) convertView.findViewById(R.id.responden_name);
			holder.statusTV = (TextView) convertView.findViewById(R.id.survey_status);
			holder.sentStatusV = convertView.findViewById(R.id.sent_sign);
			holder.tagPanelTV=(TextView)convertView.findViewById(R.id.responden_tag_panel);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		//set selected item with certain color we want
		if(selectedIndexList.contains(String.valueOf(position))){
			convertView.setBackgroundColor(0xFFFF0000);
		}
		setupPanel(position, holder);
		mapContentToView(holder, position);
		return convertView;
	}
	
	private void setupPanel(int position, ViewHolder holder){
		QuestionerResult questionerResult = qResultList.get(position);
		if(questionerResult.getRespondenType() == QuestionerResult.RespondenType.PANEL){
			holder.tagPanelTV.setVisibility(View.VISIBLE);
		}else{
			holder.tagPanelTV.setVisibility(View.GONE);
		}
	}
	
	public void toggleSelected(int posisi){
		if(selectedIndexList.contains(posisi)){
			selectedIndexList.remove(posisi);
		} else {
			selectedIndexList.add(String.valueOf(posisi));
		}
		notifyDataSetChanged();
	}
	
	private void mapContentToView(ViewHolder holder, int position){
		QuestionerResult qResult = qResultList.get(position);
		/*KishGridResult kGR = qResult.getKishGridResultList().get(0);
		KishGridDetailResult kGDR = null;
		for(KishGridDetailResult item:kGR.getKishGridDetailResultList()){
			if(item.isSelected()){
				kGDR = item;
			}
		}*/
		holder.respondenNameTV.setText(qResultList.get(position).getRespName());
		holder.statusTV.setText(qResultList.get(position).getState().getValue());
		holder.statusTV.setTextColor(context.getResources().getColor(qResultList.get(position).getState().getColorRes()));
		
		if(qResult.isSent()){
			holder.sentStatusV.setBackgroundResource(R.color.green_bg);
		}else{
			holder.sentStatusV.setBackgroundResource(R.color.yellow);
		}
	}
	
	private class ViewHolder{
		TextView respondenNameTV;
		TextView statusTV;
		View sentStatusV;
		TextView tagPanelTV;
	}

	public void remove(QuestionerResult item) {
		qResultList.remove(item);
		notifyDataSetChanged();
	}
	
	public void insert(QuestionerResult questionerResult, int position){
		qResultList.add(position, questionerResult);
		notifyDataSetChanged();
	}

}
