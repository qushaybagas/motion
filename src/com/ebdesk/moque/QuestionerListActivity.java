package com.ebdesk.moque;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.ebdesk.moque.adapters.QuestionerListAdapter;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.constants.GlobalVariable;
import com.ebdesk.moque.database.DetQuestions;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.JsonParser;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;
import com.ebdesk.moque.services.DetQuestionDownloaderIntentService;
import com.ebdesk.moque.services.GcmIntentService;
import com.ebdesk.moque.services.QuestionDownloaderIntentService;
import com.ebdesk.moque.services.QuestionDownloaderIntentService.QuestionerDownloaderBinder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.manuelpeinado.refreshactionitem.RefreshActionItem;
import com.manuelpeinado.refreshactionitem.RefreshActionItem.RefreshActionListener;

public class QuestionerListActivity extends SherlockActivity implements OnItemClickListener, RefreshActionListener, LocationListener {
	private ListView questionerLV;
	public RefreshActionItem mRefreshActionItem;
	private QuestionerListAdapter adapter;
	private TextView totalQuestionerTV;
	private TextView broadcastQuestionerCountTV;
	private TextView expireQuestionerCountTV;
	private TextView closedQuestionerCountTV;

	private String regid;

	private SessionManager session;
	private LocationManager locationManager;
	private String provider;
	private Location location;
	boolean mRun;
	private ArrayList<DetQuestions> uncompleteMediaDownloadedDetQuestionList;

	public static final String EXTRA_MESSAGE = "message";

	String SENDER_ID = CommonConstants.GCM_SENDER_ID;

	static final String TAG = "GCMDemo";

	// TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	Context context;


	final private static int DIALOG_LOGIN = 1;

	private BadgeUpdaterReceiver badgeUpdaterReceiver;
	private ProgressUpdaterReceiver progressUpdaterReceiver;
	private NewTargetStatusUpdateReceiver newTargetStatusUpdateReceiver;

	private ServiceConnection serviceConnection;
	private View currentProcessItemDownloadV;
	private Questioner questionerBeingProcessed;
	private boolean stillDownloading = false;
	
	Dialog overlayInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questioner_list);
		getSupportActionBar().setHomeButtonEnabled(true);

		initNeededObject();
		initWidgets();
		configLocation();
		updateFooterInformation();
		playLoginSound();
		registerToGCM();
		initServiceConnection();
	}

	private void initServiceConnection() {
		serviceConnection = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {

			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				Toast.makeText(QuestionerListActivity.this, "...binded", Toast.LENGTH_LONG).show();
				QuestionDownloaderIntentService.QuestionerDownloaderBinder binder = (QuestionerDownloaderBinder) service;
				QuestionDownloaderIntentService myService = binder.getService();
				if (myService != null) {
					Toast.makeText(QuestionerListActivity.this, "monitoring", Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	private void initNeededObject() {
		session = new SessionManager(this);
		mRun = true;
		uncompleteMediaDownloadedDetQuestionList = new ArrayList<DetQuestions>();
		badgeUpdaterReceiver = new BadgeUpdaterReceiver();
		progressUpdaterReceiver = new ProgressUpdaterReceiver();
		newTargetStatusUpdateReceiver = new NewTargetStatusUpdateReceiver();
	}

	private void configLocation() {
		turnGPSOn();

		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Define the criteria how to select the locatioin provider -> use
		// default
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		location = locationManager.getLastKnownLocation(provider);

		// Initialize the location fields
		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
			onLocationChanged(location);
		} else {
			System.out.println("Location Not Available");
		}
	}

	private String getDownloadedQuestionersId() {
		String result = "";
		try {
			ArrayList<Questioner> questionerList = DatabaseManager.getHelper(this).getAllQuestionerOfUser(session.getUsername());
			if (questionerList == null) {
				questionerList = new ArrayList<Questioner>();
			}

			for (int i = 0; i < questionerList.size(); i++) {
				result += questionerList.get(i).getIdFromServer() + ", ";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		result = result.contains(",") ? result.substring(0, result.lastIndexOf(",")) : "";
		return result;
	}

	private void downloadQuestioner() {

		ResultListener listener = new ResultListener() {

			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject jsonResponse = new JSONObject(response);
					if (jsonResponse.getBoolean("success")) {
						JsonParser.parseAndSaveQuestioner(jsonResponse.getString("value"), QuestionerListActivity.this);
						adapter.refreshContent();
						playDownloadSound();
						downloadTargetQuotaUpdate();
						mRun = false;
					} else {
						Toast.makeText(QuestionerListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_LONG).show();
						mRun = false;
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onResultFailed(Context context) {
				mRun = false;
				Toast.makeText(QuestionerListActivity.this, "Cannot reach server", Toast.LENGTH_LONG).show();
			}
		};

		String url = URLBuilder.buildQuestionerURL();
		URLTask urlTask = new URLTask(this, url, "Downloading Questioner", listener);
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", session.getUsername()));
		params.add(new BasicNameValuePair("hash", session.getHash()));
		params.add(new BasicNameValuePair("id_exclude", "[" + getDownloadedQuestionersId() + "]"));
		urlTask.setPostParams(params);
		urlTask.execute();

		notifyTheDownloadProgress();
	}

	private void notifyTheDownloadProgress() {
		Intent intent = new Intent(this, QuestionerListActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

		// Build notification
		// Actions are just fake
		final NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setContentTitle("Download Questioner").setContentText("Download in progress...").setSmallIcon(R.drawable.ic_action_download);

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (mRun) {
					mBuilder.setProgress(0, 0, true);
					mNotifyManager.notify(0, mBuilder.build());
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				mBuilder.setContentText("Download complete").setProgress(0, 0, false);
				mNotifyManager.notify(0, mBuilder.build());

			}
		}
		// Starts the thread by calling the run() method in its Runnable
		).start();
	}

	private void playLoginSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.login);
		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}

		});
		mp.start();
	}

	private void downloadTargetQuotaUpdate() {
		ResultListener listener = new ResultListener() {

			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject jsonResponse = new JSONObject(response);
					if (jsonResponse.getBoolean("success")) {
						JsonParser.parseAndSaveQuota(jsonResponse.getString("value"), QuestionerListActivity.this);
						updateFooterInformation();
						adapter.refreshContent();
					} else {
						Toast.makeText(QuestionerListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(QuestionerListActivity.this, "Cannot reach server", Toast.LENGTH_LONG).show();
			}
		};
		String url = URLBuilder.buildQuestionerTargetUpdate(session.getUsername(), session.getHash());
		new URLTask(this, url, "Update target", listener).execute();
	}

	private void initWidgets() {
		questionerLV = (ListView) findViewById(R.id.questioner_list);
		adapter = new QuestionerListAdapter(this);
		questionerLV.setAdapter(adapter);
		questionerLV.setOnItemClickListener(this);

		totalQuestionerTV = (TextView) findViewById(R.id.questioner_list_footer_total);
		broadcastQuestionerCountTV = (TextView) findViewById(R.id.questioner_list_footer_broadcast);
		expireQuestionerCountTV = (TextView) findViewById(R.id.questioner_list_footer_expire);
		closedQuestionerCountTV = (TextView) findViewById(R.id.questioner_list_footer_closed);

		questionerLV.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int arg2, long arg3) {
				QuestionerListAdapter adapter = (QuestionerListAdapter) adapterView.getAdapter();
				questionerBeingProcessed = adapter.getItem(arg2);
				
				if(!stillDownloading)
					currentProcessItemDownloadV = view;
				showDialog(DIALOG_LOGIN);
				return true;
			}
		});
		overlayInfo = new Dialog(QuestionerListActivity.this);
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        overlayInfo.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        overlayInfo.setCancelable(true);
        overlayInfo.setContentView(R.layout.guide_1_overlay);
        overlayInfo.show();
        Button bGuide1Ok=(Button)overlayInfo.findViewById(R.id.guide_1_ok);
        Button bGuide2Ok=(Button)overlayInfo.findViewById(R.id.guide_2_ok);
        Button bGuide3Ok=(Button)overlayInfo.findViewById(R.id.guide_3_ok);
        Button bGuide4Ok=(Button)overlayInfo.findViewById(R.id.guide_4_ok);
        Button bGuide5Ok=(Button)overlayInfo.findViewById(R.id.guide_5_ok);
        bGuide1Ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				overlayInfo.dismiss();
			}
		});
		
	}
	
	private void toggleProgressVisibility(){
		final View containerV = currentProcessItemDownloadV.findViewById(R.id.progress_container);
		
		if(containerV.getVisibility() == View.VISIBLE){
			stillDownloading = false;
//			TextView statusTV = (TextView) containerV.findViewById(R.id.download_status);
//			statusTV.setText("...complete");
			ObjectAnimator fadeOutAnimator = ObjectAnimator.ofFloat(containerV, "alpha", 1f, 0f);
			fadeOutAnimator.setDuration(1000);
			fadeOutAnimator.addListener(new AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator arg0) {
					
				}
				
				@Override
				public void onAnimationRepeat(Animator arg0) {
					
				}
				
				@Override
				public void onAnimationEnd(Animator arg0) {
					containerV.setVisibility(View.GONE);
					currentProcessItemDownloadV = null;
					questionerBeingProcessed = null;
					adapter.refreshContent();
				}
				
				@Override
				public void onAnimationCancel(Animator arg0) {
					
				}
			});
			fadeOutAnimator.start();
		}else{
			stillDownloading = true;
			containerV.setVisibility(View.VISIBLE);
//			TextView statusTV = (TextView) containerV.findViewById(R.id.download_status);
//			statusTV.setText("Downloading...");
			containerV.setAlpha(1f);
		}
	}
	
	private void registerToGCM() {
		context = getApplicationContext();
		if (isGooglePlaySupported()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			if (regid.equals("")) {
				registerInBackground();
			}
		}
	}

	private void registerInBackground() {
		new Registor().execute();
	}

	// GCM
	private String getRegistrationId(Context context) {
		String registrationId = session.getGCMId();
		if (registrationId.equals("")) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = session.getAPPVersion();
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			session.saveAPPVersion(currentVersion);
			return "";
		}
		return registrationId;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialogDetails = null;
		switch (id) {
		case DIALOG_LOGIN:
			LayoutInflater inflater = LayoutInflater.from(this);
			View dialogview = inflater.inflate(R.layout.dialog_layout, null);

			AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();
			break;
		}
		return dialogDetails;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DIALOG_LOGIN:
			final AlertDialog alertDialog = (AlertDialog) dialog;
			Button downloadButton = (Button) alertDialog.findViewById(R.id.q_btn_download);
			if(stillDownloading){
				downloadButton.setEnabled(false);
			}else{
				downloadButton.setEnabled(true);
			}
			Button mapButton = (Button) alertDialog.findViewById(R.id.q_btn_map);
			downloadButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
					startDownloadMedia();
				}
			});

			mapButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
					Intent intent = new Intent(QuestionerListActivity.this, MapRecordActivity.class);
					intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, questionerBeingProcessed.getIdFromServer());
					startActivity(intent);
				}
			});
			break;
		}
	}

	private void updateFooterInformation() {
		try {
			int totalQuestioner = DatabaseManager.getHelper(this).getCountTotalQuestionerOfUser(session.getUsername());
			int broadCast = DatabaseManager.getHelper(this).getCountSpecificQuestionerOfUser(Questioner.State.BROADCAST, session.getUsername());
			int expired = DatabaseManager.getHelper(this).getCountSpecificQuestionerOfUser(Questioner.State.EXPIRED, session.getUsername());
			int closed = DatabaseManager.getHelper(this).getCountSpecificQuestionerOfUser(Questioner.State.CLOSED, session.getUsername());

			totalQuestionerTV.setText(String.valueOf(totalQuestioner));
			broadcastQuestionerCountTV.setText(String.valueOf(broadCast));
			expireQuestionerCountTV.setText(String.valueOf(expired));
			closedQuestionerCountTV.setText(String.valueOf(closed));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.menu_questioner_list_page, menu);
		MenuItem item = menu.findItem(R.id.main_item_refresh);
		mRefreshActionItem = (RefreshActionItem) item.getActionView();
		mRefreshActionItem.setMenuItem(item);
		mRefreshActionItem.setMax(100);
		mRefreshActionItem.setRefreshActionListener(this);

		refreshBadge();
		return true;
	}

	private void refreshBadge() {
		String badgeLabel = DatabaseManager.getHelper(this).getBadgeNotif();

		if (!badgeLabel.equals("0"))
			mRefreshActionItem.showBadge(badgeLabel);
		else
			mRefreshActionItem.hideBadge();

		try {
			DatabaseManager.getHelper(this).clearBadge();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void startDownloadMedia() {
		ArrayList<Question> questionList = new ArrayList<Question>();
		for (Question question : questionerBeingProcessed.getUncompleteMediaQuestions()) {
			questionList.add(question);
			checkDetQuestionsMedia(question);
		}
		
		if (questionList.size() > 0) {
			toggleProgressVisibility();
			Toast.makeText(this, "Start downloading resource..." + questionerBeingProcessed.getName(), Toast.LENGTH_LONG).show();
			
			ArrayList<String> questionIds = new ArrayList<String>();
			for (Question question : questionList) {
				questionIds.add(String.valueOf(question.getId()));
			}

			String questionIdList[] = new String[questionList.size()];
			questionIds.toArray(questionIdList);

			Intent intent = new Intent(this, QuestionDownloaderIntentService.class);
			intent.putExtra(CommonConstants.PASS_INTENT_QUESTION_IDs, questionIdList);
			startService(intent);

			startDownloadDetQuestionsMedia();
		}else{
			Toast.makeText(this, "No resource to download", Toast.LENGTH_LONG).show();
			currentProcessItemDownloadV = null;
			questionerBeingProcessed = null;
		}
	}

	private void checkDetQuestionsMedia(Question question) {
		ArrayList<DetQuestions> detQList = question.getOptions();
		for (DetQuestions detQ : detQList) {
			if (!detQ.isAllMediaDownloaded()) {
				uncompleteMediaDownloadedDetQuestionList.add(detQ);
			}
		}
	}

	private void startDownloadDetQuestionsMedia() {
		String detQuestionsId[] = new String[uncompleteMediaDownloadedDetQuestionList.size()];

		for (int i = 0; i < uncompleteMediaDownloadedDetQuestionList.size(); i++) {
			detQuestionsId[i] = String.valueOf(uncompleteMediaDownloadedDetQuestionList.get(i).getId());
		}

		Intent intent = new Intent(this, DetQuestionDownloaderIntentService.class);
		intent.putExtra(CommonConstants.PASS_INTENT_DET_QUESTION_IDs, detQuestionsId);
		startService(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.main_item_profile:
			Intent intent = new Intent(this, ProfileActivity.class);
			startActivity(intent);
			break;
		case android.R.id.home:
			// coba();
			break;
		case R.id.main_item_logout:
			// doLogout();
			session.logoutUser();
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
		Questioner questioner = adapter.getItem(position);
		GlobalVariable global=new GlobalVariable();
		global.questionerId=questioner.getIdFromServer();
		global.questionerStatus=questioner.getState().toString();
		Intent intent = new Intent(this, QuestionerResultListActivity.class);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, questioner.getId());
		startActivity(intent);
	}

	private void playDownloadSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.download);
		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}

		});
		mp.start();
	}

	@Override
	public void onRefreshButtonClick(RefreshActionItem sender) {
		downloadQuestioner();
		refreshBadge();
	}

	// Turn On GPS
	private void turnGPSOn() {
		String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		if (!provider.contains("gps")) { // if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			sendBroadcast(poke);
		}

	}

	/* Request updates at startup */
	@Override
	protected void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(provider, 400, 1, this);

		registerOurReceiver();
	}

	private void registerOurReceiver() {
		registerReceiver(badgeUpdaterReceiver, new IntentFilter(CommonConstants.RECEIVER_FLAG_BADGE_UPDATE));
		registerReceiver(progressUpdaterReceiver, new IntentFilter(CommonConstants.RECEIVER_FLAG_UPDATE_DOWNLOAD_PROGRESS));
		registerReceiver(newTargetStatusUpdateReceiver, new IntentFilter(CommonConstants.RECEIVER_FLAG_UPDATE_STATUS_TARGET));
	}

	private void unregisterOurReceiver() {
		unregisterReceiver(badgeUpdaterReceiver);
		unregisterReceiver(progressUpdaterReceiver);
		unregisterReceiver(newTargetStatusUpdateReceiver);
	}

	@Override
	public void onLocationChanged(Location arg0) {
		int lat = (int) (location.getLatitude());
		int lng = (int) (location.getLongitude());

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// Toast.makeText(this, "Disabled provider " + provider,
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// Toast.makeText(this, "Enabled new provider " + provider,
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
	

	public void doLogout() {
		String url = URLBuilder.buildLogoutURL();
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

		ResultListener listener = new ResultListener() {

			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					session.clearSession();
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onResultFailed(Context context) {
			}
		};

		URLTask task = new URLTask(this, url, "Signing in", listener);
		task.setPostParams(params);
		task.execute();

	}

	private int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private boolean isGooglePlaySupported() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, 9000).show();
			} else {
				finish();
			}
		}
		return true;
	}

	class Registor extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... arg0) {
			String msg = "";
			boolean success = false;
			try {
				// make sure that gcm var is not null even though we have
				// initialize it
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}

				regid = gcm.register(SENDER_ID);
				msg = "Device registered, registration ID=" + regid;

				// You should send the registration ID to your server over HTTP,
				// so it can use GCM/HTTP or CCS to send messages to your app.
				// The request to your server should be authenticated if your
				// app
				// is using accounts.

				// For this demo: we don't need to send it because the device
				// will send upstream messages to a server that echo back the
				// message using the 'from' address in the message.
				success = true;
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
				success = false;
			}

			Log.d("GCMRegistration", msg);
			return success;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			if (success) {
				sendRegistrationIdToBackend(regid);
			} else {
				Toast.makeText(QuestionerListActivity.this, "Perangkat belum terdaftar\nSilahkan login kembali", Toast.LENGTH_LONG).show();
			}
		}
	}

	private void sendRegistrationIdToBackend(String gcm_id) {
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", session.getUsername()));
		params.add(new BasicNameValuePair("hash", session.getHash()));
		params.add(new BasicNameValuePair("gcm_id", gcm_id));

		ResultListener listener = new ResultListener() {

			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					if (responseJson.getBoolean("success")) {
						session.saveGCMId(regid);
					} else {
						Toast.makeText(getApplicationContext(), responseJson.getString("error"), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(getApplicationContext(), "Cannot reach server", Toast.LENGTH_LONG).show();
			}
		};

		URLTask task = new URLTask(QuestionerListActivity.this, URLBuilder.buildProfileGCMRegister(), "Mempersiapkan aplikasi", listener);
		task.setPostParams(params);
		task.execute();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterOurReceiver();
	}
	
	public class NewTargetStatusUpdateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			adapter.refreshContent();
			updateFooterInformation();
		}
	}

	public class MyBroadcastReceiver_Update extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String update = intent.getStringExtra(GcmIntentService.EXTRA_KEY_UPDATE);
			mRefreshActionItem.showBadge(update);
		}
	}
	


	private class BadgeUpdaterReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent data) {
			String badgeLabel = DatabaseManager.getHelper(context).getBadgeNotif();
			mRefreshActionItem.showBadge(badgeLabel);
		}

	}

	private class ProgressUpdaterReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			toggleProgressVisibility();
		}

	}
}
