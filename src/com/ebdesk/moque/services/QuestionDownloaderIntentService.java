package com.ebdesk.moque.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.DirectoryHelper;

public class QuestionDownloaderIntentService extends IntentService{
	private String questionId[];
	private QuestionerDownloaderBinder mBinder;
	private int progress = 0;;
	private int questionerId;

	public QuestionDownloaderIntentService() {
		super("MotionApp_FileDownloader");
		mBinder = new QuestionerDownloaderBinder();
	}
	
	public int getProgress(){
		return progress;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	public class QuestionerDownloaderBinder extends Binder{
		public QuestionDownloaderIntentService getService(){
			return QuestionDownloaderIntentService.this;
		}
	}
	
	@Override
	protected void onHandleIntent(Intent data) {
		questionerId = data.getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, 0);
		questionId = data.getStringArrayExtra(CommonConstants.PASS_INTENT_QUESTION_IDs);
		
		try {
			for (int i = 0; i < questionId.length; i++) {
				Question question = DatabaseManager.getHelper(this).getQuestionWithId(Integer.parseInt(questionId[i]));
				
				if (question.getAudioPath() != null) {
					if (!question.getAudioPath().equals("null") && !question.getAudioPath().equals(""))
						downloadAudio(question);
				}
				if (question.getImagePath() != null) {
					if (!question.getImagePath().equals("null") && !question.getImagePath().equals(""))
						downloadImage(question);
				}
				if (question.getVideoPath() != null) {
					if (!question.getVideoPath().equals("null") && !question.getVideoPath().equals(""))
						downloadVideo(question);
				}

				question.setIsAllMediaDownloaded(true);
				DatabaseManager.getHelper(this).updateQuestion(question);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		broadCastStatus();
		stopSelf();
	}
	
	private void broadCastStatus(){
		Intent intent = new Intent();
		intent.setAction(CommonConstants.RECEIVER_FLAG_UPDATE_DOWNLOAD_PROGRESS);
		sendBroadcast(intent);
	}
	
	private void downloadVideo(Question question) throws IOException{
		int count;
		byte buffer[] = new byte[1024];
		URL url = new URL(question.getVideoPath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getVideoBasePath(), question.getIdFromServer()+".mp4");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		int totalSize = bis.available();
		int downloaded = 0;
        while ((count = bis.read(buffer)) != -1) {
        	downloaded += count;
            fos.write(buffer, 0, count);
            
            progress = (downloaded/totalSize)*100;
        }
		fos.flush();
		fos.close();
		bis.close();
		question.setVideoPath(outFile.getAbsolutePath());
	}
	
	private void downloadAudio(Question question) throws IOException{
		byte buffer[] = new byte[512];
		URL url = new URL(question.getAudioPath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getAudioBasePath(), question.getIdFromServer()+".mp3");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		int count;
		while ((count = bis.read(buffer)) != -1) {
			fos.write(buffer, 0, count);
		}
		fos.flush();
		fos.close();
		bis.close();
		question.setAudioPath(outFile.getAbsolutePath());
	}
	
	private void downloadImage(Question question) throws IOException{
		byte buffer[] = new byte[512];
		URL url = new URL(question.getImagePath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getImageBasePath(), question.getIdFromServer()+".jpg");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		int count;
		while ((count = bis.read(buffer)) != -1) {
			fos.write(buffer, 0, count);
		}
		fos.flush();
		fos.close();
		bis.close();
		question.setImagePath(outFile.getAbsolutePath());
	}

}
