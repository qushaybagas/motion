package com.ebdesk.moque.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.database.DetQuestions;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.DirectoryHelper;

public class DetQuestionDownloaderIntentService extends IntentService{
	private String detQuestionId[];
	private DetQuestionDownloaderBinder mBinder;
	private String status;

	public DetQuestionDownloaderIntentService() {
		super("MotionApp_FileDownloader_DetQuestions");
		mBinder = new DetQuestionDownloaderBinder();
	}
	
	public String getStatus(){
		return status;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	public class DetQuestionDownloaderBinder extends Binder{
		public DetQuestionDownloaderIntentService getService(){
			return DetQuestionDownloaderIntentService.this;
		}
	}

	@Override
	protected void onHandleIntent(Intent data) {
		Log.d("DetQuestion", "download DetQuestion");
		detQuestionId = data.getStringArrayExtra(CommonConstants.PASS_INTENT_DET_QUESTION_IDs);
		
		try {
			for (int i = 0; i < detQuestionId.length; i++) {
				DetQuestions detQuestion = DatabaseManager.getHelper(this).getDetQuestionWithId(Integer.parseInt(detQuestionId[i]));

				if (detQuestion.getAudioPath() != null) {
					if (!detQuestion.getAudioPath().equals("null") && !detQuestion.getAudioPath().equals(""))
						downloadAudio(detQuestion);
				}
				if (detQuestion.getImagePath() != null) {
					if (!detQuestion.getImagePath().equals("null") && !detQuestion.getImagePath().equals(""))
						downloadImage(detQuestion);
				}
				if (detQuestion.getVideoPath() != null) {
					if (!detQuestion.getVideoPath().equals("null") && !detQuestion.getVideoPath().equals(""))
						downloadVideo(detQuestion);
				}

				detQuestion.setAllMediaDownloaded(true);
				DatabaseManager.getHelper(this).updateDetQuestion(detQuestion);
				status = "done";
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		stopSelf();
	}
	
	private void downloadVideo(DetQuestions detQuestion) throws IOException{
		int count;
		status = "Download Video";
		byte buffer[] = new byte[1024];
		URL url = new URL(detQuestion.getVideoPath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getVideoBasePath(), "detQ"+detQuestion.getIdFromServer()+".mp4");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		int total = 0;
        while ((count = bis.read(buffer)) != -1) {
        	total += count;
        	status = "vid: "+total;
            fos.write(buffer, 0, count);
        }
		fos.flush();
		fos.close();
		bis.close();
		detQuestion.setVideoPath(outFile.getAbsolutePath());
	}
	
	private void downloadAudio(DetQuestions detQuestion) throws IOException{
		byte buffer[] = new byte[512];
		URL url = new URL(detQuestion.getAudioPath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getAudioBasePath(), "detQ"+detQuestion.getIdFromServer()+".mp3");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		status = "Download Audio";
		int count;
		while ((count = bis.read(buffer)) != -1) {
			fos.write(buffer, 0, count);
		}
		fos.flush();
		fos.close();
		bis.close();
		detQuestion.setAudioPath(outFile.getAbsolutePath());
	}
	
	private void downloadImage(DetQuestions detQuestion) throws IOException{
		byte buffer[] = new byte[512];
		URL url = new URL(detQuestion.getImagePath());
		BufferedInputStream bis = new BufferedInputStream(url.openStream());
		File outFile = new File(DirectoryHelper.getImageBasePath(), "detQ"+detQuestion.getIdFromServer()+".jpg");
		FileOutputStream fos = new FileOutputStream(outFile);
		
		status = "Download image";
		int count;
		while ((count = bis.read(buffer)) != -1) {
			fos.write(buffer, 0, count);
		}
		fos.flush();
		fos.close();
		bis.close();
		detQuestion.setImagePath(outFile.getAbsolutePath());
	}

}
