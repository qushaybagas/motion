package com.ebdesk.moque.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ebdesk.moque.QuestionerListActivity;
import com.ebdesk.moque.R;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.helpers.DatabaseManager;
import com.ebdesk.moque.helpers.JsonParser;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	private String gcmMessage;
	QuestionerListActivity questionerList;

	private final String TAG = "GcmIntentService";

	public static final String ACTION_MyIntentService = "com.ebdesk.moque.RESPONSE";
	public static final String ACTION_MyUpdate = "com.ebdesk.moque.UPDATE";
	public static final String EXTRA_KEY_IN = "EXTRA_IN";
	public static final String EXTRA_KEY_OUT = "EXTRA_OUT";
	public static final String EXTRA_KEY_UPDATE = "EXTRA_UPDATE";
	String msgFromActivity;
	String extraOut;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	private void updateBadgeNotif() {
		DatabaseManager.getHelper(this).updateBadge();

		Intent broadCastIntent = new Intent();
		broadCastIntent.setAction(CommonConstants.RECEIVER_FLAG_BADGE_UPDATE);
		sendBroadcast(broadCastIntent);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		if (!extras.isEmpty()) { // has effect of unparcelling Bundle

			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */

			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " + extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

				// Post notification of received message.
				gcmMessage = extras.getString("message");
				sendNotification(gcmMessage);
//				updateNotification(gcmMessage);
				
				String actionCode = extras.getString("code");
				String url = extras.getString("url");
				takeAction(Integer.parseInt(actionCode));
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void sendNotification(String msg) {
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, QuestionerListActivity.class), 0);

		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_notif).setContentTitle("MOTION")
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg).setSound(soundUri).setAutoCancel(true);

		mBuilder.setContentIntent(contentIntent);
		
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
	
	/**
	 * 1. Update questioner
	 * 		action : update badge and update database BadgeNotif
	 * 2. Update target
	 * 		action : database process, update screen if screen is active, without notification
	 * 3. Update questioner status
	 * 		action : database process, update screen if screen is active, without notification
	 * @param actionCode
	 */
	private void takeAction(int actionCode){
		switch(actionCode){
		case 1: updateBadgeNotif();break;
		case 2: updateTarget();break;
		case 3: updateStatus();break;
		}
	}

	private void updateTarget() {
		try {
			Log.d(TAG, "update target");
			SessionManager session = new SessionManager(this);
			String username = session.getUsername();
			String hash = session.getHash();
			String url = URLBuilder.buildQuestionerTargetUpdate(username, hash);

			HttpClient httpClient = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			HttpResponse response;
			response = httpClient.execute(get);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			
			JSONObject jsonResponse = new JSONObject(sb.toString());
			if (jsonResponse.getBoolean("success")) {
				JsonParser.parseAndSaveQuota(jsonResponse.getString("value"), this);
			}
			
			Intent intent = new Intent();
			intent.setAction(CommonConstants.RECEIVER_FLAG_UPDATE_STATUS_TARGET);
			sendBroadcast(intent);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void updateStatus() {
		Log.d(TAG, "update status");
		try {
			SessionManager session = new SessionManager(this);
			String username = session.getUsername();
			String hash = session.getHash();
			String url = URLBuilder.buildQuestionerStatusUpdate(username, hash);

			HttpClient httpClient = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			HttpResponse response;
			response = httpClient.execute(get);

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			JSONObject jsonResponse = new JSONObject(sb.toString());
			if (jsonResponse.getBoolean("success")) {
				JsonParser.parseAndSaveQuestionerStatus(jsonResponse.getString("value"), this);
			}
			
			Intent intent = new Intent();
			intent.setAction(CommonConstants.RECEIVER_FLAG_UPDATE_STATUS_TARGET);
			sendBroadcast(intent);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateNotification(String pesan) {
		Intent intentUpdate = new Intent();
		intentUpdate.setAction(ACTION_MyUpdate);
		intentUpdate.addCategory(Intent.CATEGORY_DEFAULT);
		intentUpdate.putExtra(EXTRA_KEY_UPDATE, pesan);
		sendBroadcast(intentUpdate);
	}

}