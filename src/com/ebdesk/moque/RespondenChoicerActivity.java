package com.ebdesk.moque;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.database.KishGridDetailResult;
import com.ebdesk.moque.database.KishGridResult;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.helpers.DatabaseManager;

public class RespondenChoicerActivity extends SherlockActivity{
	private ActionBar actionBar;
	private TableLayout respondenInputContainer;
	private ArrayList<TableRow> fieldList;
	private LayoutInflater inflater;
	private ArrayList<KishGridDetailResult> kishGridDetailResultList;
	private Button startBtn;
	private Button selectRespondenBtn;
	private int indexSelected;
	
	private QuestionerResult qResult;
	private KishGridResult kishGridResult;
	
	private final String TAG = "RespondenChoicerActivity";
	private AlertDialog exitDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_responden_choicer);
		initActionBar();
		handleIntent();
		initWidgets();
		initConfirmExitDialog();
		initKishGridData();
		inflater = LayoutInflater.from(this);
		kishGridDetailResultList = new ArrayList<KishGridDetailResult>();
	}
	
	private void initKishGridData(){
		kishGridResult = new KishGridResult();
		kishGridResult.setQuestionerResult(qResult);
		kishGridResult.setIdFromServer(1);//TODO later this id will be generated using hash
		DatabaseManager.getHelper(this).insertKishGridResult(kishGridResult);
	}
	
	private void initConfirmExitDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Are you sure");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				DatabaseManager.getHelper(RespondenChoicerActivity.this).deleteQuestionerResult(qResult);
				finish();
			}
		});
		exitDialog = builder.create();
	}
	
	private void handleIntent(){
		Intent intent = getIntent();
		int questionerResultId = intent.getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, 0);
		
		qResult = DatabaseManager.getHelper(this).getQuestionerResultWithId(questionerResultId);
	}

	private void initActionBar() {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == CommonConstants.RETURN_RESULT_CODE_SUCCESS) {
			TableRow row = (TableRow) inflater.inflate(R.layout.responden_init_input, null, false);
			TextView usernameTV = (TextView) row.findViewById(R.id.responden_name);
			TextView ageTV = (TextView) row.findViewById(R.id.responden_age);

			int kishGridDetailResultId = data.getIntExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_ID, 0);
			KishGridDetailResult kishGridDetailResult = DatabaseManager.getHelper(this).getKishGridDetailResultWithId(kishGridDetailResultId);
			kishGridDetailResultList.add(kishGridDetailResult);
			usernameTV.setText(data.getStringExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_NAME));
			ageTV.setText(String.valueOf(data.getIntExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_AGE, 1)));

			Button inputDetailB = (Button) row.findViewById(R.id.input_detail_button);
			inputDetailB.setVisibility(View.INVISIBLE);

			fieldList.add(row);
			respondenInputContainer.addView(row);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}*/

	public void addNewRespondenField(View view) {
		Intent intent = new Intent(this, AddRespondenActivity.class);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, qResult.getId());
		intent.putExtra(CommonConstants.PASS_INTENT_KISH_GRID_DETAIL_RESULT_ID, kishGridDetailResultList.get(indexSelected).getId());
		startActivityForResult(intent, 1);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == CommonConstants.RETURN_RESULT_CODE_SUCCESS) {
			startBtn.setVisibility(View.VISIBLE);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void addForm(){
		TableRow row = (TableRow) LayoutInflater.from(this).inflate(R.layout.input_kish_grid_layout, null, false);
		respondenInputContainer.addView(row);
	}

	private void initWidgets() {
		fieldList = new ArrayList<TableRow>();
		respondenInputContainer = (TableLayout) findViewById(R.id.responden_input_container);
		startBtn = (Button) findViewById(R.id.questioner_start);
		startBtn.setVisibility(View.GONE);
		selectRespondenBtn = (Button) findViewById(R.id.select_responden);
	}

	public void doSelect(View view) {
		int indexSelected = 0;
		int biggestAge = 0;

		for (int i = 0; i < kishGridDetailResultList.size(); i++) {
			int age = kishGridDetailResultList.get(i).getAge();
			if (age > biggestAge) {
				indexSelected = i;
				biggestAge = age;
			}
		}
		
		Button inputDetailB = (Button) fieldList.get(indexSelected).findViewById(R.id.input_detail_button);
//		inputDetailB.setVisibility(View.VISIBLE);
		fieldList.get(indexSelected).setBackgroundResource(R.color.light_blue);
		selectRespondenBtn.setVisibility(View.GONE);
		startBtn.setVisibility(View.VISIBLE);
		this.indexSelected = indexSelected;
	}
	
	public void doSelectVer2(View view){
		view.setVisibility(View.GONE);
		indexSelected = 0;
		int biggestAge = 0;
		
		for(int i=0;i<respondenInputContainer.getChildCount();i++){
			TableRow row = (TableRow) respondenInputContainer.getChildAt(i);
			EditText nameET = (EditText) row.findViewById(R.id.name);
			EditText ageET = (EditText) row.findViewById(R.id.age);
			int age = Integer.parseInt(ageET.getText().toString());
			
			KishGridDetailResult kgdResult = new KishGridDetailResult();
			kgdResult.setAge(age);
			kgdResult.setKishGridResult(kishGridResult);
			kgdResult.setName(nameET.getText().toString());
			kgdResult.setIdFromServer(1);
			kgdResult.setSelected(false);
			kishGridDetailResultList.add(kgdResult);
			DatabaseManager.getHelper(this).insertKishGridDetailResult(kgdResult);
			
			if (age > biggestAge) {
				indexSelected = i;
				biggestAge = age;
			}
		}
		
		TableRow row = (TableRow) respondenInputContainer.getChildAt(indexSelected);
		Button inputDetailButton = (Button) row.findViewById(R.id.detail_button);
		selectRespondenBtn.setVisibility(View.GONE);
		inputDetailButton.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.responden_choicer_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int menuID = item.getItemId();
		switch (menuID) {
		case R.id.add_responden:
//			addNewRespondenField();
			addForm();
			break;
		case android.R.id.home:
			exitDialog.show();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		exitDialog.show();
	}

	protected void onPause() {
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		super.onPause();
	}

	public void startQuestionerActivity(View view) {
		Intent intent = new Intent(this, DoSurveyActivity.class);
		KishGridDetailResult grid = kishGridDetailResultList.get(indexSelected);
		grid.setSelected(true);
		DatabaseManager.getHelper(this).updateKishGridDetailResult(grid);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, kishGridResult.getQuestionerResult().getId());
		startActivity(intent);
	}

	public void startLater(View view) {
		Intent intent = new Intent(this, QuestionerResultListActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
}
