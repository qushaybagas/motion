package com.ebdesk.moque;

import java.util.UUID;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.helpers.BitmapHelper;
import com.ebdesk.moque.helpers.DatabaseManager;

public class AddRespondenActivity extends SherlockActivity {
	private final String TAG = "AddResponden";
	private QuestionerResult questionerResult;
	public boolean isPhotoAttached;
	private View imageContainer;
	private ImageView pict1;
	private EditText nameET;
	private EditText addressET;
	private EditText phoneNumberET;
	private EditText handphoneET;
	private EditText officePhoneET;
	private EditText photoURLET;
	private EditText noKtpET;
	private EditText occupationET;
	private EditText lastEducationET;
	private EditText ageET;
	private RadioButton priaRB;
	private RadioButton wanitaRB;
	int questionerResultId;
	String uniqueID;
	Boolean isPanel;
	private AlertDialog onBackPressedAskDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_responden);

		uniqueID = UUID.randomUUID().toString();

		handleIntent();
		initWidget();
		initDialogs();
		configDisplay();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	private void initWidget() {
		imageContainer = findViewById(R.id.image_container);
		pict1 = (ImageView) findViewById(R.id.user_pict);
		nameET = (EditText) findViewById(R.id.person_name);
		addressET = (EditText) findViewById(R.id.person_address);
		phoneNumberET = (EditText) findViewById(R.id.person_phone_number);
		handphoneET = (EditText) findViewById(R.id.person_handphone);
		officePhoneET = (EditText) findViewById(R.id.person_office_phone);
		noKtpET = (EditText) findViewById(R.id.person_ktp_number);
		occupationET = (EditText) findViewById(R.id.person_occupation);
		lastEducationET = (EditText) findViewById(R.id.person_last_education);
		ageET = (EditText) findViewById(R.id.person_age);
		priaRB = (RadioButton) findViewById(R.id.radio_pria);
		wanitaRB = (RadioButton) findViewById(R.id.radio_wanita);
		if (isPanel){
			initDataPanel();
		}
		initDialogs();
	}
	
	private void initDataPanel(){
		nameET.setText(questionerResult.getRespName());
		addressET.setText(questionerResult.getRespAddress());
		ageET.setText(questionerResult.getRespAge());
		priaRB.isChecked();
		if(questionerResult.getRespSex().equals("Pria")){
			priaRB.isChecked();
		}else{
			wanitaRB.isChecked();
		}
		lastEducationET.setText(questionerResult.getRespLastEducation());
		noKtpET.setText(questionerResult.getRespNoKTP());
		handphoneET.setText(questionerResult.getRespHandPhone());
		phoneNumberET.setText(questionerResult.getRespPhone());
		officePhoneET.setText(questionerResult.getRespOfficePhone());
		occupationET.setText(questionerResult.getRespOccupation());
	}
	
	private void initDialogs() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this)
		.setMessage("Are you sure?").setPositiveButton("Yes", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				if(!isPanel){
					DatabaseManager.getHelper(AddRespondenActivity.this).deleteQuestionerResult(questionerResult);
				}
				finish();
			}
		})
		.setNegativeButton("No", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				onBackPressedAskDialog.dismiss();
			}
		});

		onBackPressedAskDialog = builder.create();
	}

	private void handleIntent() {
		Intent data = getIntent();
		isPanel = data.getBooleanExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_PANEL, false);
		questionerResultId = data.getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, 0);
		questionerResult = DatabaseManager.getHelper(this).getQuestionerResultWithId(questionerResultId);
	}

	private void configDisplay() {
		if (!isPhotoAttached) {
			imageContainer.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.action_addresponden_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			finish();
			break;
		case R.id.save_new_responden:
			saveResponden();
			break;
		case R.id.attach_images:
			startCamera();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		onBackPressedAskDialog.show();
	}
	
	private void saveResponden(){
		String name = nameET.getText().toString();
		String address = addressET.getText().toString();
		String phoneNumber = phoneNumberET.getText().toString();
		String handPhone = handphoneET.getText().toString();
		String officePhone = officePhoneET.getText().toString();
		String noKtp = noKtpET.getText().toString();
		String occupation = occupationET.getText().toString();
		String lastEducation = lastEducationET.getText().toString();
		String age = ageET.getText().toString();
		String sex = priaRB.isChecked() ? priaRB.getText().toString() : wanitaRB.getText().toString();
		
		
		questionerResult.setRespName(name);
		questionerResult.setRespAddress(address);
		questionerResult.setRespPhone(phoneNumber);
		questionerResult.setRespHandPhone(handPhone);
		questionerResult.setRespOfficePhone(officePhone);
		questionerResult.setRespNoKTP(noKtp);
		questionerResult.setRespOccupation(occupation);
		questionerResult.setRespLastEducation(lastEducation);
		questionerResult.setRespAge(age);
		questionerResult.setRespSex(sex);
//		questionerResult.setRespondenType(QuestionerResult.RespondenType.NONPANEL);
		DatabaseManager.getHelper(this).updateQuestionerResult(questionerResult);
		setResult(CommonConstants.RETURN_RESULT_CODE_SUCCESS);
		startQuestionerActivity();
	}
	
	private void startQuestionerActivity() {
		Intent intent = new Intent(this, DoSurveyActivity.class);
		intent.putExtra(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, questionerResult.getId());
		intent.putExtra(CommonConstants.PASS_INTENT_UUID_TRANSCATION, uniqueID);
		startActivity(intent);
	}
	
	/*private void saveResponden() {
		String pName = personName.getText().toString();
		String pAge = ageET.getText().toString();
		
		if(pName.equals("") || pAge.equals("")){
			uncompleteMsgDialog.show();
		}else{
			KishGridDetailResult kishGridDetailResult = new KishGridDetailResult();
			kishGridDetailResult.setName(pName);
			kishGridDetailResult.setAge(Integer.parseInt(pAge));
			kishGridDetailResult.setKishGridResult(kishGridResult);
			DatabaseManager.getHelper(this).insertKishGridDetailResult(kishGridDetailResult);
					
			Intent data = new Intent();
			data.putExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_ID, kishGridDetailResult.getId());
			data.putExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_NAME, kishGridDetailResult.getName());
			data.putExtra(CommonConstants.PASS_INTENT_REGISTERED_RESPONDEN_AGE, kishGridDetailResult.getAge());
			setResult(CommonConstants.RETURN_RESULT_CODE_SUCCESS, data);
			finish();
		}
	}*/
	
	
	
	// Run camera
	private void startCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, 1);
	}

	
	//if picture taken from camera
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == -1) {
			Bitmap bitmap = (Bitmap) data.getExtras().get("data");
			String userPhotoPath = BitmapHelper.saveBitmap(bitmap, questionerResult.getIdForServer()+".jpg");
			updateQuestionerResultRespPhoto(userPhotoPath); //Save image path to local DB
			pict1.setImageBitmap(bitmap);
			questionerResult.setRespPhotoPath(questionerResult.getIdForServer()+".jpg");
			imageContainer.setVisibility(View.VISIBLE);
		}
	}
	
	//Save image path to local DB
	private void updateQuestionerResultRespPhoto(String respPhotoPath){
		questionerResult.setRespPhotoPath(respPhotoPath);
		DatabaseManager.getHelper(this).updateQuestionerResult(questionerResult);
	}

}
