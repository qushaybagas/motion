package com.ebdesk.moque.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ebdesk.moque.constants.GlobalVariable;
import com.ebdesk.moque.database.QuestionResult;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.helpers.DirectoryHelper;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class PackagedData {
	public String mqQid;
	private String mqTid;
	private String mqTvalue;
	private String mqTupdateBy;
	private QuestionerResult questionerResult;
	private SessionManager session;
	private Context context;

	public PackagedData(QuestionerResult questionerResult, Context context) {
		this.context = context;
		this.questionerResult = questionerResult;
//		mqQid = String.valueOf(questionerResult.getQuestioner().getId);
		GlobalVariable global=new GlobalVariable();
		mqQid = String.valueOf(global.questionerId);
		mqTid = questionerResult.getIdForServer();
		try {
			mqTvalue = generateJsonFromExistingData();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		mqTupdateBy = loadSavedPreferences(context);
		session = new SessionManager(context);
	}
	
	public QuestionerResult getQuestionerResult(){
		return questionerResult;
	}

	private String loadSavedPreferences(Context context) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String name = sharedPreferences.getString(SessionManager.KEY_USERNAME, "");
		return name;
	}

	public ArrayList<NameValuePair> getPostedData() {
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("mq_q_id", mqQid));
		params.add(new BasicNameValuePair("mq_t_id", mqTid));
		params.add(new BasicNameValuePair("mq_t_value", mqTvalue));
		params.add(new BasicNameValuePair("username", session.getUsername()));
		params.add(new BasicNameValuePair("hash", session.getHash()));

		sendPhoto();
		return params;
	}

	private void sendPhoto() {
		
		RequestParams params = new RequestParams();
		try {
			params.put("username", session.getUsername());
			params.put("hash", session.getHash());
			params.put("responden_photo",
			        new FileInputStream(DirectoryHelper.getPhotoRespBasePath() + "/"+questionerResult.getIdForServer()+".jpg"),
			        questionerResult.getIdForServer()+".jpg",
			        "image/jpeg");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		final Activity activity = this;
		AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		        @Override
		        public void onSuccess(String content) {
//		                Toast.makeText(activity, "Success: " + content, Toast.LENGTH_LONG).show();
		        }

		        @Override
		        public void onFailure(Throwable error, String content) {
//		                Toast.makeText(activity, "Failure: " + content, Toast.LENGTH_LONG).show();
		        }

		        @Override
		        public void onProgress(int position, int length) {
		                // e.g. Update a ProgressBar:
		        }
		};

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(URLBuilder.buildTransactionSendPhoto(), params, responseHandler);
	}

	public ArrayList<String[]> getMultipartPostedData() {
		ArrayList<String[]> result = new ArrayList<String[]>();
		String qId[] = new String[] { "mq_q_id", mqQid, "text" };

		String qTid[] = new String[] { "mq_t_id", mqTid, "text" };

		String qTValue[] = new String[] { "mq_t_value", mqTvalue, "text" };
		String qTupdateBy[] = new String[] { "mq_t_update_by", mqTupdateBy, "text" };

		String qRespPhoto[] = new String[] { "mq_resp_photo", questionerResult.getRespPhotoPath(), "file" };

		result.add(qId);
		result.add(qTid);
		result.add(qTValue);
		result.add(qTupdateBy);
		// result.add(qRespPhoto);

		return result;
	}

	// public String generateHash(){
	// return UUID.randomUUID().toString();
	// }

	public String generateJsonFromExistingData() throws JSONException {
		JSONObject mainObject = new JSONObject();

		JSONObject questionerResultJSONObject = new JSONObject();
		questionerResultJSONObject.put("mq_qr_resp_name", questionerResult.getRespName());
		questionerResultJSONObject.put("mq_qr_resp_address", questionerResult.getRespAddress());
		questionerResultJSONObject.put("mq_qr_resp_phone_number", questionerResult.getRespPhone());
		questionerResultJSONObject.put("mq_qr_resp_handphone", questionerResult.getRespHandPhone());
		questionerResultJSONObject.put("mq_qr_resp_office_phone", questionerResult.getRespOfficePhone());
		questionerResultJSONObject.put("mq_qr_resp_phot", questionerResult.getRespPhotoPath());
		questionerResultJSONObject.put("mq_qr_resp_ktp", questionerResult.getRespNoKTP());
		questionerResultJSONObject.put("mq_qr_resp_occupation", questionerResult.getRespOccupation());
		questionerResultJSONObject.put("mq_qr_resp_last_education", questionerResult.getRespLastEducation());
		questionerResultJSONObject.put("mq_qr_resp_age", questionerResult.getRespAge());
		questionerResultJSONObject.put("mq_qr_resp_sex", questionerResult.getRespSex());
		questionerResultJSONObject.put("mq_qr_latitude", questionerResult.getLattitude());
		questionerResultJSONObject.put("mq_qr_longitude", questionerResult.getLongitude());
		questionerResultJSONObject.put("mq_qr_resp_int_by", questionerResult.getInterviewedBy());
		questionerResultJSONObject.put("mq_qr_resp_int_date", questionerResult.getInterviewDate());
		mainObject.put("questioner_result", questionerResultJSONObject);

		JSONArray questionResultJSONArray = new JSONArray();
		ArrayList<QuestionResult> questionResultList = questionerResult.getQuestionsResultList();
		for (QuestionResult questionResult : questionResultList) {
			JSONObject questionResultJSONObject = new JSONObject();
			questionResultJSONObject.put("mq_qtr_essai_choice", questionResult.getEssayChoice());
			questionResultJSONObject.put("mq_qtr_essai", questionResult.getEssai());
			questionResultJSONObject.put("mq_qtr_id", questionResult.getQuestion().getIdFromServer());
			if (questionResult.getDetQuestion() != null)
				questionResultJSONObject.put("mq_qtr_choice_id", questionResult.getDetQuestion().getIdFromServer());

			questionResultJSONObject.put("mq_qtr_sa_ma", questionResult.getQuestion().getType().toString());
			questionResultJSONArray.put(questionResultJSONObject);
		}

		mainObject.put("question_result", questionResultJSONArray);
		return mainObject.toString();
	}
}
