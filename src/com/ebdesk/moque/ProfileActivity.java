package com.ebdesk.moque;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ebdesk.moque.database.User;
import com.ebdesk.moque.dialog.change_password;
import com.ebdesk.moque.dialog.change_password.onSubmitListener;
import com.ebdesk.moque.helpers.BitmapHelper;
import com.ebdesk.moque.helpers.JsonParser;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ProfileActivity extends FragmentActivity implements onSubmitListener  {
	
	private TextView usernameTV;
	private EditText positionET;
	private EditText firstNameET;
	private EditText lastNameET;
	private EditText addressET;
	private EditText homePhoneET;
	private EditText mobilePhoneET;
	private EditText emailET;
	private EditText companyNameET;
	private EditText officeAddressET;
	private EditText officePhoneNumberET;
	
	private EditText oldPasswordET;
	private EditText newPasswordET;
	private EditText confirmNewPasswordET;
	private ImageView userIV;
	private SessionManager session;
	private TelephonyManager telephonyManager;
	private AlertDialog exitFailedToConnectDialog;
	protected static final int CAMERA_REQUEST = 0;
	protected static final int GALLERY_PICTURE = 1;
	private Intent pictureActionIntent = null;
	Bitmap bitmap;
	
	private ImageLoader imgLoader;
	private DisplayImageOptions displayOption;
	
	private View userImageProfileContainerV;
	private View loadingLayerV;
	private View noImageLayerV;

    String selectedImagePath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		session = new SessionManager(this);
		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		
		initWidgets();
		getData();
		initImageLoader();
	}
	
	private void initImageLoader(){
		imgLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration imgLoaderConfig = ImageLoaderConfiguration.createDefault(this);
		imgLoader.init(imgLoaderConfig);
		displayOption = DisplayImageOptions.createSimple();
	}
	
	private void loadUserImage(String imgURL){
		if(!imgURL.equals("")){
			imgLoader.displayImage(imgURL, userIV, displayOption);
			loadingLayerV.setVisibility(View.GONE);
		} else {
			loadingLayerV.setVisibility(View.GONE);
			noImageLayerV.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onBackPressed() {
		saveAndFinish();
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		getMenuInflater().inflate(R.menu.profile, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			saveAndFinish();
			break;
		case R.id.change_password:
			change_password fragment1 = new change_password();   
		    fragment1.mListener = ProfileActivity.this;  
		    fragment1.show(getFragmentManager(), "");  
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initWidgets() {
		userIV = (ImageView) findViewById(R.id.user_image);
		usernameTV = (TextView) findViewById(R.id.profile_username_head);
		firstNameET = (EditText) findViewById(R.id.profile_first_name);
		lastNameET = (EditText) findViewById(R.id.profile_last_name);
		emailET = (EditText) findViewById(R.id.profile_email);
		addressET = (EditText) findViewById(R.id.profile_alamat);
		positionET = (EditText) findViewById(R.id.profile_position);
		companyNameET = (EditText) findViewById(R.id.profile_company_name);
		mobilePhoneET = (EditText) findViewById(R.id.profile_handphone_number);
		homePhoneET = (EditText) findViewById(R.id.profile_home_phone);
		officeAddressET = (EditText) findViewById(R.id.profile_office_address);
		officePhoneNumberET = (EditText) findViewById(R.id.profile_office_phone_number);


		oldPasswordET = (EditText) findViewById(R.id.profile_old_password);
		newPasswordET = (EditText) findViewById(R.id.profile_new_password);
		confirmNewPasswordET = (EditText) findViewById(R.id.profile_confirm_new_password);
		
		userImageProfileContainerV = findViewById(R.id.profile_layout_foto);
		loadingLayerV = findViewById(R.id.loading_preview_layer);
		noImageLayerV = findViewById(R.id.no_image_layer);
		noImageLayerV.setVisibility(View.GONE);
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
			.setMessage("Cannot reach server")
			.setPositiveButton("Coba lagi", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					saveAndFinish();
				}
			})
			.setNegativeButton("Keluar tanpa menyimpan", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});
		
		userImageProfileContainerV.setOnLongClickListener(imageClickListener);
		exitFailedToConnectDialog = dialogBuilder.create();
	}
	
	private OnLongClickListener imageClickListener = new OnLongClickListener() {
		
		@Override
		public boolean onLongClick(View arg0) {
			startDialogImage();
			return false;
		}
	};
	private void startDialogImage() {
	    AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(this);
	    myAlertDialog.setTitle("Pilih gambar");
	    myAlertDialog.setMessage("Pilihan metode pengambilan gambar");

	    myAlertDialog.setPositiveButton("Galeri",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface arg0, int arg1) {
	                    pictureActionIntent = new Intent(
	                            Intent.ACTION_GET_CONTENT, null);
	                    pictureActionIntent.setType("image/*");
	                    pictureActionIntent.putExtra("return-data", true);
	                    startActivityForResult(pictureActionIntent,
	                            GALLERY_PICTURE);
	                }
	            });

	    myAlertDialog.setNegativeButton("Kamera",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface arg0, int arg1) {
	                    pictureActionIntent = new Intent(
	                            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	                    startActivityForResult(pictureActionIntent,
	                            CAMERA_REQUEST);

	                }
	            });
	    myAlertDialog.show();
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == GALLERY_PICTURE) {
	        if (resultCode == RESULT_OK) {
	            if (data != null) {
	                // our BitmapDrawable for the thumbnail
	                BitmapDrawable bmpDrawable = null;
	                // try to retrieve the image using the data from the intent
	                Cursor cursor = getContentResolver().query(data.getData(),null, null, null, null);
	                if (cursor != null) {
	                    cursor.moveToFirst();
	                    int idx = cursor.getColumnIndex(ImageColumns.DATA);
	                    String fileSrc = cursor.getString(idx);
	                    bitmap = BitmapFactory.decodeFile(fileSrc); 
	                    bitmap = Bitmap.createScaledBitmap(bitmap,100, 100, false);
	                    // bmpDrawable = new BitmapDrawable(bitmapPreview);
	                    userIV.setImageBitmap(bitmap);
	                    sendPhoto(fileSrc); // upload photo
	                } else {
	                    bmpDrawable = new BitmapDrawable(getResources(), data.getData().getPath());
	                    userIV.setImageDrawable(bmpDrawable);
	                    sendPhoto(data.getData().getPath()); // upload photo
	                }
	            } else {
	                Toast.makeText(getApplicationContext(), "Cancelled",Toast.LENGTH_SHORT).show();
	            }
	        } else if (resultCode == RESULT_CANCELED) {
	            Toast.makeText(getApplicationContext(), "Cancelled",Toast.LENGTH_SHORT).show();
	        }
	    } else if (requestCode == CAMERA_REQUEST) {
	        if (resultCode == RESULT_OK) {
	            if (data.hasExtra("data")) {
	                // retrieve the bitmap from the intent
	                bitmap = (Bitmap) data.getExtras().get("data");
	                Cursor cursor = getContentResolver()
	                        .query(Media.EXTERNAL_CONTENT_URI,
	                                new String[] {
	                                        Media.DATA,
	                                        Media.DATE_ADDED,
	                                        MediaStore.Images.ImageColumns.ORIENTATION },
	                                Media.DATE_ADDED, null, "date_added ASC");
	                if (cursor != null && cursor.moveToFirst()) {
	                    do {
	                        Uri uri = Uri.parse(cursor.getString(cursor
	                                .getColumnIndex(Media.DATA)));
	                        selectedImagePath = uri.toString();
	                    } while (cursor.moveToNext());
	                    cursor.close();
	                }

	    			String userPhotoPath = BitmapHelper.saveBitmap(bitmap, session.getUsername()+".jpg");
	                Log.e("path of the image from camera ====> ", selectedImagePath);

	                bitmap = Bitmap.createScaledBitmap(bitmap, 100,100, false);
	                // update the image view with the bitmap
	                userIV.setImageBitmap(bitmap); // upload photo

                    sendPhoto(userPhotoPath);

//    	            Toast.makeText(getApplicationContext(), "1 : "+userPhotoPath,Toast.LENGTH_LONG).show();
	            } else if (data.getExtras() == null) {
	                Toast.makeText(getApplicationContext(),"No extras to retrieve!", Toast.LENGTH_SHORT).show();
	                BitmapDrawable thumbnail = new BitmapDrawable(getResources(), data.getData().getPath());
	                // update the image view with the newly created drawable
	                userIV.setImageDrawable(thumbnail);
                    sendPhoto(data.getData().getPath()); // upload photo
	            }
	        } else if (resultCode == RESULT_CANCELED) {
	            Toast.makeText(getApplicationContext(), "Cancelled",Toast.LENGTH_SHORT).show();
	        }
	    }
	}

	private void getData(){
		ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				Log.d("profile", response);
				try {
					User user = JsonParser.parseProfile(response);
					mapData(user);
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(ProfileActivity.this, "Response not recognized", Toast.LENGTH_LONG).show();
				}
			}
			
			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(ProfileActivity.this, "Cannot contact the server", Toast.LENGTH_LONG).show();
			}
		};
		
		String url = URLBuilder.buildProfileRetrieveURL(session.getUsername(), session.getHash());
		new URLTask(this, url, "Mengambil data user", listener).execute();
	}
	
	private void mapData(User user){
		
		usernameTV.setText(user.getUsername());
		firstNameET.setText(user.getFirstName());
		lastNameET.setText(user.getLastName());
		emailET.setText(user.getEmail());
		addressET.setText(user.getAlamat());
		positionET.setText(user.getPosition());
		companyNameET.setText(user.getCompanyName());
		mobilePhoneET.setText(telephonyManager.getLine1Number()!=null?telephonyManager.getLine1Number():"");
		homePhoneET.setText(user.getHomePhone());
		officeAddressET.setText(user.getOfficeAddress());
		officePhoneNumberET.setText(user.getOfficePhoneNumber());
		
		loadUserImage(user.getUserImageURL());
	}
	
	private void saveAndFinish(){
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("first_name", firstNameET.getText().toString()));
		params.add(new BasicNameValuePair("last_name", lastNameET.getText().toString()));
		params.add(new BasicNameValuePair("email", emailET.getText().toString()));
		params.add(new BasicNameValuePair("address", addressET.getText().toString()));
		params.add(new BasicNameValuePair("position", positionET.getText().toString()));
		params.add(new BasicNameValuePair("company_name", companyNameET.getText().toString()));
		params.add(new BasicNameValuePair("mobile_phone", mobilePhoneET.getText().toString()));
		params.add(new BasicNameValuePair("home_phone", homePhoneET.getText().toString()));
		params.add(new BasicNameValuePair("company_address", officeAddressET.getText().toString()));
		params.add(new BasicNameValuePair("office_phone", officePhoneNumberET.getText().toString()));
		
		ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					if(responseJson.getBoolean("success")){
						Toast.makeText(ProfileActivity.this, "Tersimpan", Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(ProfileActivity.this, "Gagal tersimpan!", Toast.LENGTH_SHORT).show();
					}
					finish();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onResultFailed(Context context) {
				exitFailedToConnectDialog.show();
			}
		};
		
		String url = URLBuilder.buildProfileSaveURL(session.getUsername(), session.getHash());
		URLTask saveProfileURLTask = new URLTask(this, url, "Menyimpan", listener);
		saveProfileURLTask.setPostParams(params);
		saveProfileURLTask.execute();
	}
	
	
	private void sendPhoto(String path) {
		
		RequestParams params = new RequestParams();
		try {
			params.put("username", session.getUsername());
			params.put("hash", session.getHash());
			params.put("photo",
			        new FileInputStream(path),
			        session.getUsername()+".jpg",
			        "image/jpeg");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		final Activity activity = this;
		AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		        @Override
		        public void onSuccess(String content) {
//		                Toast.makeText(activity, "Success: " + content, Toast.LENGTH_LONG).show();
		        }

		        @Override
		        public void onFailure(Throwable error, String content) {
//		                Toast.makeText(activity, "Failure: " + content, Toast.LENGTH_LONG).show();
		        }

		        @Override
		        public void onProgress(int position, int length) {
		                // e.g. Update a ProgressBar:
		        }
		};

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(URLBuilder.buildProfileSendPhoto(), params, responseHandler);
	}
	
	@Override  
	 public void setOnSubmitListener(String arg) {  
	  oldPasswordET.setText(arg);  
	 }  
}