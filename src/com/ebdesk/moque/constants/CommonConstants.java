package com.ebdesk.moque.constants;

public class CommonConstants {
	
	public static String PASS_INTENT_INDEX_VISIT = "index";
	public static String PASS_INTENT_IS_LAST = "isLast";
	public static String PASS_INTENT_QUESTIONER_ID = "questioner_id";
	public static String PASS_INTENT_QUESTIONER_RESULT_ID = "questioner_result_id";
	public static String PASS_INTENT_QUESTIONER_RESULT_PANEL = "questioner_result_panel";
	public static String PASS_INTENT_UUID_TRANSCATION="uuid_transaction";
	public static String PASS_INTENT_QUESTION_ID = "question_id";
	public static String PASS_INTENT_SURVEY_ID = "survey_id";
	public static String PASS_INTENT_VISIT_ID = "visit_id";
	public static String PASS_INTENT_REGISTERED_RESPONDEN_ID = "responden_id";
	public static String PASS_INTENT_QUESTION_IDs = "question_ids";
	public static String PASS_INTENT_DET_QUESTION_IDs = "det_question_ids";
	public static String PASS_INTENT_REGISTERED_RESPONDEN_NAME = "responden_name";
	public static String PASS_INTENT_REGISTERED_RESPONDEN_AGE = "responden_age";
	public static String PASS_INTENT_KISH_GRID_ID = "kish_grid_id";
	public static String PASS_INTENT_KISH_GRID_DETAIL_RESULT_ID = "kish_grid_detail_result_id";
	
	public static int RETURN_RESULT_CODE_SUCCESS = 1;
	public static int RETURN_RESULT_CODE_FAILED = -1;
	public static int REQUEST_CODE = 1;
	
	public static String SERVER_URL = "http://192.168.1.104/moque_web";
	public static String SERVER_URL_EBDESK = "http://192.168.10.80/moque_web";
	public static String SERVER_URL_CLOUD = "http://112.78.150.30:27801";
//	public static String SERVER_URL_CLOUD = "http://112.78.150.28:5281";
	
	public static String RECEIVER_FLAG_BADGE_UPDATE = "com.ebdesk.moque.UpdateBadge";
	public static String RECEIVER_FLAG_UPDATE_DOWNLOAD_PROGRESS = "com.ebdesk.moque.UpdateProgress";
	public static String RECEIVER_FLAG_UPDATE_STATUS_TARGET = "com.ebdesk.moque.UpdateStatusAndTarget";
	
	public static String PREF_NAME = "MotionSessionPref";
	
	public static String GCM_REG_ID = "registration_id";
//	public static String GCM_SENDER_ID="754285056578";
	public static String GCM_SENDER_ID="503961155212";
	public static String PROPERTY_APP_VERSION = "";
}
