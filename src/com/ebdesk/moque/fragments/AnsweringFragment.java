package com.ebdesk.moque.fragments;

import java.sql.SQLException;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.ebdesk.moque.R;
import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.customviews.RadioButtonCheckedListener;
import com.ebdesk.moque.database.DetQuestions;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.QuestionResult;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.helpers.DatabaseManager;

public class AnsweringFragment extends Fragment {
	private View mainView;
	private QuestionerResult questionerResult;
	private Question question;
	private TextView questionTV;
	
	private ImageView questionIV;
	private VideoView questionVV;
	private View questionMediaContainer;
	
	private ViewGroup optionContainer;
	private ArrayList<QuestionResult> questionResultList;
	private LayoutInflater inflater;
	private RadioButtonCheckedListener radioListener;
	
	private TextView headerTV;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handleIntent();
	}

	private void handleIntent() {
		Bundle bundle = getArguments();
		int questionerResultId = bundle.getInt(CommonConstants.PASS_INTENT_QUESTIONER_RESULT_ID, 0);
		questionerResult = DatabaseManager.getHelper(getActivity()).getQuestionerResultWithId(questionerResultId);
		
		int questionId = bundle.getInt(CommonConstants.PASS_INTENT_QUESTION_ID, 0);
		question = DatabaseManager.getHelper(getActivity()).getQuestionWithId(questionId);
		
		try {
			questionResultList = DatabaseManager.getHelper(getActivity()).getQuestionResultList(questionerResult, question);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initWidgets() {
		questionTV = (TextView) mainView.findViewById(R.id.question_text);
		questionIV = (ImageView) mainView.findViewById(R.id.question_image);
		questionVV = (VideoView) mainView.findViewById(R.id.question_video);
		questionMediaContainer = mainView.findViewById(R.id.question_media_container);
		optionContainer = (ViewGroup) mainView.findViewById(R.id.container);
		inflater = LayoutInflater.from(getActivity());
		radioListener = new RadioButtonCheckedListener();
		headerTV = (TextView) mainView.findViewById(R.id.header);
	}

	private void mapDatasToUI() {
		questionTV.setText(question.getNumber()+". "+question.getDescription());
		if (question.getImagePath() != null || question.getAudioPath()!= null || question.getVideoPath() != null){
			questionMediaContainer.setVisibility(View.VISIBLE);
			initImage();
			initAudio();
			initVideo();
		}else{
			questionMediaContainer.setVisibility(View.GONE);
		}

		switch (question.getType()) {
		case SINGLE_ANSWER:
			setupSingleOption();
			break;
		case MULTIPLE_ANSWER:
			setupMultipleOption();
			break;
		}
		
		if(question.getHeader()!=null){
			headerTV.setVisibility(View.VISIBLE);
			headerTV.setText(question.getHeader().getName());
		}else{
			headerTV.setVisibility(View.GONE);
		}
	}
	
	private void initVideo(){
		int visibility = View.GONE;
		if (question.getVideoPath() != null) {
			Log.d("Path", question.getVideoPath());
			questionVV.setVisibility(View.VISIBLE);
			questionVV.setVideoPath(question.getVideoPath());
			questionVV.setMediaController(new MediaController(getActivity()));
			questionVV.requestFocus();
			visibility = View.VISIBLE;
		} else {
			visibility = View.GONE;
		}
		questionVV.setVisibility(visibility);
	}
	
	private void initAudio() {
		int visibility = View.GONE;
		if (question.getAudioPath() != null) {
			Log.d("Path", question.getAudioPath());
			questionVV.setVisibility(View.VISIBLE);
			questionVV.setVideoPath(question.getAudioPath());
			questionVV.setMediaController(new MediaController(getActivity()));
			questionVV.requestFocus();
			visibility = View.VISIBLE;
		} else {
			visibility = View.GONE;
		}
		questionVV.setVisibility(visibility);
	}
	
	private void initImage(){
		if(question.getImagePath() != null){
			Bitmap bitmap = BitmapFactory.decodeFile(question.getImagePath());
			questionIV.setVisibility(View.VISIBLE);
			questionIV.setImageBitmap(bitmap);
		}else{
			questionIV.setVisibility(View.GONE);
		}
	}
	
	private void showToast(String message){
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

	private void setupSingleOption() {
		EditText mainET = new EditText(getActivity());
		if(question.isEssayNeeded()){
			optionContainer.addView(mainET);
		}
		String mainContent = "";
		boolean found = false;
		ArrayList<DetQuestions> options = question.getOptions();
		for (DetQuestions option : options) {
			ViewGroup childView = (ViewGroup) inflater.inflate(R.layout.single_option_item_layout, null, false);
			RadioButton radioButton = (RadioButton) childView.findViewById(R.id.option_radio_button);
			TableRow row = (TableRow) childView.findViewById(R.id.row_essai);
			TableRow imageRow = (TableRow) childView.findViewById(R.id.image_row);
			TableRow audioVideoRow = (TableRow) childView.findViewById(R.id.audio_video_row);
			TextView optionNameTV = (TextView) childView.findViewById(R.id.option_name);
			ImageView optionIV = (ImageView) childView.findViewById(R.id.image_view);
			VideoView optionVV = (VideoView) childView.findViewById(R.id.video_view);
			
			EditText optionET = (EditText) childView.findViewById(R.id.option_textfield);
			if(!option.isUsingEssay()){
				row.setVisibility(View.GONE);
			}
			
			if(option.getImagePath() == null){
				imageRow.setVisibility(View.GONE);
			}else{
				imageRow.setVisibility(View.VISIBLE);
				Bitmap bitmap = BitmapFactory.decodeFile(option.getImagePath());
				optionIV.setImageBitmap(bitmap);
			}
			
			if (option.getAudioPath() == null && option.getVideoPath() == null) {
				audioVideoRow.setVisibility(View.GONE);
			} else {
				if (option.getAudioPath() != null) {
					audioVideoRow.setVisibility(View.VISIBLE);
					optionVV.setVideoPath(option.getAudioPath());
					optionVV.setMediaController(new MediaController(getActivity()));
					optionVV.requestFocus();
				}
				
				if (option.getVideoPath() != null) {
					audioVideoRow.setVisibility(View.VISIBLE);
					optionVV.setVideoPath(option.getVideoPath());
					optionVV.setMediaController(new MediaController(getActivity()));
					optionVV.requestFocus();
				}
			}

			if (questionerResult.getState().equals(QuestionerResult.State.FINISH)) {
				radioButton.setEnabled(false);
				optionET.setEnabled(false);
			}
			
			optionNameTV.setText(option.getDesc());
			childView.setTag(option);
			radioButton.setTag(option.getId());
			
			for(QuestionResult savedQuestionResult:questionResultList){
				if (savedQuestionResult.getDetQuestion() != null) {
					if (option.getId() == savedQuestionResult.getDetQuestion().getId()) {
						radioButton.setChecked(true);
						radioButton.setSelected(true);
						
						optionET.setText(savedQuestionResult.getEssai());
					}
				}else{
					found = true;
					mainContent = savedQuestionResult.getEssai();
				}
			}
			radioListener.addMember(radioButton);
			optionContainer.addView(childView);
		}
		
		if(!found){
			mainContent = searchForEssay();
		}
		
		mainET.setText(mainContent);
		if(questionerResult.getState().equals(QuestionerResult.State.FINISH)){
			mainET.setEnabled(false);
		}
	}
	
	private String searchForEssay(){
		String result = "";
		for(QuestionResult questionResult:questionResultList){
			if(questionResult.getDetQuestion()==null){
				result += questionResult.getEssai();
			}
		}
		return result;
	}

	private void setupMultipleOption() {

		EditText mainET = new EditText(getActivity());
		if(question.isEssayNeeded()){
			optionContainer.addView(mainET);
		}
		String mainContent = "";
		boolean found = false;
		ArrayList<DetQuestions> options = question.getOptions();
		for (DetQuestions option : options) {
			ViewGroup childView = (ViewGroup) inflater.inflate(R.layout.multiple_option_item_layout, null, false);
			
			TableRow row = (TableRow) childView.findViewById(R.id.row_essai);
			TableRow imageRow = (TableRow) childView.findViewById(R.id.image_row);
			TableRow audioVideoRow = (TableRow) childView.findViewById(R.id.audio_video_row);
			
			CheckBox checkBox = (CheckBox) childView.findViewById(R.id.option_checkbox);
			TextView optionNameTV = (TextView) childView.findViewById(R.id.option_name);
			
			ImageView optionIV = (ImageView) childView.findViewById(R.id.image_view);
			VideoView optionVV = (VideoView) childView.findViewById(R.id.video_view);
			
			EditText optionET = (EditText) childView.findViewById(R.id.option_textfield);
			if(!option.isUsingEssay()){
				row.setVisibility(View.GONE);
			}
			
			if(option.getImagePath() == null){
				imageRow.setVisibility(View.GONE);
			}else{
				imageRow.setVisibility(View.VISIBLE);
				Bitmap bitmap = BitmapFactory.decodeFile(option.getImagePath());
				optionIV.setImageBitmap(bitmap);
			}
			
			if(option.getImagePath() == null){
				imageRow.setVisibility(View.GONE);
			}else{
				imageRow.setVisibility(View.VISIBLE);
				Bitmap bitmap = BitmapFactory.decodeFile(option.getImagePath());
				optionIV.setImageBitmap(bitmap);
			}
			
			if (option.getAudioPath() == null && option.getVideoPath() == null) {
				audioVideoRow.setVisibility(View.GONE);
			} else {
				if (option.getAudioPath() != null) {
					audioVideoRow.setVisibility(View.VISIBLE);
					optionVV.setVideoPath(option.getAudioPath());
					optionVV.setMediaController(new MediaController(getActivity()));
					optionVV.requestFocus();
				}
				
				if (option.getVideoPath() != null) {
					audioVideoRow.setVisibility(View.VISIBLE);
					optionVV.setVideoPath(option.getVideoPath());
					optionVV.setMediaController(new MediaController(getActivity()));
					optionVV.requestFocus();
				}
			}

			if (questionerResult.getState().equals(QuestionerResult.State.FINISH)) {
				checkBox.setEnabled(false);
				optionET.setEnabled(false);
			}
			
			optionNameTV.setText(option.getDesc());
			childView.setTag(option);
			checkBox.setTag(option.getId());
			
			for(QuestionResult savedQuestionResult:questionResultList){
				if (savedQuestionResult.getDetQuestion() != null) {
					if (option.getId() == savedQuestionResult.getDetQuestion().getId()) {
						checkBox.setChecked(true);
						checkBox.setSelected(true);
						
						optionET.setText(savedQuestionResult.getEssai());
					}
				}else{
					found = true;
					mainContent = savedQuestionResult.getEssai();
				}
			}
			optionContainer.addView(childView);
		}
		
		if(!found){
			mainContent = searchForEssay();
		}
		
		mainET.setText(mainContent);
		if(questionerResult.getState().equals(QuestionerResult.State.FINISH)){
			mainET.setEnabled(false);
		}
	}
	
	public void saveAnswer(){
		switch (question.getType()) {
		case SINGLE_ANSWER:
			saveSingleOption();
			break;
		case MULTIPLE_ANSWER:
			saveMultipleOption();
			break;
		}
	}
	
	private void saveSingleOption(){
		ArrayList<QuestionResult> questionResultList = new ArrayList<QuestionResult>();
		for(int i=0;i<optionContainer.getChildCount();i++){
			View currentView = optionContainer.getChildAt(i);
			if(currentView instanceof EditText){
				EditText curET = (EditText) currentView;
				String content = curET.getText().toString();

				if (!content.equals("")) {
					QuestionResult questionResult = new QuestionResult();
					questionResult.setEssai(content);
					questionResult.setQuestion(question);
					questionResult.setQuestionerResult(questionerResult);

					questionResultList.add(questionResult);
				}
				
			}else{
				ViewGroup parentView = (ViewGroup) currentView;
				RadioButton curRB = (RadioButton) parentView.findViewById(R.id.option_radio_button);
				if(curRB.isChecked()){
					EditText curET = (EditText) parentView.findViewById(R.id.option_textfield);
					String content = curET.getText().toString();
					
					DetQuestions detQuestion = (DetQuestions) parentView.getTag();
					
					if (detQuestion.isUsingEssay() && content.equals("")) {
						showToast("Jawaban tidak disimpan, silahkan isi essai nya");
					} else {
						QuestionResult questionResult = new QuestionResult();
						questionResult.setEssai(content);
						questionResult.setQuestion(question);
						questionResult.setQuestionerResult(questionerResult);
						questionResult.setDetQuestion(detQuestion);

						questionResultList.add(questionResult);
					}
				}
			}
		}
		try {
			DatabaseManager.getHelper(getActivity()).insertQuestionResult(questionResultList, questionerResult, question);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void saveMultipleOption(){
		ArrayList<QuestionResult> questionResultList = new ArrayList<QuestionResult>();
		for(int i=0;i<optionContainer.getChildCount();i++){
			View currentView = optionContainer.getChildAt(i);
			if(currentView instanceof EditText){
				EditText curET = (EditText) currentView;
				String content = curET.getText().toString();

				if (!content.equals("")) {
					QuestionResult questionResult = new QuestionResult();
					questionResult.setEssai(content);
					questionResult.setQuestion(question);
					questionResult.setQuestionerResult(questionerResult);

					questionResultList.add(questionResult);
				}
				
			}else{
				ViewGroup parentView = (ViewGroup) currentView;
				CheckBox curCB = (CheckBox) parentView.findViewById(R.id.option_checkbox);
				if(curCB.isChecked()){
					EditText curET = (EditText) parentView.findViewById(R.id.option_textfield);
					String content = curET.getText().toString();
					
					DetQuestions detQuestion = (DetQuestions) parentView.getTag();
					if (detQuestion.isUsingEssay() && content.equals("")) {
						showToast("Jawaban tidak disimpan, silahkan isi essai nya");
					} else {
						QuestionResult questionResult = new QuestionResult();
						questionResult.setEssai(content);
						questionResult.setQuestion(question);
						questionResult.setQuestionerResult(questionerResult);
						questionResult.setDetQuestion(detQuestion);

						questionResultList.add(questionResult);
					}
				}
			}
		}
		try {
			DatabaseManager.getHelper(getActivity()).insertQuestionResult(questionResultList, questionerResult, question);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mainView = (ViewGroup) inflater.inflate(R.layout.fragment_answering_layout, null, false);
		initWidgets();
		mapDatasToUI();
		return mainView;
	}

}
