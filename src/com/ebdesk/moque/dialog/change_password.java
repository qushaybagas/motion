package com.ebdesk.moque.dialog;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ebdesk.moque.R;
import com.ebdesk.moque.helpers.JsonParser;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;

public class change_password extends DialogFragment {

	private SessionManager session;
	Button mButton;
	EditText oldPasswordET;
	EditText newPasswordET;
	EditText confirmNewPasswordET;
	public onSubmitListener mListener;
	public String oldPassword = "";
	public String newPassword = "";
	public String confirmNewPassword = "";

	public interface onSubmitListener {
		void setOnSubmitListener(String arg);
	}

 @Override
 	public Dialog onCreateDialog(Bundle savedInstanceState) {
	 final Dialog dialog = new Dialog(getActivity());

	 session = new SessionManager(getActivity());
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setContentView(R.layout.dialog_change_password);
		  dialog.getWindow().setBackgroundDrawable(
		    new ColorDrawable(Color.TRANSPARENT));
		  dialog.show();
  mButton = (Button) dialog.findViewById(R.id.profile_save_password);
  oldPasswordET = (EditText) dialog.findViewById(R.id.profile_old_password);
  newPasswordET = (EditText) dialog.findViewById(R.id.profile_new_password);
  confirmNewPasswordET = (EditText) dialog.findViewById(R.id.profile_confirm_new_password);
  oldPasswordET.setText(oldPassword);
  newPasswordET.setText(newPassword);
  confirmNewPasswordET.setText(confirmNewPassword);
  mButton.setOnClickListener(new OnClickListener() {

   @Override
   public void onClick(View v) {
	   oldPassword=oldPasswordET.getText().toString().trim();
	   newPassword=newPasswordET.getText().toString().trim();
	   confirmNewPassword=confirmNewPasswordET.getText().toString().trim();

	   boolean isContainNumber = newPassword.matches(".*\\d.*");
	   boolean isContainCaps = newPassword.matches(".*[A-Z].*");
	   
	   if(oldPassword.equals("")||newPassword.equals("")||confirmNewPassword.equals("")){
		   Toast.makeText(getActivity(), "Semuanya harus diisi...", Toast.LENGTH_SHORT).show();
		   clearData();
		   oldPasswordET.requestFocus();
	   }else{
		   if (isContainNumber && isContainCaps && newPassword.length() >= 8){

			   if(!newPassword.equals(confirmNewPassword)){
				   Toast.makeText(getActivity(), "Password baru tidak cocok...", Toast.LENGTH_SHORT).show();
				   clearData();
				   oldPasswordET.requestFocus();
			   }else{
				   updatePassword();
			   }
		   }else{
			   Toast.makeText(getActivity(), "Password harus terdapat huruf kapital, angka dan minimal 8 karakter!", Toast.LENGTH_SHORT).show();
			   
		   }
	   }
//    mListener.setOnSubmitListener(mEditText.getText().toString());
    dismiss();
   }
  });
  return dialog;
 }
 
 private void clearData(){
	 oldPasswordET.setText("");
	 newPasswordET.setText("");
	 confirmNewPasswordET.setText("");
 }
 
 private void updatePassword(){
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("old_password", oldPasswordET.getText().toString()));
		params.add(new BasicNameValuePair("new_password", newPasswordET.getText().toString()));
		
		ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					if(responseJson.getBoolean("success")){
						JsonParser.parseChangePassword(responseJson.getJSONObject("value"), context);
					}else{
						Toast.makeText(context, "Password tidak berubah", Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(context, "Password tidak berubah", Toast.LENGTH_LONG).show();
			}
		};
		
		String url = URLBuilder.buildProfileChangePassword(session.getUsername(), session.getHash());
		URLTask saveProfileURLTask = new URLTask(getActivity(), url, "Mengganti Password", listener);
		saveProfileURLTask.setPostParams(params);
		saveProfileURLTask.execute();
	}
}
