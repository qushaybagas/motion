package com.ebdesk.moque.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.ebdesk.moque.QuestionerListActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmRegistration {

	
	public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private final static String PREF_NAME = "MotionGCMPref";
    
    String SENDER_ID = "503961155212";
    
    static final String TAG = "GCMDemo";
    
    //TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;
    String regid;
    
    public GcmRegistration(Context contex){
    	this.context = contex;
    	prefs = context.getSharedPreferences("MotionSessionPref", Context.MODE_PRIVATE);
    }
    
    //simpan ID GCm ke shared preference
    public void saveGCMId(){
		if(isGooglePlaySupported()){
			gcm = GoogleCloudMessaging.getInstance(context);
			regid = getRegistrationId(context);

            if (regid.equals("")) {
                registerInBackground();
            }
		}
    }
    
    // generate ID GCM dari GCM Server
  	public String getRegistrationId(Context context) {
  	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
  	    if (registrationId.equals("")) {
  	        Log.i(TAG, "Registration not found.");
  	        return "";
  	    }
  	    // Check if app was updated; if so, it must clear the registration ID
  	    // since the existing regID is not guaranteed to work with the new
  	    // app version.
  	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
  	    int currentVersion = getAppVersion(context);
  	    if (registeredVersion != currentVersion) {
  	        Log.i(TAG, "App version changed.");
  	        return "";
  	    }
  	    return registrationId;
  	}
  	
  	public static int getAppVersion(Context context) {
  	    try {
  	        PackageInfo packageInfo = context.getPackageManager()
  	                .getPackageInfo(context.getPackageName(), 0);
  	        return packageInfo.versionCode;
  	    } catch (NameNotFoundException e) {
  	        // should never happen
  	        throw new RuntimeException("Could not get package name: " + e);
  	    }
  	}
  	
  	private boolean isGooglePlaySupported(){
  		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
  		if(resultCode != ConnectionResult.SUCCESS){
  			if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
  				GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, 9000).show();
  			}else{
//  				finish();
  			}
  		}
  		return true;
  	}
  	
  	private void registerInBackground() {
  		new Registor().execute();
  	}
  	
  	class Registor extends AsyncTask<Void, Void, String>{

  		@Override
  		protected String doInBackground(Void... arg0) {
  			String msg = "";
              try {
                  if (gcm == null) {
                      gcm = GoogleCloudMessaging.getInstance(context);
                  }
                  regid = gcm.register(SENDER_ID);
                  msg = "Device registered, registration ID=" + regid;

                  // You should send the registration ID to your server over HTTP,
                  // so it can use GCM/HTTP or CCS to send messages to your app.
                  // The request to your server should be authenticated if your app
                  // is using accounts.
                  sendRegistrationIdToBackend();

                  // For this demo: we don't need to send it because the device
                  // will send upstream messages to a server that echo back the
                  // message using the 'from' address in the message.

                  // Persist the regID - no need to register again.
                  storeRegistrationId(context, regid);
              } catch (IOException ex) {
                  msg = "Error :" + ex.getMessage();
                  // If there is an error, don't just keep trying to register.
                  // Require the user to click a button again, or perform
                  // exponential back-off.
              }
              return msg;
  		}

  		@Override
  		protected void onPostExecute(String msg) {
  		}
  		
  	}
  	
  	private void sendRegistrationIdToBackend() {
  		SessionManager session = new SessionManager(context);
  	    ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				Intent intent = new Intent(context, QuestionerListActivity.class);
				context.startActivity(intent);
			}
			
			@Override
			public void onResultFailed(Context context) {
				
			}
		};
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", session.getUsername()));
		params.add(new BasicNameValuePair("hash", session.getHash()));
		params.add(new BasicNameValuePair("gcm_id", regid));
		
		String url = URLBuilder.buildProfileGCMRegister();
		URLTask registrator = new URLTask(context, url, "Register GCM", listener);
		registrator.setPostParams(params);
		registrator.execute();
  	}
  	
  	private void storeRegistrationId(Context context, String regId) {
  	    int appVersion = getAppVersion(context);
  	    Log.i(TAG, "Saving regId on app version " + appVersion);
  	    SharedPreferences.Editor editor = prefs.edit();
  	    editor.putString(PROPERTY_REG_ID, regId);
  	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
  	    editor.commit();
  	}
}
