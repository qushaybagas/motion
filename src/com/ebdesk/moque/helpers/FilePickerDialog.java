package com.ebdesk.moque.helpers;

import java.io.File;

import com.ebdesk.moque.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class FilePickerDialog extends Dialog {
	private Context context;
	private View mainView;
	
	private ListView dirLV;
	private Button selectDirBtn;

	public FilePickerDialog(Context context) {
		super(context);
		this.context = context;
		initWidgets();
	}
	
	private void initWidgets(){
		mainView = LayoutInflater.from(context).inflate(R.layout.dialog_file_directory, null, false);
		dirLV = (ListView) mainView.findViewById(R.id.directory_list);
		
	}

	private void initData() {
		String externalDirPath = DirectoryHelper.getExternalDirPath();
		openDirectory(externalDirPath);
	}

	private void openDirectory(String dirPath) {
		File file = new File(dirPath);
		String fileList[] = file.list();
	}

}
