package com.ebdesk.moque.helpers;

import java.util.HashMap;

import com.ebdesk.moque.LoginActivity;
import com.ebdesk.moque.constants.CommonConstants;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";

	// User name (make variable public to access from outside)
	public static final String KEY_USERNAME = "username";

	// Email address (make variable public to access from outside)
	public static final String KEY_PASSWORD = "password";

	public static final String KEY_HASH = "hash";

	// User name (make variable public to access from outside)
	public static final String KEY_LATITUDE = "latitude";

	// Email address (make variable public to access from outside)
	public static final String KEY_LONGITUDE = "longitude";

	// Constructor
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(CommonConstants.PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Create login session
	 * */
	public void createLoginSession(String username, String password) {
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);

		// Storing name in pref
		editor.putString(KEY_USERNAME, username);

		// Storing email in pref
		editor.putString(KEY_PASSWORD, password);

		// commit changes
		editor.commit();
	}
	
	public void getGCMRegistrationID(){
		
	}

	public void createPosition(Double lat, Double lon) {
		editor.putString(KEY_LATITUDE, lat.toString());
		editor.putString(KEY_LONGITUDE, lon.toString());
		editor.commit();
	}

	/**
	 * Check login method wil check user login status If false it will redirect
	 * user to login page Else won't do anything
	 * */
	public void checkLogin() {
		// Check login status
		if (!this.isLoggedIn()) {
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			_context.startActivity(i);
		}

	}

	public void saveUsername(String username) {
		editor.putString(KEY_USERNAME, username);
		editor.putBoolean(IS_LOGIN, true);
		editor.commit();
	}

	public void saveHash(String hash) {
		editor.putString(KEY_HASH, hash);
		editor.commit();
	}

	public void saveLatitude(double lat) {
		editor.putString(KEY_LATITUDE, Double.valueOf(lat).toString());
		editor.commit();
	}

	public void saveLongitude(double lon) {
		editor.putString(KEY_LONGITUDE, Double.valueOf(lon).toString());
		editor.commit();
	}

	public String getHash() {
		return pref.getString(KEY_HASH, "");
	}

	public String getUsername() {
		return pref.getString(KEY_USERNAME, "");
	}

	public double getLatitude() {
		String latitudeString = pref.getString(KEY_LATITUDE, "0");
		double latitude = Double.parseDouble(latitudeString);
		return latitude;
	}

	public double getLongitude() {
		String longitudeString = pref.getString(KEY_LONGITUDE, "0");
		double longitude = Double.parseDouble(longitudeString);
		return longitude;
	}
	
	public void clearSession(){
		String gcmId = getGCMId();
		
		editor.clear();
		editor.commit();
		
		saveGCMId(gcmId);
	}
	
	public void saveGCMId(String gcmId){
		editor.putString(CommonConstants.GCM_REG_ID, gcmId);
		editor.commit();
	}

	public String getGCMId() {
		return pref.getString(CommonConstants.GCM_REG_ID, "");
	}
	
	public int getAPPVersion(){
		return pref.getInt(CommonConstants.PROPERTY_APP_VERSION, 0);
	}
	
	public void saveAPPVersion(int version){
		editor.putInt(CommonConstants.PROPERTY_APP_VERSION, version);
		editor.commit();
	}

	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		// user name
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));

		// user email id
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));

		// return user
		return user;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser() {
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();

		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, LoginActivity.class);
		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
		_context.startActivity(i);
	}

	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
}