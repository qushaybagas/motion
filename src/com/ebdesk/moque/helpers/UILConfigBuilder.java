package com.ebdesk.moque.helpers;

import android.content.Context;

import com.ebdesk.moque.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class UILConfigBuilder {
	
	public static ImageLoaderConfiguration getImageLoaderConfig(Context context){
		return ImageLoaderConfiguration.createDefault(context);
	}
	
	public static DisplayImageOptions getDisplayOptions(){
		return new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.onload_image)
		.showImageForEmptyUri(R.drawable.no_image)
		.showImageOnFail(R.drawable.no_image).build();
	}
	
}
