package com.ebdesk.moque.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class URLTask extends AsyncTask<Void, Void, String> {
	private String url;
	private ResultListener listener;
	private String message;
	private ArrayList<NameValuePair> nameValuePairs;
	private Context context;
	private ProgressDialog dialog;

	public URLTask(Context context, String url, String message, ResultListener resListener) {
		this.url = url;
		this.message = message;
		this.listener = resListener;
		this.context = context;
	}

	public void setPostParams(ArrayList<NameValuePair> params) {
		this.nameValuePairs = params;
	}

	@Override
	protected String doInBackground(Void... arg0) {
		String response = "";
		try {
			if (nameValuePairs == null) {
				response = doGetRequest();
			} else {
				response = doPostRequest();
			}
			Log.d("urlresponse", response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private String doPostRequest() throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = httpClient.execute(post);
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer sb = new StringBuffer();
		String line;
		while((line = reader.readLine())!=null){
			sb.append(line);
		}
		return sb.toString();
	}

	private String doGetRequest() throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse response = httpClient.execute(get);
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer sb = new StringBuffer();
		String line;
		while((line = reader.readLine())!=null){
			sb.append(line);
		}
		Log.d("doGet", sb.toString());
		return sb.toString();
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (dialog != null && dialog.isShowing())
			dialog.dismiss();
		Log.d("onPost", result);
		if (result.equals("")) {
			listener.onResultFailed(context);
		} else {
			listener.onResultSuccess(context, result);
		}
	}

	@Override
	protected void onPreExecute() {
		if (!message.equals(null) && !message.equals("")) {
			dialog = new ProgressDialog(context);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
		super.onPreExecute();
	}

}
