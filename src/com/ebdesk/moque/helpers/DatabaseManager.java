package com.ebdesk.moque.helpers;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import android.content.Context;

public class DatabaseManager {
	
	private static DatabaseHelper dbHelper;
	
	public static DatabaseHelper getHelper(Context context){
		if(dbHelper==null){
			dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		
		if(!dbHelper.isOpen()){
			releaseHelper();
			dbHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		return dbHelper;
	}
	
	public static void releaseHelper(){
		OpenHelperManager.releaseHelper();
	}
	
}
