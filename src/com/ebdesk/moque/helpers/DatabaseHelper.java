package com.ebdesk.moque.helpers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.ebdesk.moque.database.BadgeNotif;
import com.ebdesk.moque.database.DetQuestions;
import com.ebdesk.moque.database.Header;
import com.ebdesk.moque.database.KishGridDetailResult;
import com.ebdesk.moque.database.KishGridResult;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.QuestionResult;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.database.Quota;
import com.ebdesk.moque.database.QuotaQuestioner;
import com.ebdesk.moque.database.QuotaQuestionerResult;
import com.ebdesk.moque.database.Transaction;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	private RuntimeExceptionDao<DetQuestions, String> detQuestionsDao;
	private RuntimeExceptionDao<Header, String> headerDao;
	private RuntimeExceptionDao<Question, String> questionDao;
	private RuntimeExceptionDao<Questioner, String> questionerDao;
	private RuntimeExceptionDao<QuestionerResult, String> questionerResultDao;
	private RuntimeExceptionDao<QuestionResult, String> questionResultDao;
	private RuntimeExceptionDao<Quota, String> quotaDao;
	private RuntimeExceptionDao<QuotaQuestioner, String> quotaQuestionerDao;
	private RuntimeExceptionDao<QuotaQuestionerResult, String> quotaQuestionerResultDao;
	private RuntimeExceptionDao<Transaction, String> transactionDao;
	private RuntimeExceptionDao<KishGridResult, String> kishGridResultDao;
	private RuntimeExceptionDao<KishGridDetailResult, String> kishGridDetailResultDao;
	private RuntimeExceptionDao<BadgeNotif, String> badgeNotifyDao;

	public DatabaseHelper(Context context) {
		super(context, "moque.db", null, 1);
		try {
			initDaos();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public DatabaseHelper(Context context, String databaseName, CursorFactory factory, int databaseVersion) {
		super(context, databaseName, factory, databaseVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDB, ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, DetQuestions.class);
			TableUtils.createTable(connectionSource, Header.class);
			TableUtils.createTable(connectionSource, Question.class);
			TableUtils.createTable(connectionSource, Questioner.class);
			TableUtils.createTable(connectionSource, QuestionerResult.class);
			TableUtils.createTable(connectionSource, QuestionResult.class);
			TableUtils.createTable(connectionSource, Quota.class);
			TableUtils.createTable(connectionSource, QuotaQuestioner.class);
			TableUtils.createTable(connectionSource, QuotaQuestionerResult.class);
			TableUtils.createTable(connectionSource, Transaction.class);
			TableUtils.createTable(connectionSource, KishGridResult.class);
			TableUtils.createTable(connectionSource, KishGridDetailResult.class);
			TableUtils.createTable(connectionSource, BadgeNotif.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initDaos() throws SQLException {
		detQuestionsDao = getRuntimeExceptionDao(DetQuestions.class);
		headerDao = getRuntimeExceptionDao(Header.class);
		questionDao = getRuntimeExceptionDao(Question.class);
		questionerDao = getRuntimeExceptionDao(Questioner.class);
		questionerResultDao = getRuntimeExceptionDao(QuestionerResult.class);
		questionResultDao = getRuntimeExceptionDao(QuestionResult.class);
		quotaDao = getRuntimeExceptionDao(Quota.class);
		quotaQuestionerDao = getRuntimeExceptionDao(QuotaQuestioner.class);
		quotaQuestionerResultDao = getRuntimeExceptionDao(QuotaQuestionerResult.class);
		transactionDao = getRuntimeExceptionDao(Transaction.class);
		kishGridResultDao = getRuntimeExceptionDao(KishGridResult.class);
		kishGridDetailResultDao = getRuntimeExceptionDao(KishGridDetailResult.class);
		badgeNotifyDao = getRuntimeExceptionDao(BadgeNotif.class);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2, int arg3) {
		
	}
	
	public void updateBadge(){
		Random random = new Random();
		int count = random.nextInt(5);

		BadgeNotif badge = getBadgeObject();
		
		if(badge==null){
			badge = new BadgeNotif();
			badge.setNumber(String.valueOf(count));
			badgeNotifyDao.create(badge);
		}else{
			int lastNumber = Integer.parseInt(badge.getNumber());
			badge.setNumber(String.valueOf(count+lastNumber));
			badgeNotifyDao.update(badge);
		}
	}
	
	public BadgeNotif getBadgeObject(){
		ArrayList<BadgeNotif> notifList = (ArrayList<BadgeNotif>) badgeNotifyDao.queryForAll();
		BadgeNotif result = null;
		if(notifList != null){
			if(notifList.size()>0){
				result = notifList.get(0);
			}
		}
		
		return result;
	}
	
	public String getBadgeNotif(){
		ArrayList<BadgeNotif> notifList = (ArrayList<BadgeNotif>) badgeNotifyDao.queryForAll();
		String result = "0";
		if(notifList != null){
			if(notifList.size()>0){
				result = notifList.get(0).getNumber();
			}
		}
		
		return result;
	}
	
	public void clearBadge() throws SQLException{
		TableUtils.clearTable(getConnectionSource(), BadgeNotif.class);
	}
	
	private RuntimeExceptionDao<KishGridDetailResult, String> getKishGridDetailResultDao(){
		return kishGridDetailResultDao;
	}
	
	private RuntimeExceptionDao<KishGridResult, String> getKishGridResultDao(){
		return kishGridResultDao;
	}

	private RuntimeExceptionDao<DetQuestions, String> getDetQuestionsDao() {
		return detQuestionsDao;
	}
	
	private RuntimeExceptionDao<QuestionerResult, String> getQuestionerResultDao(){
		return questionerResultDao;
	}
	
	private RuntimeExceptionDao<Header, String> getHeaderDao() {
		return headerDao;
	}
	
	private RuntimeExceptionDao<Question, String> getQuestionDao() {
		return questionDao;
	}
	
	private RuntimeExceptionDao<Questioner, String> getQuestionerDao() {
		return questionerDao;
	}
	
	private RuntimeExceptionDao<QuestionResult, String> getQuestionResultDao() {
		return questionResultDao;
	}
	
	private RuntimeExceptionDao<Quota, String> getQuotaDao() {
		return quotaDao;
	}
	
	private RuntimeExceptionDao<QuotaQuestioner, String> getQuotaQuestionerDao() {
		return quotaQuestionerDao;
	}
	
	private RuntimeExceptionDao<QuotaQuestionerResult, String> getQuotaQuestionerResultDao() {
		return quotaQuestionerResultDao;
	}
	
	private RuntimeExceptionDao<Transaction, String> getTransactionDao() {
		return transactionDao;
	}

	/*public Answer getAnswerOfQuestionAndSurvey(Question question, Survey survey) throws SQLException {
		Where<Answer, String> answerWhere = getAnswerDao().queryBuilder().where();
		answerWhere.eq("question_id", question.getQuestionId());
		answerWhere.and();
		answerWhere.eq("survey_id", survey.getSurveyId());
		List<Answer> result = answerWhere.query();
		if (result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}*/
	
	//HEADER
	public void insertHeader(Header header){
		getHeaderDao().create(header);
	}
	//END OF HEADER
	
	//QUOTA
	public void insertQuota(Quota quota){
		getQuotaDao().create(quota);
	}
	//END OF QUOTA
	
	//QUESTIONER
	public boolean isQuestionerExist(Questioner questioner, String username) throws SQLException{
		Where<Questioner, String> where = getQuestionerDao().queryBuilder().where();
		where.eq("idFromServer", questioner.getIdFromServer());
		where.and();
		where.eq("username", username);
		List<Questioner> result = where.query();
		if(result.size()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean insertQuestioner(Questioner questioner, String username) throws SQLException{
		boolean result = false;
		if(!isQuestionerExist(questioner, username)){
			getQuestionerDao().create(questioner);
			result = true;
		}
		return result;
	}
	public Questioner getQuestionerWithID(int id){
		String questionerId = String.valueOf(id);
		return getQuestionerDao().queryForId(questionerId);
	}
	
	public void updateQuestioner(Questioner questioner){
		getQuestionerDao().update(questioner);
	}
	
	public Questioner getQuestionerWithServerID(int id) throws SQLException{
		Where<Questioner, String> questionerWhere = getQuestionerDao().queryBuilder().where();
		questionerWhere.eq("idFromServer", id);
		ArrayList<Questioner> questionerList = (ArrayList<Questioner>) questionerWhere.query();
		if(questionerList == null){
			return null;
		}else{
			return questionerList.get(0);
		}
	}
	
	public ArrayList<Questioner> getAllQuestioner(){
		return (ArrayList<Questioner>) getQuestionerDao().queryForAll();
	}
	
	public ArrayList<Questioner> getAllQuestionerOfUser(String username) throws SQLException{
		Where<Questioner, String> questionerWhere = getQuestionerDao().queryBuilder().orderBy("createdDate", false).where();
		questionerWhere.eq("username", username);
		ArrayList<Questioner> result = (ArrayList<Questioner>) questionerWhere.query();
		
		return result;
	}
	
	public void updateQuestion(Question question){
		getQuestionDao().update(question);
	}
	
	public int getCountTotalQuestioner(){
		ArrayList<Questioner> questionerList = getAllQuestioner();
		return questionerList.size();
	}
	
	public int getCountTotalQuestionerOfUser(String username) throws SQLException{
		ArrayList<Questioner> questionerList = getAllQuestionerOfUser(username);
		return questionerList.size();
	}
	
	public int getCountSpecificQuestioner(Questioner.State state) throws SQLException{
		Where<Questioner, String> questionerWhere = getQuestionerDao().queryBuilder().where();
		questionerWhere.eq("state", state);
		ArrayList<Questioner> result = (ArrayList<Questioner>) questionerWhere.query();
		if(result != null){
			return result.size();
		}else{
			return 0;
		}
	}
	
	public int getCountSpecificQuestionerOfUser(Questioner.State state, String username) throws SQLException{
		Where<Questioner, String> questionerWhere = getQuestionerDao().queryBuilder().where();
		questionerWhere.eq("state", state);
		questionerWhere.and();
		questionerWhere.eq("username", username);
		ArrayList<Questioner> result = (ArrayList<Questioner>) questionerWhere.query();
		if(result != null){
			return result.size();
		}else{
			return 0;
		}
	}
	//END OF QUESTIONER
	//QUESTION
	public void insertQuestion(Question question){
		getQuestionDao().create(question);
	}
	
	public ArrayList<Question> getQuestionOfQuestioner(Questioner questioner) throws SQLException{
		Where<Question, String> questionWhere = getQuestionDao().queryBuilder().where();
		questionWhere.eq("questioner_id", questioner.getId());
		ArrayList<Question> questionList = (ArrayList<Question>) questionWhere.query();
		if(questionList==null){
			return new ArrayList<Question>();
		}else{
			return questionList;
		}
	}
	
	public ArrayList<Question> getQuestionOfQuestioner(int questionerId) throws SQLException{
		Where<Question, String> questionWhere = getQuestionDao().queryBuilder().where();
		questionWhere.eq("questioner_id", questionerId);
		ArrayList<Question> questionList = (ArrayList<Question>) questionWhere.query();
		if(questionList==null){
			return new ArrayList<Question>();
		}else{
			return questionList;
		}
	}
	
	public Question getQuestionWithId(int questionId){
		String questionIdString = String.valueOf(questionId);
		return getQuestionDao().queryForId(questionIdString);
	}
	//END OF QUESTION
	//DETQUESTIONS
		public void insertDetQuestions(DetQuestions detQ){
			getDetQuestionsDao().create(detQ);
		}
		
		public DetQuestions getDetQuestionWithId(int detQuestionId){
			String detQuestionIdString = String.valueOf(detQuestionId);
			return getDetQuestionsDao().queryForId(detQuestionIdString);
		}
		
		public void updateDetQuestion(DetQuestions detQuestion){
			getDetQuestionsDao().update(detQuestion);
		}
		//END OF DETQUESTIONS
	//QUESTIONER RESULT
	public void insertQuestionerResult(QuestionerResult qResult){
		getQuestionerResultDao().create(qResult);
	}
	
	public QuestionerResult getQuestionerResultWithId(int questionId){
		String questionerResultIdString = String.valueOf(questionId);
		return getQuestionerResultDao().queryForId(questionerResultIdString);
	}
	
	public void updateQuestionerResult(QuestionerResult qResult){
		getQuestionerResultDao().update(qResult);
	}
	
	public void deleteQuestionerResult(QuestionerResult questionerResult){
		getQuestionerResultDao().delete(questionerResult);
	}
	
	public ArrayList<QuestionerResult> getQuestionerResultFinishedNotSent(int questionerId) throws SQLException{
		Where<QuestionerResult, String> qRWhere = getQuestionerResultDao().queryBuilder().where();
		qRWhere.eq("questioner_id", questionerId);
		qRWhere.and();
		qRWhere.eq("sent", false);
		qRWhere.and();
		qRWhere.eq("MQ_QR_STATE", QuestionerResult.State.FINISH);
		return (ArrayList<QuestionerResult>) qRWhere.query();
	}
	
	public ArrayList<QuestionerResult> getQuestionerResultFinishedSent(int questionerId) throws SQLException{
		Where<QuestionerResult, String> qRWhere = getQuestionerResultDao().queryBuilder().where();
		qRWhere.eq("questioner_id", questionerId);
		qRWhere.and();
		qRWhere.eq("sent", true);
		qRWhere.and();
		qRWhere.eq("MQ_QR_STATE", QuestionerResult.State.FINISH);
		return (ArrayList<QuestionerResult>) qRWhere.query();
	}
	//END OF QUESTIONER RESULT
	//KISHGRIDRESULT
	public void insertKishGridResult(KishGridResult kishGridResult){
		getKishGridResultDao().create(kishGridResult);
	}
	
	public void deleteKishGridResult(KishGridResult kishGridResult){
		getKishGridResultDao().delete(kishGridResult);
	}
	
	public KishGridResult getKishGridResultWithId(int kishGridResultId){
		String kishGridResultIdString = String.valueOf(kishGridResultId);
		return getKishGridResultDao().queryForId(kishGridResultIdString);
	} 
	//END OF KISHGRIDRESULT
	//KISH GRID DETAIL RESULT
	public void insertKishGridDetailResult(KishGridDetailResult kishGridDetailResult){
		getKishGridDetailResultDao().create(kishGridDetailResult);
	}
	
	public void updateKishGridDetailResult(KishGridDetailResult kishGridDetailResult){
		getKishGridDetailResultDao().update(kishGridDetailResult);
	}
	
	public void deleteKishGridDetailResult(KishGridDetailResult kishGridDetailResult){
		getKishGridDetailResultDao().delete(kishGridDetailResult);
	}
	
	public KishGridDetailResult getKishGridDetailResultWithId(int kishGridDetailResultId){
		String kishGridDetailResultIdString = String.valueOf(kishGridDetailResultId);
		return getKishGridDetailResultDao().queryForId(kishGridDetailResultIdString);
	} 
	//END OF KISH GRID DETAIL RESULT
	//QUESTION RESULT
	public void insertQuestionResult(QuestionResult questionResult, QuestionerResult questionerResult, Question question) throws SQLException {
		deleteQuestionResultFromQuestionerResultAndQuestion(questionerResult, question);
		getQuestionResultDao().create(questionResult);
		setFinishLine(questionerResult);
	}
	
	public boolean isQuestionAnswered(QuestionerResult questionerResult, Question question) throws SQLException{
		ArrayList<QuestionResult> questionResultList = getQuestionResultList(questionerResult, question);
		boolean answered = false;
		if (questionResultList != null) {
			if (questionResultList.size() > 0) {
				answered = true;
			}
		}
		return answered;
	}
	
	public void setFinishLine(QuestionerResult questionerResult) throws SQLException{
		ArrayList<Question> questionList = questionerResult.getQuestioner().getQuestions();
		boolean questionerResultFinish = true;
		for(Question question:questionList){
			questionerResultFinish = questionerResultFinish && isQuestionAnswered(questionerResult, question);
		}
		
		if(questionerResultFinish){
			questionerResult.setState(QuestionerResult.State.FINISH);
			updateQuestionerResult(questionerResult);
		}
	}
	
	public void insertQuestionResult(ArrayList<QuestionResult> questionResultList, QuestionerResult questionerResult, Question question) throws SQLException {
		deleteQuestionResultFromQuestionerResultAndQuestion(questionerResult, question);
		for(QuestionResult questionResult:questionResultList){
			getQuestionResultDao().create(questionResult);
		}
		setFinishLine(questionerResult);
	}
	
	public QuestionResult getQuestionResult(int questionResultId){
		String questionResultIdString = String.valueOf(questionResultId);
		return getQuestionResultDao().queryForId(questionResultIdString);
	}
	
	public ArrayList<QuestionResult> getQuestionResultList(QuestionerResult questionerResult, Question question) throws SQLException{
		Where<QuestionResult, String> questionResultWhere = getQuestionResultDao().queryBuilder().where();
		questionResultWhere.eq("questioner_result_id", questionerResult.getId());
		questionResultWhere.and();
		questionResultWhere.eq("question_id", question.getId());
		return (ArrayList<QuestionResult>) questionResultWhere.query();
	}
	
	public void deleteQuestionResultFromQuestionerResultAndQuestion(QuestionerResult questionerResult, Question question) throws SQLException{
		ArrayList<QuestionResult> questionResultList = getQuestionResultList(questionerResult, question);
		for(QuestionResult item:questionResultList){
			deleteQuestionResult(item);
		}
	}
	
	public void deleteQuestionResult(QuestionResult questionResult){
		getQuestionResultDao().delete(questionResult);
	}
	//END OF QUESTION RESULT
}
