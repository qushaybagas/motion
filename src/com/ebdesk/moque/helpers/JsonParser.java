package com.ebdesk.moque.helpers;

import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

import com.ebdesk.moque.database.DetQuestions;
import com.ebdesk.moque.database.Header;
import com.ebdesk.moque.database.Question;
import com.ebdesk.moque.database.Questioner;
import com.ebdesk.moque.database.QuestionerResult;
import com.ebdesk.moque.database.User;

public class JsonParser {
	

	private static Questioner.State convertToQuestionerState(String stateString){
		Questioner.State state = Questioner.State.CLOSED;
		if(stateString.equals("broadcast")){
			state = Questioner.State.BROADCAST;
		}else if(stateString.equals("expired")){
			state = Questioner.State.EXPIRED;
		}
		return state;
	}
	
	/**
	 * @param jsonResult
	 * @param context
	 * @throws JSONException
	 */
	public static void parseAndSaveQuestioner(String jsonResult, Context context) throws JSONException {
		boolean save = false;
		JSONArray questionerArrayJson = new JSONArray(jsonResult);
		SessionManager session = new SessionManager(context);
		for(int questionerIndex = 0;questionerIndex<questionerArrayJson.length();questionerIndex++){
			JSONObject questionerJsonObject = questionerArrayJson.getJSONObject(questionerIndex);
			Questioner questioner = new Questioner();
			questioner.setIdFromServer(questionerJsonObject.getInt("mq_q_id"));
			questioner.setCode(questionerJsonObject.getString("mq_q_code"));
			questioner.setComment(questionerJsonObject.getString("mq_q_comment"));
			questioner.setCreatedBy(questionerJsonObject.getString("mq_q_created_by"));
			questioner.setCreatedDate(questionerJsonObject.getString("mq_q_created_date"));
			questioner.setExpiredDate(questionerJsonObject.getString("mq_q_expired_date"));
			questioner.setName(questionerJsonObject.getString("mq_q_name"));
			questioner.setDescription(questionerJsonObject.getString("mq_q_desc"));
			Questioner.State questionerState = convertToQuestionerState(questionerJsonObject.getString("mq_q_state")); 
			questioner.setState(questionerState);
			questioner.setUsername(session.getUsername());
			try {
				save = DatabaseManager.getHelper(context).insertQuestioner(questioner, session.getUsername());
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			//panel
			if (!questionerJsonObject.isNull("panel")) {
				JSONArray panelJSONArray = questionerJsonObject.getJSONArray("panel");
				if (save)
					parseAndSavePanels(panelJSONArray, questioner, context);
			}
			//--end of panel
			
			//question
			JSONArray questionsJsonArray = questionerJsonObject.getJSONArray("questions");
			if(save)
				parseAndSaveQuestions(questionsJsonArray, questioner, context);
		}
	}
	
	public static void parseAndSaveQuestionerStatus(String jsonResult, Context context) throws JSONException, SQLException{
		JSONArray questionerArrayJson = new JSONArray(jsonResult);
		for(int questionerIndex = 0;questionerIndex<questionerArrayJson.length();questionerIndex++){
			JSONObject questionerJSONObj = questionerArrayJson.getJSONObject(questionerIndex);
			Questioner questioner = DatabaseManager.getHelper(context).getQuestionerWithServerID(questionerJSONObj.getInt("mq_q_id"));
			if (questioner!= null){
				String newStateString = questionerJSONObj.getString("mq_q_state");
				Questioner.State newState;
				
				if(newStateString.equals("broadcast")){
					newState = Questioner.State.BROADCAST;
				}else if(newStateString.equals("expired")){
					newState = Questioner.State.EXPIRED;
				}else{
					newState = Questioner.State.CLOSED;
				}
				questioner.setState(newState);
				DatabaseManager.getHelper(context).updateQuestioner(questioner);
			}
		}
	}
	
	private static void parseAndSavePanels(JSONArray panelJSONArray, Questioner questioner, Context context) throws JSONException{
		for(int panelIndex = 0;panelIndex<panelJSONArray.length();panelIndex++){
			JSONObject panelJSONObject = panelJSONArray.getJSONObject(panelIndex);
			QuestionerResult panel = new QuestionerResult();
			
			String respName = panelJSONObject.isNull("mq_pnl_name")?"":panelJSONObject.getString("mq_pnl_name");
			String respAddress = panelJSONObject.isNull("mq_pnl_address")?"":panelJSONObject.getString("mq_pnl_address");
			String respOfficePhone = panelJSONObject.isNull("mq_pnl_phone_number")?"":panelJSONObject.getString("mq_pnl_phone_number");
			String respHandPhone = panelJSONObject.isNull("mq_pnl_handphone")?"":panelJSONObject.getString("mq_pnl_handphone");
			String respPhotoPath = panelJSONObject.isNull("mq_pnl_photo")?"":panelJSONObject.getString("mq_pnl_photo");
			String respNoKTP = panelJSONObject.isNull("mq_pnl_ktp")?"":panelJSONObject.getString("mq_pnl_ktp");
			String respOccupation = panelJSONObject.isNull("mq_pnl_occupation")?"":panelJSONObject.getString("mq_pnl_occupation");
			String respSex = panelJSONObject.isNull("mq_pnl_sex")?"":panelJSONObject.getString("mq_pnl_sex");
			String respAge = panelJSONObject.isNull("mq_pnl_sex")?"":panelJSONObject.getString("mq_pnl_sex");
			String respLastEducation = panelJSONObject.isNull("mq_pnl_last_education")?"":panelJSONObject.getString(panelJSONObject.getString("mq_pnl_last_education"));
			
			panel.setRespName(respName);
			panel.setRespAddress(respAddress);
			panel.setRespOfficePhone(respOfficePhone);
			panel.setRespHandPhone(respHandPhone);
			panel.setRespPhotoPath(respPhotoPath);
			panel.setRespNoKTP(respNoKTP);
			panel.setRespOccupation(respOccupation);
			panel.setRespSex(respSex);
			panel.setRespAge(respAge);
			panel.setRespLastEducation(respLastEducation);
			panel.setQuestioner(questioner);
			panel.setRespondenType(QuestionerResult.RespondenType.PANEL);

			DatabaseManager.getHelper(context).insertQuestionerResult(panel);
		}
	}
	
	private static void parseAndSaveQuestions(JSONArray questionsJsonArray, Questioner questioner, Context context) throws JSONException{
		for(int questionIndex=0;questionIndex<questionsJsonArray.length();questionIndex++){
			JSONObject questionJsonObject = questionsJsonArray.getJSONObject(questionIndex);
			Question question = new Question();
			question.setIdFromServer(questionJsonObject.getInt("mq_qt_id"));
			question.setQuestioner(questioner);

			if(!questionJsonObject.getString("mq_h_id").equals("null")){
				Header header = new Header();
				header.setIdFromServer(questionJsonObject.getInt("mq_h_id"));
				header.setCode(questionJsonObject.getString("mq_h_code"));
				header.setName(questionJsonObject.getString("mq_h_name"));
				header.setDesc(questionJsonObject.getString("mq_h_desc"));
				DatabaseManager.getHelper(context).insertHeader(header);
				
				question.setHeader(header);
			}
			
			question.setEssayNeeded(questionJsonObject.getInt("mq_qt_essai")==0?false:true);
			question.setNumber(Integer.parseInt(questionJsonObject.getString("mq_qt_no")));
			String name = questionJsonObject.getString("mq_qt_name");
			if(name.equals("null"))
				name = "No Title";
			question.setName(name);
			question.setDescription(questionJsonObject.getString("mq_qt_desc"));
			String questionType = questionJsonObject.getString("mq_qt_sa_ma");
			if(questionType.equals("SA")){
				question.setType(Question.Type.SINGLE_ANSWER);
			}else{
				question.setType(Question.Type.MULTIPLE_ANSWER);
			}
			int isTypeNeeded = questionJsonObject.getInt("mq_qt_essai");
			question.setEssayNeeded(isTypeNeeded==0?false:true);

			boolean isDownloaded = true;
			if (!questionJsonObject.isNull("mq_qt_audio")){
				question.setAudioPath(questionJsonObject.getString("mq_qt_audio"));
				isDownloaded = isDownloaded && false;
			}
			if (!questionJsonObject.isNull("mq_qt_video")){
				question.setVideoPath(questionJsonObject.getString("mq_qt_video"));
				isDownloaded = isDownloaded && false;
			}
			if (!questionJsonObject.isNull("mq_qt_image")){
				question.setImagePath(questionJsonObject.getString("mq_qt_image"));
				isDownloaded = isDownloaded && false;
			}
			question.setIsAllMediaDownloaded(isDownloaded);

			DatabaseManager.getHelper(context).insertQuestion(question);
			
			//detail question
			JSONArray optionJsonArray = questionJsonObject.getJSONArray("det_questions");
			parseAndSaveDetQuestions(optionJsonArray, question.getId(), context);
		}
	}
	
	private static void parseAndSaveDetQuestions(JSONArray optionJsonArray, int questionId, Context context) throws JSONException{
		for(int detQIndex = 0;detQIndex<optionJsonArray.length();detQIndex++){
			Question question = DatabaseManager.getHelper(context).getQuestionWithId(questionId);
			boolean isDownloaded = question.isAllMediaDownloaded();
			JSONObject detQJsonObject = optionJsonArray.getJSONObject(detQIndex);
			DetQuestions detQuestion = new DetQuestions();
			detQuestion.setIdFromServer(detQJsonObject.getInt("mq_dqt_id"));
			detQuestion.setQuestion(question);
			detQuestion.setDesc(detQJsonObject.getString("mq_dqt_desc"));
			boolean isEssaiNeeded = false;
			if(!detQJsonObject.isNull("mq_dqt_essai"))
				isEssaiNeeded = detQJsonObject.getInt("mq_dqt_essai")==0?false:true;
			detQuestion.setUsingEssay(isEssaiNeeded);
			
			if (!detQJsonObject.isNull("mq_dqt_audio")){
				detQuestion.setAudioPath(detQJsonObject.getString("mq_dqt_audio"));
				isDownloaded = isDownloaded && false;
			}
			if (!detQJsonObject.isNull("mq_dqt_video")){
				detQuestion.setVideoPath(detQJsonObject.getString("mq_dqt_video"));
				isDownloaded = isDownloaded && false;
			}
			if (!detQJsonObject.isNull("mq_dqt_image")){
				detQuestion.setImagePath(detQJsonObject.getString("mq_dqt_image"));
				isDownloaded = isDownloaded && false;
			}
			detQuestion.setAllMediaDownloaded(isDownloaded);
			DatabaseManager.getHelper(context).insertDetQuestions(detQuestion);
			
			question.setIsAllMediaDownloaded(isDownloaded && question.isAllMediaDownloaded());
			DatabaseManager.getHelper(context).updateQuestion(question);
		}
	}

	public static void parseAndSaveQuota(String jsonResult, Context context) throws JSONException{
		JSONArray jsonArray = new JSONArray(jsonResult);
		for(int i=0;i<jsonArray.length();i++){
			JSONObject entity = jsonArray.getJSONObject(i);
			int questionerServerId = entity.getInt("mq_q_id");
			Questioner questioner = null;
			try {
				questioner = DatabaseManager.getHelper(context).getQuestionerWithServerID(questionerServerId);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			questioner.setTargetAreaAchieved(entity.getInt("mq_q_achieved_area"));
			questioner.setTargetAreaCount(entity.getInt("mq_q_target_area"));
			questioner.setTargetAgenAchieved(entity.getInt("mq_q_achieved_agen"));
			questioner.setState(convertToQuestionerState(entity.getString("mq_q_state")));
			DatabaseManager.getHelper(context).updateQuestioner(questioner);
		}
	}
	
	public static void parseAndSaveQuestionerResult(String jsonResult, Context context,int QuestioneResultId) throws JSONException{
		JSONArray jsonArray = new JSONArray(jsonResult);
		for(int i=0;i<jsonArray.length();i++){
			JSONObject entity = jsonArray.getJSONObject(i);
			int questionerServerId = entity.getInt("mq_qr_id");
			QuestionerResult questionerResult = DatabaseManager.getHelper(context).getQuestionerResultWithId(questionerServerId);
			questionerResult.setSent(entity.getBoolean("success"));
			DatabaseManager.getHelper(context).updateQuestionerResult(questionerResult);
		}
	}
	
	public static User parseProfile(String jsonResult) throws JSONException{
		JSONObject jsonObj = new JSONObject(jsonResult);
		User user = new User();
		String userImageURL = jsonObj.isNull("photo_profile")?"":jsonObj.getString("photo_profile");
		String firstName = jsonObj.isNull("first_name")?"":jsonObj.getString("first_name");
		String lastName = jsonObj.isNull("last_name")?"":jsonObj.getString("last_name");
		String alamat = jsonObj.isNull("address")?"":jsonObj.getString("address");
		String email = jsonObj.isNull("email")?"":jsonObj.getString("email");
		String position = jsonObj.isNull("position")?"":jsonObj.getString("position");
		String companyName = jsonObj.isNull("company_name")?"":jsonObj.getString("company_name");
		String officeAddress = jsonObj.isNull("company_address")?"":jsonObj.getString("company_address");
		String homePhone = jsonObj.isNull("home_phone")?"":jsonObj.getString("home_phone");
		String officePhoneNumber = jsonObj.isNull("office_phone")?"":jsonObj.getString("office_phone");
		String username = jsonObj.isNull("user_name")?"":jsonObj.getString("user_name");
		String password = jsonObj.isNull("user_name")?"":jsonObj.getString("user_name");
		
		user.setUserImageURL(userImageURL);
		user.setPosition(position);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAlamat(alamat);
		user.setEmail(email);
		user.setCompanyName(companyName);
		user.setHomePhone(homePhone);
		user.setOfficeAddress(officeAddress);
		user.setOfficePhoneNumber(officePhoneNumber);
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
	
	public static void parseChangePassword(JSONObject jsonObj, Context context) throws JSONException{
		SessionManager session = new SessionManager(context);

		
		String username = jsonObj.isNull("username")?"":jsonObj.getString("username");
		String hash = jsonObj.isNull("hash")?"":jsonObj.getString("hash");
		
		session.saveUsername(username);
		session.saveHash(hash);

		Toast.makeText(context, "Password berubah", Toast.LENGTH_LONG).show();
	}
	public static ArrayList<String[]> parseMap(String jsonResult) throws JSONException{
		JSONArray jsonArray = new JSONArray(jsonResult);
		ArrayList<String[]> position = new ArrayList<String[]>();
		for(int i=0;i<jsonArray.length();i++){
			JSONObject entity = jsonArray.getJSONObject(i);
			String[] pos=new String[3];
			pos[0]=entity.getString("mq_qr_latitude");
			pos[1]=entity.getString("mq_qr_longitude");
			pos[2]=entity.getString("mq_qr_resp_name");
			position.add(pos);
		}
		return position;
	}
	
}
