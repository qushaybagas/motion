package com.ebdesk.moque.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

public class BitmapHelper {
	public static String saveBitmap(Bitmap bitmap, String fileName){
		File file = new File(DirectoryHelper.getPhotoRespBasePath(), fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bitmap.compress(CompressFormat.JPEG, 100, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return file.getAbsolutePath();
	}
	public static String saveProfileBitmap(Bitmap bitmap, String fileName){
		File file = new File(DirectoryHelper.getPhotoInterviewerBasePath(), fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bitmap.compress(CompressFormat.JPEG, 100, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return file.getAbsolutePath();
	}
	public static void deleteRespPhoto(String fileName){
		File file = new File(DirectoryHelper.getPhotoRespBasePath(), fileName);
		file.delete();
	}
}
