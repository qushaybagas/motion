package com.ebdesk.moque.helpers;

import com.ebdesk.moque.constants.CommonConstants;

public class URLBuilder {

	public static String buildQuestionerURL() {
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner";
	}
	
	public static String buildLoginURL(){
		return CommonConstants.SERVER_URL_CLOUD+"/mlogin";
	}

	public static String buildLogoutURL(){
		return CommonConstants.SERVER_URL_CLOUD+"/mlogout";
	}
	
	public static String buildTransactionSendURL(){
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/send";
	}
	
	public static String buildQuestionerTargetUpdate(String username, String hash){
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/target?username="+username+"&hash="+hash;
	}
	
	public static String buildQuestionerStatusUpdate(String username, String hash){
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/status?username="+username+"&hash="+hash;
	}
	
	public static String buildTransactionSendPhoto(){
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/sendrespondenphoto";
	}
	
	public static String buildProfileSendPhoto(){
		return CommonConstants.SERVER_URL_CLOUD+"/api/profile/updateavatar";
	}
	
	public static String buildProfileGCMRegister(){
		return CommonConstants.SERVER_URL_CLOUD+"/api/profile/gcmregister";
	}
	
	public static String buildMapLocation(){
		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/map";
//		return CommonConstants.SERVER_URL_CLOUD+"/api/questioner/map?mq_q_id=2";
	}

	public static String buildProfileRetrieveURL(String username, String hash){
		return CommonConstants.SERVER_URL_CLOUD+"/api/profile?username="+username+"&hash="+hash;
	}
	
	public static String buildProfileSaveURL(String username, String hash){
		return CommonConstants.SERVER_URL_CLOUD+"/api/profile/update?username="+username+"&hash="+hash;
	}
	
	public static String buildProfileChangePassword(String username, String hash){
		return CommonConstants.SERVER_URL_CLOUD+"/api/profile/updatepassword?username="+username+"&hash="+hash;
	}
		
}
