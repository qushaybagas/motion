package com.ebdesk.moque.helpers;

import android.content.Context;

public interface ResultListener {
	public void onResultSuccess(Context context, String response);
	public void onResultFailed(Context context);
}
