package com.ebdesk.moque.helpers;

import java.io.File;

import android.os.Environment;

public class DirectoryHelper {
	
	public static String getExternalDirPath(){
		return Environment.getExternalStorageDirectory().getAbsolutePath();
	}
	
	public static String getAudioBasePath(){
		String path = "";
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			File sdCardDir = Environment.getExternalStorageDirectory();
			File audioDir = new File(sdCardDir.getAbsoluteFile()+"/Motion/Media/audios");
			if(!audioDir.exists()){
				audioDir.mkdirs();
			}
			path = audioDir.getAbsolutePath();
		}
		return path;
	}
	
	public static String getVideoBasePath(){
		String path = "";
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			File sdCardDir = Environment.getExternalStorageDirectory();
			File audioDir = new File(sdCardDir.getAbsoluteFile()+"/Motion/Media/videos");
			if(!audioDir.exists()){
				audioDir.mkdirs();
			}
			path = audioDir.getAbsolutePath();
		}
		return path;
	}
	
	public static String getImageBasePath(){
		String path = "";
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			File sdCardDir = Environment.getExternalStorageDirectory();
			File audioDir = new File(sdCardDir.getAbsoluteFile()+"/Motion/Media/images");
			if(!audioDir.exists()){
				audioDir.mkdirs();
			}
			path = audioDir.getAbsolutePath();
		}
		return path;
	}
	
	public static String getPhotoRespBasePath(){
		String path = "";
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			File sdCardDir = Environment.getExternalStorageDirectory();
			File audioDir = new File(sdCardDir.getAbsoluteFile()+"/Motion/Responden/photo");
			if(!audioDir.exists()){
				audioDir.mkdirs();
			}
			path = audioDir.getAbsolutePath();
		}
		return path;
	}
	
	public static String getPhotoInterviewerBasePath(){
		String path = "";
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			File sdCardDir = Environment.getExternalStorageDirectory();
			File audioDir = new File(sdCardDir.getAbsoluteFile()+"/Motion/Responden/photo");
			if(!audioDir.exists()){
				audioDir.mkdirs();
			}
			path = audioDir.getAbsolutePath();
		}
		return path;
	}
}
