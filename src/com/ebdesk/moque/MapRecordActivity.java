package com.ebdesk.moque;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.ebdesk.moque.constants.CommonConstants;
import com.ebdesk.moque.helpers.JsonParser;
import com.ebdesk.moque.helpers.ResultListener;
import com.ebdesk.moque.helpers.SessionManager;
import com.ebdesk.moque.helpers.URLBuilder;
import com.ebdesk.moque.helpers.URLTask;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapRecordActivity extends Activity {

	// Google Map
	private GoogleMap googleMap;
	private ArrayList<String[]> position;
	private SessionManager session;
	private Integer questionerId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		session = new SessionManager(this);

		Intent intent = getIntent();
		questionerId = intent.getIntExtra(CommonConstants.PASS_INTENT_QUESTIONER_ID, 0);
		
		createSurveyLocation();
				
	}

	
	
	@Override
	protected void onResume() {
		super.onResume();
		initilizeMap();
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			SessionManager session = new SessionManager(getApplicationContext());
			double sessionLat= session.getLatitude();
			double sessionLon=session.getLongitude();
			googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(sessionLat,sessionLon) , 11) );

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	/*
	 * creating random postion around a location for testing purpose only
	 */
	private void setMarker(ArrayList<String[]> pos) {

		try {
			// Loading map
			initilizeMap();

			// Changing map type
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
			// googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

			// Showing / hiding your current location
			googleMap.setMyLocationEnabled(true);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);

			double latitude = session.getLatitude();
			double longitude = session.getLongitude();

			// lets place some 10 random markers
			for (int i = 0; i < pos.size(); i++) {
				// random latitude and logitude
				String[] Location = pos.get(i);
				double lat=Double.parseDouble(Location[0]);
				double lon=Double.parseDouble(Location[1]);
				String name = Location[2].toString();
				// Adding a marker
				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(lat, lon))
						.title(name);

				// changing marker color
				if (i == 0)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				if (i == 1)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
				if (i == 2)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
				if (i == 3)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
				if (i == 4)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
				if (i == 5)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
				if (i == 6)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_RED));
				if (i == 7)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
				if (i == 8)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
				if (i >= 9)
					marker.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

				googleMap.addMarker(marker);

				// Move the camera to last position with a zoom level
				//if (i == pos.size()-1) {
					CameraPosition cameraPosition = new CameraPosition.Builder()
							.target(new LatLng(lat,lon)).zoom(15).build();

					googleMap.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
				//}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createSurveyLocation(){
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("mq_q_id", questionerId.toString()));
		params.add(new BasicNameValuePair("username", session.getUsername()));
		params.add(new BasicNameValuePair("hash", session.getHash()));
		
		ResultListener listener = new ResultListener() {
			
			@Override
			public void onResultSuccess(Context context, String response) {
				try {
					JSONObject responseJson = new JSONObject(response);
					if(responseJson.getBoolean("success")){
						position=(JsonParser.parseMap(responseJson.getString("value")));
						setMarker(position);
					}else{
						Toast.makeText(MapRecordActivity.this, responseJson.getString("error"), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onResultFailed(Context context) {
				Toast.makeText(MapRecordActivity.this, "Cannot reach server", Toast.LENGTH_LONG).show();
			}
		};

		URLTask task = new URLTask(MapRecordActivity.this, URLBuilder.buildMapLocation(), "Load Map...", listener);
		task.setPostParams(params);
		task.execute();
	}
 
}