package com.ebdesk.moque.temp;

import android.content.Context;

public class Seeder {

	public static void initDatabaseQuestioner(Context context) {
		/*Responden bagas = new Responden();
		bagas.setRespondenIdFromServer(1);
		bagas.setNoKTP("123");
		bagas.setName("Qushay Bagas");

		Responden rofid = new Responden();
		rofid.setRespondenIdFromServer(2);
		rofid.setNoKTP("456");
		rofid.setName("Rofid Rahmadi");

		DatabaseManager.getHelper(context).insertResponden(bagas);
		DatabaseManager.getHelper(context).insertResponden(rofid);
		
		Questioner questioner1 = new Questioner();
		questioner1.setQuestionerId(1);
		questioner1.setQuestionerName("Questioner Jawa Barat");
		questioner1.setDescription("Ditujukan untuk masy jawa barat");
		questioner1.setExpDate("30 Januari 2013");
		questioner1.setTotalRespondenTarget(80);
		DatabaseManager.getHelper(context).insertQuestioner(questioner1);

		Section section1 = new Section();
		section1.setQuestioner(questioner1);
		section1.setSectionId(1);
		section1.setSectionName("Hoby");
		section1.setDescription("Semua tentang hobi kamu");

		Section section2 = new Section();
		section2.setQuestioner(questioner1);
		section2.setSectionId(2);
		section2.setSectionName("Banjir");
		section2.setDescription("Musim hujan 2013");
		DatabaseManager.getHelper(context).insertSection(section1);
		DatabaseManager.getHelper(context).insertSection(section2);

		Question question1 = new Question();
		question1.setQuestionId(1);
		question1.setSection(section1);
		question1.setType(Question.QuestionType.SINGLE);
		question1.setQuestionText("Apakah iya?");

		Question question2 = new Question();
		question2.setQuestionId(2);
		question2.setSection(section2);
		question2.setType(Question.QuestionType.SINGLE);
		question2.setQuestionText("Yang mana yang anda mau?");
		DatabaseManager.getHelper(context).insertQuestion(question1);
		DatabaseManager.getHelper(context).insertQuestion(question2);

		Option optionYa = new Option();
		optionYa.setOptionId(1);
		optionYa.setOptionText("Ya");
		optionYa.setQuestion(question1);

		Option optionTidak = new Option();
		optionTidak.setOptionId(2);
		optionTidak.setOptionText("Tidak");
		optionTidak.setQuestion(question1);

		Option optionMobil = new Option();
		optionMobil.setOptionId(3);
		optionMobil.setOptionText("Mobil");
		optionMobil.setQuestion(question2);

		Option optionMotor = new Option();
		optionMotor.setOptionId(4);
		optionMotor.setOptionText("Motor");
		optionMotor.setQuestion(question2);

		Option optionSepeda = new Option();
		optionSepeda.setOptionId(5);
		optionSepeda.setOptionText("Sepeda");
		optionSepeda.setQuestion(question2);
		DatabaseManager.getHelper(context).insertOption(optionYa);
		DatabaseManager.getHelper(context).insertOption(optionTidak);
		DatabaseManager.getHelper(context).insertOption(optionMobil);
		DatabaseManager.getHelper(context).insertOption(optionMotor);
		DatabaseManager.getHelper(context).insertOption(optionSepeda);
		
		Survey surveyRofid = new Survey();
		surveyRofid.setQuestioner(questioner1);
		surveyRofid.setStatus(Survey.Status.PENDING);
		surveyRofid.setResponden(rofid);
		
		Survey surveyBagas = new Survey();
		surveyBagas.setQuestioner(questioner1);
		surveyBagas.setStatus(Survey.Status.FINISH);
		surveyBagas.setResponden(bagas);
		DatabaseManager.getHelper(context).insertSurvey(surveyRofid);
		DatabaseManager.getHelper(context).insertSurvey(surveyBagas);
		
		Visit visit1Rofid = new Visit();
		visit1Rofid.setDate("January 14th, 2013");
		visit1Rofid.setInfo("Survey dipending karena calon responden tidak ada di rumah");
		visit1Rofid.setSurvey(surveyRofid);
		
		Visit visit2Rofid = new Visit();
		visit2Rofid.setDate("January 15th, 2013");
		visit2Rofid.setInfo("Survey dipending karena calon responden tidak ada di rumah");
		visit2Rofid.setSurvey(surveyRofid);
		
		Visit visit1Bagas = new Visit();
		visit1Bagas.setDate("January 13th, 2013");
		visit1Bagas.setInfo("Kondisi responden tidak memungkinkan");
		visit1Bagas.setSurvey(surveyBagas);
		
		DatabaseManager.getHelper(context).insertVisit(visit1Rofid);
		DatabaseManager.getHelper(context).insertVisit(visit2Rofid);
		DatabaseManager.getHelper(context).insertVisit(visit1Bagas);*/
	}
}
