package com.ebdesk.moque.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ebdesk.moque.R;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Questioner {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@DatabaseField(canBeNull = true)
	private String code;
	@DatabaseField(canBeNull = true)
	private String name;
	@DatabaseField(canBeNull = true)
	private String description;
	@DatabaseField(canBeNull = true)
	private State state;
	@DatabaseField(canBeNull = true)
	private String comment;
	@DatabaseField(canBeNull = true)
	private String expiredDate;
	@DatabaseField(canBeNull = true)
	private String createdDate;
	@DatabaseField(canBeNull = true)
	private String createdBy;
	@DatabaseField(canBeNull = true)
	private int intFlag;
	@DatabaseField(canBeNull = true)
	private String statementInterviewer;
	@DatabaseField(canBeNull = true)
	private boolean interviewerSignNeeded;
	@DatabaseField(canBeNull = true)
	private int spvFlag;
	@DatabaseField(canBeNull = true)
	private String spvStatement;
	@DatabaseField(canBeNull = true)
	private boolean spvSignNeeded;
	@DatabaseField(canBeNull = true)
	private int kishgridInt;
	@DatabaseField(canBeNull = true)
	private String kishgridStatement;
	@DatabaseField(canBeNull = true)
	private String kishgridStatementFailed1;
	@DatabaseField(canBeNull = true)
	private String kishgridStatementFailed2;
	@DatabaseField(canBeNull = true)
	private int screenerFlag;
	@DatabaseField(canBeNull = true)
	private int fixResponden;// 0. responden bebas | 1. responden tetap | 2.
								// responden tetap dan tidak
	@DatabaseField(canBeNull = true)
	private int freeRespondenCount;
	@DatabaseField(canBeNull = true)
	private int panelRespondenCount;
	@ForeignCollectionField
	private ForeignCollection<Question> questionList;
	@ForeignCollectionField
	private ForeignCollection<QuotaQuestioner> quotaQuestionerList;
	/*@ForeignCollectionField
	private ForeignCollection<Transaction> transactionList;*/
	@ForeignCollectionField
	private ForeignCollection<QuestionerResult> qResultList;
	@DatabaseField
	private int personalAchievedCount;
	@DatabaseField
	private int achievedAreaCount;
	@DatabaseField
	private int targetAreaCount;
	@DatabaseField
	private String username;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getTargetAgenCount() {
		return personalAchievedCount;
	}

	public void setTargetAgenAchieved(int personalAchievedCount) {
		this.personalAchievedCount = personalAchievedCount;
	}

	public int getAchievedAreaCount() {
		return achievedAreaCount;
	}

	public void setTargetAreaAchieved(int achievedAreaCount) {
		this.achievedAreaCount = achievedAreaCount;
	}

	public int getTargetAreaCount() {
		return targetAreaCount;
	}

	public void setTargetAreaCount(int targetAreaCount) {
		this.targetAreaCount = targetAreaCount;
	}

	public static enum State {
		DRAFT, PUBLISH, BROADCAST, EXPIRED, CLOSED;
		
		public int getColor(){
			int result;
			switch(this){
			case EXPIRED: result = R.color.tag_expire;break;
			case BROADCAST: result = R.color.tag_broadcast;break;
			case CLOSED: result = R.color.tag_closed;break;
			default:
				result = R.color.black;
				break;
			}
			
			return result;
		}
		
		public String toString(){
			String result;
			switch(this){
			case EXPIRED: result = "Expired";break;
			case BROADCAST: result = "Broadcast";break;
			case CLOSED: result = "Closed";break;
			default:
				result = "Undefined";
				break;
			}
			
			return result;
		}
	}
	
	public ArrayList<Question> getUncompleteMediaQuestions(){
		ArrayList<Question> result = new ArrayList<Question>();
		for(Question item:questionList){
			if(!item.isAllMediaDownloaded())
				result.add(item);
		}
		return result;
	}
	
	public ArrayList<Question> getQuestions(){
		ArrayList<Question> result = new ArrayList<Question>();
		for(Question item:questionList){
			result.add(item);
		}
		return result;
	}
	
	public ArrayList<QuestionerResult> getQuestionerResultList(){
		ArrayList<QuestionerResult> result = new ArrayList<QuestionerResult>();
		for(QuestionerResult item:qResultList){
			result.add(item);
		}
		return result;
	}

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getCreatedDate() {
		String date = createdDate;
		String date_after = formateDateFromstring("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm", date);
		return date_after;
	}
	public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

	    Date parsed = null;
	    String outputDate = "";

	    SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
	    SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

	    try {
	        parsed = df_input.parse(inputDate);
	        outputDate = df_output.format(parsed);

	    } catch (ParseException e) { 
	    }

	    return outputDate;

	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getIntFlag() {
		return intFlag;
	}

	public void setIntFlag(int intFlag) {
		this.intFlag = intFlag;
	}

	public String getStatementInterviewer() {
		return statementInterviewer;
	}

	public void setStatementInterviewer(String statementInterviewer) {
		this.statementInterviewer = statementInterviewer;
	}

	public boolean isInterviewerSignNeeded() {
		return interviewerSignNeeded;
	}

	public void setInterviewerSignNeeded(boolean interviewerSignNeeded) {
		this.interviewerSignNeeded = interviewerSignNeeded;
	}

	public int getSpvFlag() {
		return spvFlag;
	}

	public void setSpvFlag(int spvFlag) {
		this.spvFlag = spvFlag;
	}

	public String getSpvStatement() {
		return spvStatement;
	}

	public void setSpvStatement(String spvStatement) {
		this.spvStatement = spvStatement;
	}

	public boolean isSpvSignNeeded() {
		return spvSignNeeded;
	}

	public void setSpvSignNeeded(boolean spvSignNeeded) {
		this.spvSignNeeded = spvSignNeeded;
	}

	public int getKishgridInt() {
		return kishgridInt;
	}

	public void setKishgridInt(int kishgridInt) {
		this.kishgridInt = kishgridInt;
	}

	public String getKishgridStatement() {
		return kishgridStatement;
	}

	public void setKishgridStatement(String kishgridStatement) {
		this.kishgridStatement = kishgridStatement;
	}

	public String getKishgridStatementFailed1() {
		return kishgridStatementFailed1;
	}

	public void setKishgridStatementFailed1(String kishgridStatementFailed1) {
		this.kishgridStatementFailed1 = kishgridStatementFailed1;
	}

	public String getKishgridStatementFailed2() {
		return kishgridStatementFailed2;
	}

	public void setKishgridStatementFailed2(String kishgridStatementFailed2) {
		this.kishgridStatementFailed2 = kishgridStatementFailed2;
	}

	public int getScreenerFlag() {
		return screenerFlag;
	}

	public void setScreenerFlag(int screenerFlag) {
		this.screenerFlag = screenerFlag;
	}

	public int getFixResponden() {
		return fixResponden;
	}

	public void setFixResponden(int fixResponden) {
		this.fixResponden = fixResponden;
	}

	public int getFreeRespondenCount() {
		return freeRespondenCount;
	}

	public void setFreeRespondenCount(int freeRespondenCount) {
		this.freeRespondenCount = freeRespondenCount;
	}

	public int getPanelRespondenCount() {
		return panelRespondenCount;
	}

	public void setPanelRespondenCount(int panelRespondenCount) {
		this.panelRespondenCount = panelRespondenCount;
	}

	public int getId() {
		return id;
	}

}
