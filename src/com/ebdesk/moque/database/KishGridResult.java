package com.ebdesk.moque.database;

import java.util.ArrayList;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class KishGridResult {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull=true)
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoCreate = true)
	private QuestionerResult questionerResult;
	@DatabaseField(canBeNull = true)
	private String reasonFailed1;
	@DatabaseField(canBeNull = true)
	private String date1;
	@DatabaseField(canBeNull = true)
	private String reasonFailed2;
	@DatabaseField(canBeNull = true)
	private String date2;
	@ForeignCollectionField
	private ForeignCollection<KishGridDetailResult> kishGridDetailResultList;
	
	public ArrayList<KishGridDetailResult> getKishGridDetailResultList(){
		ArrayList<KishGridDetailResult> result = new ArrayList<KishGridDetailResult>();
		for(KishGridDetailResult item:kishGridDetailResultList){
			result.add(item);
		}
		return result;
	}

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public QuestionerResult getQuestionerResult() {
		return questionerResult;
	}

	public void setQuestionerResult(QuestionerResult questionerResult) {
		this.questionerResult = questionerResult;
	}

	public String getReasonFailed1() {
		return reasonFailed1;
	}

	public void setReasonFailed1(String reasonFailed1) {
		this.reasonFailed1 = reasonFailed1;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getReasonFailed2() {
		return reasonFailed2;
	}

	public void setReasonFailed2(String reasonFailed2) {
		this.reasonFailed2 = reasonFailed2;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public int getId() {
		return id;
	}

}
