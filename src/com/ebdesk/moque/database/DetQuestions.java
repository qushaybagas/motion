package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class DetQuestions {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Question question;
	@DatabaseField
	private String code;
	@DatabaseField(canBeNull = true)
	private String desc;
	@DatabaseField
	private boolean isUsingEssay;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_DQT_QUOTA", canBeNull=true)
	private Quota quota;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_DQT_QUOTA_QUESTIONER", canBeNull=true)
	private QuotaQuestioner quotaQuestioner;
	@DatabaseField
	private boolean allMediaDownloaded=false;
	@DatabaseField(canBeNull=true)
	private String videoPath;
	@DatabaseField(canBeNull=true)
	private String audioPath;
	@DatabaseField(canBeNull=true)
	private String imagePath;
	
	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public String getAudioPath() {
		return audioPath;
	}

	public void setAudioPath(String audioPath) {
		this.audioPath = audioPath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public boolean isAllMediaDownloaded() {
		return allMediaDownloaded;
	}

	public void setAllMediaDownloaded(boolean allMediaDownloaded) {
		this.allMediaDownloaded = allMediaDownloaded;
	}
	
	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isUsingEssay() {
		return isUsingEssay;
	}

	public void setUsingEssay(boolean isUsingEssay) {
		this.isUsingEssay = isUsingEssay;
	}

	public Quota getQuota() {
		return quota;
	}

	public void setQuota(Quota quota) {
		this.quota = quota;
	}

	public QuotaQuestioner getQuotaQuestioner() {
		return quotaQuestioner;
	}

	public void setQuotaQuestioner(QuotaQuestioner quotaQuestioner) {
		this.quotaQuestioner = quotaQuestioner;
	}

	public int getId() {
		return id;
	}

}
