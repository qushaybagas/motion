package com.ebdesk.moque.database;

import java.util.ArrayList;
import java.util.UUID;

import com.ebdesk.moque.R;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class QuestionerResult {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(columnName = "MQ_QR_ID")
	private String idForServer;
	@DatabaseField
	private boolean sent = false;
	/*
	 * @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName =
	 * "MQ_T_ID") private Transaction transaction;
	 */
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Questioner questioner;
	@DatabaseField(columnName = "MQ_QR_STATE")
	private State state = State.PENDING;
	@DatabaseField(columnName = "MQ_QR_COMMENT_STATE", canBeNull = true)
	private String commentState;
	@DatabaseField(columnName = "MQ_QR_RESP_ID", canBeNull = true)
	private int respId;
	@DatabaseField(columnName = "MQ_QR_RESP_NAME", canBeNull = true)
	private String respName;
	@DatabaseField(columnName = "MQ_QR_RESP_ADDRESS", canBeNull = true)
	private String respAddress;
	@DatabaseField(columnName = "MQ_QR_RESP_PHONE_NUMBER", canBeNull = true)
	private String respPhone;
	@DatabaseField(columnName = "MQ_QR_RESP_HANDPHONE", canBeNull = true)
	private String respHandPhone;
	@DatabaseField(columnName = "MQ_QR_RESP_OFFICE_PHONE", canBeNull = true)
	private String respOfficePhone;
	@DatabaseField(columnName = "MQ_QR_RESP_PHOT", canBeNull = true)
	private String respPhotoPath;
	@DatabaseField(columnName = "MQ_QR_RESP_KTP", canBeNull = true)
	private String respNoKTP;
	@DatabaseField(columnName = "MQ_QR_RESP_OCCUPATION", canBeNull = true)
	private String respOccupation;
	@DatabaseField(columnName = "MQ_QR_RESP_LAST_EDUCATION", canBeNull = true)
	private String respLastEducation;
	@DatabaseField(columnName = "MQ_QR_RESP_SEX", canBeNull = true)
	private String respSex;
	@DatabaseField(columnName = "MQ_QR_RESP_AGE", canBeNull = true)
	private String respAge;
	@DatabaseField(columnName = "MQ_QR_INT_DATE", canBeNull = true)
	private String interviewDate;
	@DatabaseField(columnName = "MQ_QR_INT_BY", canBeNull = true)
	private String interviewedBy;
	@DatabaseField(columnName = "MQ_QR_INT_SIGN", canBeNull = true)
	private String interviewerSignaturePath;
	@DatabaseField(columnName = "MQ_QR_LATITUDE", canBeNull = true)
	private double lattitude;
	@DatabaseField(columnName = "MQ_QR_LONGITUDE", canBeNull = true)
	private double longitude;
	@DatabaseField(columnName = "MQ_QR_LOC_DESC", canBeNull = true)
	private String locDesc;
	@DatabaseField(columnName = "MQ_QR_LOC_TYPE", canBeNull = true)
	private PositioningType type;
	@DatabaseField(columnName = "MQ_QR_LOC_COUNTRY", canBeNull = true)
	private String country;
	@DatabaseField(columnName = "MQ_QR_LOC_ADM_AREA1", canBeNull = true)
	private String locAdmArea1;
	@DatabaseField(columnName = "MQ_QR_LOC_ADM_AREA2", canBeNull = true)
	private String locAdmArea2;
	@DatabaseField(columnName = "MQ_QR_LOC_ADM_AREA13", canBeNull = true)
	private String locAdmArea3;
	@DatabaseField(columnName = "MQ_QR_LOC_POLITICAL_MISC", canBeNull = true)
	private String locPoliticalMisc;
	@DatabaseField(columnName = "MQ_QR_LOC_POSTAL_CODE", canBeNull = true)
	private String locPostalCode;
	@ForeignCollectionField
	private ForeignCollection<QuestionResult> questionResultList;
	@ForeignCollectionField
	private ForeignCollection<KishGridResult> kishGridResultList;
	@DatabaseField
	private RespondenType respondenType;
	
	public RespondenType getRespondenType() {
		return respondenType;
	}

	public void setRespondenType(RespondenType respondenType) {
		this.respondenType = respondenType;
	}

	public static enum RespondenType{
		PANEL, NONPANEL;
	}

	// TODO KISHGRID INFORMATION FIELD
	
	public QuestionerResult(){
		UUID uuid = UUID.randomUUID();
		idForServer = uuid.toString();
	}

	public static enum PositioningType {
		GPS, NETWORK
	}
	
	public String getRespAge() {
		return respAge;
	}

	public void setRespAge(String respAge) {
		this.respAge = respAge;
	}
	
	public String getRespLastEducation() {
		return respLastEducation;
	}

	public void setRespLastEducation(String respLastEducation) {
		this.respLastEducation = respLastEducation;
	}
	
	public ArrayList<QuestionResult> getQuestionsResultList(){
		ArrayList<QuestionResult> qResultList = new ArrayList<QuestionResult>();
		for(QuestionResult item:questionResultList){
			qResultList.add(item);
		}
		return qResultList;
	}
	
	public void removeKishGridResult(KishGridResult kishGridResult){
		kishGridResultList.remove(kishGridResult);
	}
	
	public ArrayList<KishGridResult> getKishGridResultList(){
		ArrayList<KishGridResult> result = new ArrayList<KishGridResult>();
		for(KishGridResult item:kishGridResultList){
			result.add(item);
		}
		return result;
	}

	// TODO PANEL INFORMATION FIELD
	public static enum State {
		FINISH, VALID_SPV, INVALID_SPV, VALID, INVALID, PENDING;
		
		public String getValue(){
			String result = "";
			switch (this) {
			case FINISH:
				result = "Finish";
				break;
			case PENDING:
				result = "Pending";
				break;
			default:
				result = "Not defined yet";
				break;
			}
			return result;
		}
		
		public int getColorRes(){
			int result;
			switch (this) {
			case FINISH:
				result = R.color.green_bg;
				break;
			case PENDING:
				result = R.color.light_yellow;
				break;
			default:
				result = R.color.red;
				break;
			}
			return result;
		}
	}

	public String getIdForServer() {
		return idForServer;
	}

	public void setIdForServer(String idFromServer) {
		this.idForServer = idFromServer;
	}

	/*
	 * public Transaction getTransaction() { return transaction; }
	 * 
	 * public void setTransaction(Transaction transaction) { this.transaction =
	 * transaction; }
	 */

	public State getState() {
		return state;
	}

	public Questioner getQuestioner() {
		return questioner;
	}

	public void setQuestioner(Questioner questioner) {
		this.questioner = questioner;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getCommentState() {
		return commentState;
	}

	public void setCommentState(String commentState) {
		this.commentState = commentState;
	}

	public int getRespId() {
		return respId;
	}

	public void setRespId(int respId) {
		this.respId = respId;
	}

	public String getRespName() {
		return respName;
	}

	public void setRespName(String respName) {
		this.respName = respName;
	}

	public String getRespAddress() {
		return respAddress;
	}

	public void setRespAddress(String respAddress) {
		this.respAddress = respAddress;
	}

	public String getRespPhone() {
		return respPhone;
	}

	public void setRespPhone(String respPhone) {
		this.respPhone = respPhone;
	}

	public String getRespHandPhone() {
		return respHandPhone;
	}

	public void setRespHandPhone(String respHandPhone) {
		this.respHandPhone = respHandPhone;
	}

	public String getRespOfficePhone() {
		return respOfficePhone;
	}

	public void setRespOfficePhone(String respOfficePhone) {
		this.respOfficePhone = respOfficePhone;
	}

	public String getRespPhotoPath() {
		return respPhotoPath;
	}

	public void setRespPhotoPath(String respPhotoPath) {
		this.respPhotoPath = respPhotoPath;
	}

	public String getRespNoKTP() {
		return respNoKTP;
	}

	public void setRespNoKTP(String respNoKTP) {
		this.respNoKTP = respNoKTP;
	}

	public String getRespOccupation() {
		return respOccupation;
	}

	public void setRespOccupation(String respOccupation) {
		this.respOccupation = respOccupation;
	}

	public String getRespSex() {
		return respSex;
	}

	public void setRespSex(String respSex) {
		this.respSex = respSex;
	}

	public String getInterviewDate() {
		return interviewDate;
	}

	public void setInterviewDate(String interviewDate) {
		this.interviewDate = interviewDate;
	}

	public String getInterviewedBy() {
		return interviewedBy;
	}

	public void setInterviewedBy(String interviewedBy) {
		this.interviewedBy = interviewedBy;
	}

	public String getInterviewerSignaturePath() {
		return interviewerSignaturePath;
	}

	public void setInterviewerSignaturePath(String interviewerSignaturePath) {
		this.interviewerSignaturePath = interviewerSignaturePath;
	}

	public double getLattitude() {
		return lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getLocDesc() {
		return locDesc;
	}

	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	public PositioningType getType() {
		return type;
	}

	public void setType(PositioningType type) {
		this.type = type;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLocAdmArea1() {
		return locAdmArea1;
	}

	public void setLocAdmArea1(String locAdmArea1) {
		this.locAdmArea1 = locAdmArea1;
	}

	public String getLocAdmArea2() {
		return locAdmArea2;
	}

	public void setLocAdmArea2(String locAdmArea2) {
		this.locAdmArea2 = locAdmArea2;
	}

	public String getLocAdmArea3() {
		return locAdmArea3;
	}

	public void setLocAdmArea3(String locAdmArea3) {
		this.locAdmArea3 = locAdmArea3;
	}

	public String getLocPoliticalMisc() {
		return locPoliticalMisc;
	}

	public void setLocPoliticalMisc(String locPoliticalMisc) {
		this.locPoliticalMisc = locPoliticalMisc;
	}

	public String getLocPostalCode() {
		return locPostalCode;
	}

	public void setLocPostalCode(String locPostalCode) {
		this.locPostalCode = locPostalCode;
	}

	public int getId() {
		return id;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

}
