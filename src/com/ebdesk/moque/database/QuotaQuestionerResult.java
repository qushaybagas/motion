package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class QuotaQuestionerResult {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(columnName = "MQ_QQR_ID")
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_QQ_ID")
	private QuotaQuestioner quotaQuestioner;
	@DatabaseField(columnName = "MQ_QQR_VALUE")
	private int value;

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public QuotaQuestioner getQuotaQuestioner() {
		return quotaQuestioner;
	}

	public void setQuotaQuestioner(QuotaQuestioner quotaQuestioner) {
		this.quotaQuestioner = quotaQuestioner;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getId() {
		return id;
	}

}
