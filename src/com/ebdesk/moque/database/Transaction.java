package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class Transaction {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(columnName = "MQ_T_ID")
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_Q_ID")
	private Questioner questioner;
	@DatabaseField(columnName = "MQ_T_VALUE")
	private String value;
	@DatabaseField(columnName = "MQ_T_ISEXTRACTED")
	private boolean isExtracted;// TODO ASK
	/*@ForeignCollectionField
	private ForeignCollection<QuestionerResult> questionerResultList;*/

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public Questioner getQuestioner() {
		return questioner;
	}

	public void setQuestioner(Questioner questioner) {
		this.questioner = questioner;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isExtracted() {
		return isExtracted;
	}

	public void setExtracted(boolean isExtracted) {
		this.isExtracted = isExtracted;
	}

	public int getId() {
		return id;
	}

}
