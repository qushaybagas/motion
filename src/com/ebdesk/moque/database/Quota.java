package com.ebdesk.moque.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Quota {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(columnName = "MQ_QUO_ID")
	private int idFromServer;
	@DatabaseField(columnName = "MQ_QUO_NAME")
	private String name;
	@DatabaseField(columnName = "MQ_QUO_ID_DEFAULT")
	private boolean isDefault;
	@ForeignCollectionField
	private ForeignCollection<QuotaQuestioner> quotaQuestionerList;

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public int getId() {
		return id;
	}

}
