package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class KishGridDetailResult {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private KishGridResult kishGridResult;
	@DatabaseField
	private String name;
	@DatabaseField
	private int age;
	@DatabaseField
	private boolean selected = false;

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public KishGridResult getKishGridResult() {
		return kishGridResult;
	}

	public void setKishGridResult(KishGridResult kishGridResult) {
		this.kishGridResult = kishGridResult;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getId() {
		return id;
	}

}
