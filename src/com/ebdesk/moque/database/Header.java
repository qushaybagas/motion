package com.ebdesk.moque.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Header {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@ForeignCollectionField
	private ForeignCollection<Question> questionList;
	@DatabaseField(foreign=true, foreignAutoRefresh=true)
	private Questioner questioner;
	@DatabaseField(canBeNull = true)
	private String code;
	@DatabaseField(canBeNull = true)
	private String name;
	@DatabaseField(canBeNull = true)
	private String desc;
	public int getIdFromServer() {
		return idFromServer;
	}
	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}
	public ForeignCollection<Question> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(ForeignCollection<Question> questionList) {
		this.questionList = questionList;
	}
	public Questioner getQuestioner() {
		return questioner;
	}
	public void setQuestioner(Questioner questioner) {
		this.questioner = questioner;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getId() {
		return id;
	}
	
	
}
