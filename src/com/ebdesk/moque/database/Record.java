package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class Record {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int hasil;
	@DatabaseField
	private int hasilArea;
	@DatabaseField
	private int targetArea;

	public int getHasil() {
		return hasil;
	}

	public void setHasil(int hasil) {
		this.hasil = hasil;
	}

	public int getHasilArea() {
		return hasilArea;
	}

	public void setHasilArea(int hasilArea) {
		this.hasilArea = hasilArea;
	}

	public int getTargetArea() {
		return targetArea;
	}

	public void setTargetArea(int targetArea) {
		this.targetArea = targetArea;
	}

	public int getId() {
		return id;
	}

}
