package com.ebdesk.moque.database;

import java.util.ArrayList;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class Question {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@DatabaseField
	private int number;
	@DatabaseField
	private String name;
	@DatabaseField(canBeNull = true)
	private String description;
	@DatabaseField
	private boolean isEssayExist;
	@DatabaseField
	private Type type;
	@DatabaseField(canBeNull = true)
	private String videoPath;
	@DatabaseField(canBeNull = true)
	private String imagePath;
	@DatabaseField(canBeNull = true)
	private String audioPath;
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Header header;
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Questioner questioner;
	@ForeignCollectionField
	private ForeignCollection<DetQuestions> detQuestionList;
	@ForeignCollectionField
	private ForeignCollection<QuestionResult> questionResultList;
	@DatabaseField
	private boolean isAllMediaDownloaded;

	public static enum Type {
		SINGLE_ANSWER, MULTIPLE_ANSWER;
		
		public String toString(){
			String result;
			switch (this) {
			case SINGLE_ANSWER:
				result = "SA";
				break;
			case MULTIPLE_ANSWER:
				result = "MA";
				break;
			default:
				result = "";
				break;
			}
			return result;
		}
	}
	
	public boolean isAllMediaDownloaded(){
		return isAllMediaDownloaded;
	}
	
	public void setIsAllMediaDownloaded(boolean isDownloaded){
		isAllMediaDownloaded = isDownloaded;
	}
	
	public ArrayList<QuestionResult> getQuestionResultList(){
		ArrayList<QuestionResult> result = new ArrayList<QuestionResult>();
		for(QuestionResult option:questionResultList){
			result.add(option);
		}
		return result;
	}
	
	public ArrayList<DetQuestions> getOptions(){
		ArrayList<DetQuestions> result = new ArrayList<DetQuestions>();
		for(DetQuestions option:detQuestionList){
			result.add(option);
		}
		return result;
	}

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEssayNeeded() {
		return isEssayExist;
	}

	public void setEssayNeeded(boolean isEssayExist) {
		this.isEssayExist = isEssayExist;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getAudioPath() {
		return audioPath;
	}

	public void setAudioPath(String audioPath) {
		this.audioPath = audioPath;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Questioner getQuestioner() {
		return questioner;
	}

	public void setQuestioner(Questioner questioner) {
		this.questioner = questioner;
	}

	public int getId() {
		return id;
	}

}
