package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class QuestionResult {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private int idFromServer;
	@DatabaseField(canBeNull = true)
	private int headerId;
	@DatabaseField(canBeNull = true)
	private String essai;
	@DatabaseField(canBeNull = true)
	private String essayChoice;
	@DatabaseField(canBeNull = true)
	private Question.Type questionType;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, columnName="question_id")
	private Question question;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=true)
	private DetQuestions detQuestion;
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=true, columnName="questioner_result_id")
	private QuestionerResult questionerResult;

	public QuestionerResult getQuestionerResult() {
		return questionerResult;
	}

	public void setQuestionerResult(QuestionerResult questionerResult) {
		this.questionerResult = questionerResult;
	}

	public DetQuestions getDetQuestion() {
		return detQuestion;
	}

	public void setDetQuestion(DetQuestions detQuestion) {
		this.detQuestion = detQuestion;
		this.question = detQuestion.getQuestion();
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Question getQuestion() {
		return question;
	}

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public int getHeaderId() {
		return headerId;
	}

	public void setHeaderId(int headerId) {
		this.headerId = headerId;
	}

	public String getEssai() {
		return essai;
	}

	public void setEssai(String essai) {
		this.essai = essai;
	}

	public String getEssayChoice() {
		return essayChoice;
	}

	public void setEssayChoice(String essayChoice) {
		this.essayChoice = essayChoice;
	}

	public Question.Type getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Question.Type questionType) {
		this.questionType = questionType;
	}

	public int getId() {
		return id;
	}

}
