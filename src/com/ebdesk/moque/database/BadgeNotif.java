package com.ebdesk.moque.database;

import com.j256.ormlite.field.DatabaseField;

public class BadgeNotif {
	@DatabaseField(generatedId=true)
	private int id;
	@DatabaseField
	private String number;
	
	public int getId() {
		return id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
