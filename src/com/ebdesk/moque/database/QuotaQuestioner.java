package com.ebdesk.moque.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

public class QuotaQuestioner {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(columnName = "MQ_QQ_ID")
	private int idFromServer;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_Q_ID")
	private Questioner questioner;
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "MQ_QUO_ID")
	private Quota quota;
	@DatabaseField(canBeNull = true, columnName = "MQ_QQ_CODE")
	private String code;
	@DatabaseField(columnName = "MQ_QQ_NAME")
	private String name;
	@DatabaseField(columnName = "MQ_QQ_TYPE")
	private QuoQuestionerType type;
	@DatabaseField(columnName = "MQ_QQ_COMPARE")
	private String compare;
	@DatabaseField(columnName = "MQ_QQ_VALUE")
	private String value;
	@ForeignCollectionField
	private ForeignCollection<QuotaQuestionerResult> quotaQuestionerResult;

	public static enum QuoQuestionerType {
		NOMINAL, PERSEN
	}

	public int getIdFromServer() {
		return idFromServer;
	}

	public void setIdFromServer(int idFromServer) {
		this.idFromServer = idFromServer;
	}

	public Questioner getQuestioner() {
		return questioner;
	}

	public void setQuestioner(Questioner questioner) {
		this.questioner = questioner;
	}

	public Quota getQuota() {
		return quota;
	}

	public void setQuota(Quota quota) {
		this.quota = quota;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public QuoQuestionerType getType() {
		return type;
	}

	public void setType(QuoQuestionerType type) {
		this.type = type;
	}

	public String getCompare() {
		return compare;
	}

	public void setCompare(String compare) {
		this.compare = compare;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ForeignCollection<QuotaQuestionerResult> getQuotaQuestionerResult() {
		return quotaQuestionerResult;
	}

	public void setQuotaQuestionerResult(ForeignCollection<QuotaQuestionerResult> quotaQuestionerResult) {
		this.quotaQuestionerResult = quotaQuestionerResult;
	}

	public int getId() {
		return id;
	}

}
