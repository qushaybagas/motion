# README #

last update : February 25th 2014

### What is this repository for? ###

* It’s an integrated system to conduct a survey of data. This project aims to survey election data.
There is also a web application to manage questioner and analyst data survey result.
* Version : 1.0

### How do I get set up? ###

* Clone / download this repo
* Import the android project on eclipse
* Set & config webservice (PHP Laravel Framework)
* Set & config MySQL DB

### Who do I talk to? ###

* Repo owner : bagas.qushay@gmail.com